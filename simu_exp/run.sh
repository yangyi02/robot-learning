PADDLE_PATH=/home/yi/code/paddle/idl/paddle
export PATH=$PADDLE_PATH/paddle:$PATH
# Train path
export PYTHONPATH=$PADDLE_PATH/paddle/output/pylib:$PYTHONPATH
export PYTHONPATH=$PADDLE_PATH/paddle/trainer:$PYTHONPATH

#paddle_trainer --config=captioning_and_qa_gating.conf.py --use_gpu=1 --trainer_count=2 --log_period=200 --test_period=0 --num_passes=31 --save_dir=model/captioning_and_qa_gating 2>&1 | tee train.log
#paddle_trainer --config=captioning_and_qa_gating.conf.py --use_gpu=0 --trainer_count=1 --log_period=200 --test_period=0 --num_passes=31 --job=test --test_pass=49 --save_dir=model/captioning_and_qa_gating --config_args generating=1 2>&1 | tee train.log
#paddle_trainer --config=captioning_and_qa_gating2.conf.py --use_gpu=1 --trainer_count=2 --log_period=200 --test_period=0 --num_passes=31 --save_dir=model/captioning_and_qa_gating2 2>&1 | tee train.log
#paddle_trainer --config=captioning_and_qa_gating2.conf.py --use_gpu=0 --trainer_count=1 --log_period=200 --test_period=0 --num_passes=31 --job=test --test_pass=49 --save_dir=model/captioning_and_qa_gating2 --config_args generating=1 2>&1 | tee train.log
#paddle_trainer --config=captioning_and_qa_gating3.conf.py --use_gpu=1 --trainer_count=2 --log_period=200 --test_period=0 --num_passes=31 --save_dir=model/captioning_and_qa_gating2 2>&1 | tee train.log
#paddle_trainer --config=captioning_and_qa_gating3.conf.py --use_gpu=0 --trainer_count=1 --log_period=200 --test_period=0 --num_passes=31 --job=test --test_pass=49 --save_dir=model/captioning_and_qa_gating2 --config_args generating=1 2>&1 | tee train.log
#paddle_trainer --config=captioning_and_qa_gating4.conf.py --use_gpu=1 --trainer_count=2 --log_period=200 --test_period=0 --num_passes=31 --save_dir=model/captioning_and_qa_gating4 2>&1 | tee train.log
paddle_trainer --config=captioning_and_qa_gating4.conf.py --use_gpu=0 --trainer_count=1 --log_period=200 --test_period=0 --num_passes=31 --job=test --test_pass=49 --save_dir=model/captioning_and_qa_gating4 --config_args generating=1 2>&1 | tee train.log
#paddle_trainer --config=img_qa.py --use_gpu=1 --trainer_count=2 --log_period=200 --test_period=0 --num_passes=31 --save_dir=model/img_qa 2>&1 | tee train.log 
#paddle_trainer --config=img_qa_gen.py --use_gpu=0 --trainer_count=1 --log_period=200 --test_period=0 --num_passes=31 --job=test --test_pass=49 --save_dir=model/img_qa 2>&1 | tee test.log 
#paddle_trainer --config=img_qa_new.py --use_gpu=1 --trainer_count=2 --log_period=200 --test_period=0 --num_passes=31 --save_dir=model/img_qa 2>&1 | tee train.log 
#paddle_trainer --config=img_qa_new_gen.py --use_gpu=0 --trainer_count=1 --log_period=200 --test_period=0 --num_passes=31 --job=test --test_pass=49 --save_dir=model/img_qa 2>&1 | tee test.log 
