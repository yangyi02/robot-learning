# -*- coding: utf-8 -*-
"""
Created on Wed Dec  9 21:42:19 2015

@author: wangjiang03
"""

import os
import io
import time
import cPickle
import logging
import numpy
import math
import random
from trainer.PyDataProviderWrapper import DenseSlot, IndexSlot, provider

logging.basicConfig(
    format='[%(levelname)s %(asctime)s %(filename)s:%(lineno)s] %(message)s',
)
logger = logging.getLogger('paddle_data_provider')
logger.setLevel(logging.INFO)

def initHook(obj, *file_list, **kwargs):
    '''
    Description: Init with a list of data file
    file_list is the name list of input files.
    kwargs["load_data_args"] is the value of 'load_data_args'
    which can be set in config.
    kwargs["load_data_args"] is organized as follows:
        'dictionary path'(str)
        'num_data'(int)
    Each args is seperated by a space.
    '''
    str_conf_args = kwargs['load_data_args'].strip().split()
    dict_file = str_conf_args[0]
    num_data = int(str_conf_args[1])

    logger.info('Dictionary Path: %s', dict_file)
    logger.info('num of data: %d', num_data)

    if os.path.isfile(dict_file):
        word_dict = cPickle.load(io.open(dict_file, 'rb'))
        logger.info('Dictionary loaded with %d words', len(word_dict))
    else:
        logger.fatal('Dictionary file [%s] does not exist!', dict_file)

    if len(file_list) == 0:
        logger.fatal('No annotation file!')
    else:
        logger.info('There are %d annotation files', len(file_list))

    fname = file_list[0].strip()
    if os.path.isfile(fname):
        logger.debug("fname %s", fname)
    else:
        logger.warn('Annotation file %s missing!', fname)

    objects = ['cat', 'dog', 'airplane', 'apple', 'car']
    img_feat_dim = len(objects)
    obj.file_list = list(file_list)
    obj.word_dict = word_dict
    obj.num_data = num_data
    obj.img_feat_dim = img_feat_dim
    obj.objects = objects

    logger.info('DataProvider Initialization finished')
    obj.slots = [IndexSlot(1), # ground truth id
                 DenseSlot(img_feat_dim), # image feature
                 IndexSlot(len(word_dict)), # seq
                 #IndexSlot(len(word_dict)), # seq next
                 #IndexSlot(len(word_dict)), # response
                 #IndexSlot(len(word_dict)), # response next
    ]

def get_coding(sen, word_dict):
    coding = [word_dict.get(word, 0) for word in sen]
    return coding[:-1], coding[1:]


def get_seqs(sample, correct, wa):
    if correct:
        return (sample['question'], sample['answer'])
    else:
        return (sample['question'], wa, sample['answer'], sample['answer'])

def get_coding_seqs(seqs, word_dict):
    seq_all = ['$$E$$']
    for seq in seqs:
        seq_all += seq
        seq_all.append('$$E$$')
    seq_all.append('$$EOP$$')
    seq_all.append('$$E$$')
    return get_coding(seq_all, word_dict)

@provider(use_seq=True, init_hook=initHook)
def processData(obj, file_name):
    '''
    Description: Get a batch of samples
    Return format:
        image feature, question coding, current word coding, next word coding
    '''
    begin_time = time.time()
    word_dict = obj.word_dict
    data = cPickle.load(io.open(file_name, 'rb'))
    objects = obj.objects
    num_data = obj.num_data
    # only 1/10 data will get Q/A format, the other data will get
    # Q/WA/A/A format.
    obj_ids = numpy.random.randint(0, len(objects), num_data)
    for i in range(num_data):
        use_res = random.randint(0, 1)
        if use_res == 0:
            que = ["$$E$$", "what", "is", "in", "the", "image", "$$E$$"]
            ans = ["$$E$$", objects[obj_ids[i]], "$$E$$"]
            groundtruth_id = obj_ids[i]
        else:
            #que = ["$$E$$", "this", "is", objects[obj_ids[i]], "$$E$$"]
            que = ["$$E$$", "$$E$$"]
            #ans = ["$$E$$", "$$E$$"]
            ans = ["$$E$$", "this", "is", objects[obj_ids[i]], "$$E$$"]
            groundtruth_id = num_data + 1
        img_feat = numpy.zeros((obj.img_feat_dim,), dtype=numpy.float32)
        img_feat[obj_ids[i]] = 1
        #print que
        #print ans
        #print ans_res
        que, que_next = get_coding(que, word_dict)
        ans, ans_next = get_coding(ans, word_dict)
        #print que
        #print ans
        #print ans_next
        #print ans_res
        #yield ([img_feat], que, que_next, ans, ans_next)
        yield ([groundtruth_id], [img_feat], que)
    end_time = time.time()
    # logger.info('Loading time spent: %f seconds', end_time - begin_time)


if __name__ == '__main__':
    input_args = "/home/jiang/Dropbox/code/interactive_qa/test_dict.pkl 500"
    data_provider = processData("/home/jiang/data/data/coco_qa_en/cache_data/coco_qa_en/train_batches/batch_8", load_data_args = input_args)
    print len(data_provider.getNextBatch(128))
