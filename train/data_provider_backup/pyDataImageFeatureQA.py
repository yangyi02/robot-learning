#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: gaohaoyuan
# @Date:   2015-01-12 15:31:32
# @Last Modified by:   mypicture
# @Last Modified time: 2015-07-31 11:25:45
#!/usr/bin/python
'''
Description: used for PyDataProvider, user SHOULD
derive from PyDataProviderBase and implement the
functions below. The result will be organised
in binary format.
'''

import numpy as np
import struct
import gc
import os
import logging
import cPickle as pickle
import sys
import time
import random

logging.basicConfig(
    format='[%(levelname)s %(asctime)s %(filename)s:%(lineno)s] %(message)s',
)
logger = logging.getLogger('paddle_data_provider')
logger.setLevel(logging.INFO)

class pyDataImageFeatureQA:

    def __init__(self, *file_list, **kwargs):
        '''
        Description: Init with a list of data file
        file_list is the name list of input files.
        kwargs["load_data_args"] is the value of 'load_data_args' which can be set in config.

        kwargs["load_data_args"] is organized as follows
        '', () indicates the name and type of the args
        Each args is seperated by a space:
        'dictionary path'(str)
        'shuffle flag (True means shuffle)'(bool)
        'img_feat_dim'(int)
        'average norm factor for image feature'(float)
        '''
        gc.disable()
        str_conf_args = kwargs['load_data_args'].strip().split()
        dict_file_path = str_conf_args[0]
        flag_randseq = bool(int(str_conf_args[1]))
        img_feat_dim = int(str_conf_args[2])
        feat_avg_norm_factor = float(str_conf_args[3])
        epoch_num = int(str_conf_args[4])
        self.generating = 0
        if len(str_conf_args) == 6:
            self.generating = int(str_conf_args[5])

        logger.info('Dictionary Path: %s' % dict_file_path)
        logger.info('Flag shuffle: %r' % flag_randseq)
        logger.info('Image dimension: %d' % img_feat_dim)
        logger.info('Average norm factor for image feature: %.4f' %
                    feat_avg_norm_factor)

        if os.path.isfile(dict_file_path):
            dct = np.load(open(dict_file_path, 'rb'))
            if dct.get('$$S$$', 0) == 0:
                dct['$$S$$'] = len(dct)
            if dct.get('$$E$$', 0) == 0:
                dct['$$E$$'] = len(dct)
            logger.info('Dictionary loaded with %d words' % len(dct))
        else:
            logger.fatal(
                'Dictionary file [%s] does not exist!' % dict_file_path)

        if len(file_list) == 0:
            logger.fatal('No annotation file!')
        else:
            logger.info('There are %d annotation files' % len(file_list))

        tmp_num_files = 0

        fname = file_list[0].strip()
        if os.path.isfile(fname):
            logger.debug("fname %s" % fname)
            img_sen_arr = pickle.load(open(fname, 'rb'))
            tmp_num_files += 1
        else:
            logger.warn('Annotation file %s missing!' % fname)

        # img_sen_arr = img_sen_arr[0:100] #DEBUG!!MUST COMMENT OUT!!

        self.file_list = list(file_list)
        self.img_sen_arr = img_sen_arr
        self.dct = dct
        self.img_feat_dim = img_feat_dim
        self.num_in_batch = 0
        self.batch_num = 0
        self.total_batch_num = len(file_list)
        self.feat_avg_norm_factor = feat_avg_norm_factor
        self.epoch_num = epoch_num
        self.total_epoch = 0
        logger.info('SentenceImageFeatDataProvider Initialization finished')

    def shuffle(self):
        '''
          Description: set shuffle operation
        '''
        pass

    def reset(self):
        '''
          Description: execute some reset operation
          in each pass.
        '''
        pass

    def get_coding(self, words, dictionary):
        coding = []
        for word in words:
            id_c = dictionary.get(word, 0)
            coding.append(id_c)

        return coding

    def getHeader(self):
        '''
            Description: Get header info
            Return format:
                slot_num  // Total count of all slots
                use_sequence_flag // 1 if use sequence, 0 if not use sequence
                [(slot_type_1, slot_dim_1),  // A list of all slot type and dim
                 ...,
                 (slot_type_N, slot_dim_N)
                ]
            Note: The type of all header data all int32_t
        '''
        # logger.info('DEBUG::getHeader') #DEBUG
        num_slots = 4
        if self.generating :
            num_slots = 3
        use_sequence = 1

        ret = ''
        ret += struct.pack('ii', num_slots, use_sequence)
        ret += struct.pack('ii', 0, self.img_feat_dim)  # image feature slot
        ret += struct.pack('ii', 3, 1)
        ret += struct.pack('ii', 3, len(self.dct))  # question sentence slot
        if self.generating:
            return ret
        ret += struct.pack('ii', 3, len(self.dct))  # current word slot
        ret += struct.pack('ii', 3, len(self.dct))  # next word slot
        # logger.info("img feat dim: %d, dct length : %d " %(self.img_feat_dim, len(self.dct)))
        # logger.info('Header: %d slots, %d use_sequence' % (num_slots, use_sequence))
        return ret

    def getNextBatch(self, batch_size):
        '''
            Description: Get a batch of samples
            Return format:
                batch_size // specify the return value of getNextBatch
                // The flowing data was organise by slot, and the order of
                // slots was the same as in header. Different slot type has
                // diffent structure of data.
                // Now there are 4 kinds of data type.
                // 1 Dense type
                sample_num   // sample num in dense type slot
                value0 value1 ... valueN  // dense type slot values
                // 2 Sparse type with weight value
                sample_num
                offset0 offset1 ... offsetN // the offset of sample in values.
                                        // Use CSR format to store sparse value.
                length value0 value1 ... valueN
                length weight0  weight1 … weightN
                // 3 Sparse type without weight value type
                sample_num
                offset0 offset1 ... offsetN
                length value0 value1 ... valueN
                // 4 Index type
                sample_num
                value0 value1 ... valueN
                // If you use sequence, you should specify  sequences offsets in
                // each slot.
                slot1_sequence_num sequence_pos0  ... sequence_posN
                ...
                slotN_sequence_num sequence_pos0  ... sequence_posN
        '''
        # logger.info('DEBUG::getNextBatch') #DEBUG
        # if we have scan the whole data once
        # if num in batch == 0, means a new batch needed
        # logger.info("batch size %d" % batch_size)
        if self.num_in_batch == -1:
            logger.info("total_batch_num %d, now: %d" %(self.total_batch_num, self.epoch_num))
            self.num_in_batch = 0
            if self.epoch_num % self.total_batch_num == 0:
                #random.shuffle(self.file_list)
                return struct.pack('i', 0)
        begin_time = time.time()
        if self.num_in_batch == 0:
            # logger.info("Begin batch %d" %(self.epoch_num))
            fname = self.file_list[self.epoch_num % self.total_batch_num].strip()
            if self.epoch_num % self.total_batch_num == 0:
                self.total_epoch += 1
                logger.info("Begin epoch %d" % self.total_epoch)
            if os.path.isfile(fname):
                logger.debug("fname %s" % fname)
                self.img_sen_arr = pickle.load(open(fname, 'rb'))
                #random.shuffle(self.img_sen_arr)
            else:
                logger.warn('Annotation file %s missing!' % fname)
            self.epoch_num += 1

        for i in xrange(len(self.img_sen_arr)):
            if 'sen' in self.img_sen_arr[i]:
                self.img_sen_arr[i]['coding_sen'] = self.get_coding(self.img_sen_arr[i]['sen'], self.dct)
            elif 'question' in self.img_sen_arr[i]:
                self.img_sen_arr[i]['coding_q'] = self.get_coding(self.img_sen_arr[i]['question'], self.dct)
                self.img_sen_arr[i]['coding_a'] = self.get_coding(self.img_sen_arr[i]['answer'], self.dct)
            else:
                logger.fatal("Error Data")

        start_pos = self.num_in_batch
        image_number = 1
        end_pos = start_pos + image_number
        batch = self.img_sen_arr[start_pos:end_pos]

        if end_pos >= len(self.img_sen_arr):
            self.num_in_batch = -1
            self.batch_num += 1
        else:
            self.num_in_batch = end_pos

        ret_img = ''
        ret_qw = ''
        ret_cw = ''
        ret_nw = ''
        ret_seq = ''
        ret_q_seq = ''
        img_seq = ''
        num_samples_batch = 0
        num_sequence_batch = 0
        num_samples_q_batch = 0

        ret_send_id=''

        for one_sample in batch:
            if len(one_sample['feature']) == 0:
                img_feat = np.zeros((self.img_feat_dim,), dtype=np.float32)
            else:
                img_feat = one_sample['feature'] * self.feat_avg_norm_factor

            # check data type
            if one_sample.has_key('coding_sen'):
                question = [0]
            elif one_sample.has_key('coding_q'):
                question = one_sample['coding_q']
            ret_qw += np.array(question, dtype=np.int32).tostring()
            ret_img += img_feat.tostring()

            ret_q_seq = ret_q_seq + struct.pack('i', num_samples_q_batch)
            #ret_send_id += struct.pack('i', 0)
            if len(one_sample['image_id']) == 0:
                ret_send_id += struct.pack('i', 0)
            elif '-' in one_sample['image_id']:
                image_id = one_sample['image_id'].split('-')[1]
                ret_send_id += struct.pack('i', int(image_id))
            else:
                ret_send_id += struct.pack('i', int(one_sample['image_id']))
            img_seq = img_seq + struct.pack('i', num_sequence_batch)
            num_samples_q_batch += len(question)
            num_sequence_batch += 1
            logger.debug('finish samples %d' % num_samples_batch)

        ret_img = struct.pack('i', num_sequence_batch) + ret_img
        ret_qw = struct.pack('i', num_samples_q_batch) + ret_qw
        img_seq = struct.pack('i', num_sequence_batch) + img_seq
        ret_send_id = struct.pack('i', num_sequence_batch) + ret_send_id
        ret_q_seq = struct.pack('i', num_sequence_batch) + ret_q_seq
        ret = struct.pack('i', num_samples_q_batch) + \
            ret_img + ret_send_id + ret_qw  + img_seq *2 + ret_q_seq
        end_time = time.time()
        # logger.info('info: %d, %d, %s' % (num_samples_q_batch, num_sequence_batch, one_sample['image_id']))

        return ret

def check_data(input_header, input_data):
    # check header to get information

    num_slots, use_sequence = struct.unpack('ii', input_header[0:8])

    slots_info = []
    # check each data info
    for i in xrange(num_slots):
        pos_begin = i * 8 + 8
        pos_end = pos_begin + 8
        slot_type, slot_dim = struct.unpack(
            'ii', input_header[pos_begin:pos_end])
        one_slot_info = {'type': slot_type, 'dim': slot_dim}
        slots_info.append(one_slot_info)

    logger.info("Num of Slots: %d, %d, Num of Sequnece: %d" %
                (num_slots, len(slots_info), use_sequence))

    # checking data info
    batch_sample_number = struct.unpack('i', input_data[0:4])[0]
    pos_begin = 4
    print batch_sample_number, slots_info
    for one_slot in slots_info:
        samples_number = struct.unpack('i', input_data[pos_begin:pos_begin + 4])[0]
        logger.info("sample number is %d" % (samples_number))
        pos_begin += 4
        if one_slot['type'] == 0:
            pos_end = pos_begin + samples_number * one_slot['dim'] * 4
            slot_data = np.frombuffer(input_data[pos_begin: pos_end], dtype=np.float32)
            logger.info("type 0 data: len(%d) %f, %f, %f, mean: %f" % (
                len(slot_data) / one_slot['dim'], slot_data[0], slot_data[1], slot_data[-1], np.mean(slot_data)))
        if one_slot['type'] == 3:
            pos_end = pos_begin + samples_number * 4
            slot_data = np.frombuffer(
                input_data[pos_begin: pos_end], dtype=np.int32)
            logger.info("type %d data: len(%d) %d, %d" % (
                one_slot['type'], len(slot_data), slot_data[0], slot_data[-1]))
            for i in slot_data:
                if i > one_slot['dim']:
                    logger.info("error data %d", i)

        pos_begin = pos_end

    # check sequence info
    if use_sequence:
        for one_slot in slots_info:
            samples_number = struct.unpack(
                'i', input_data[pos_begin:pos_begin + 4])[0]
            pos_begin += 4
            pos_end = pos_begin + samples_number * 4
            slot_data = np.frombuffer(
                input_data[pos_begin: pos_end], dtype=np.int32)
            pos_begin = pos_end
            logger.info("type %d data: len(%d) %d, %d" % (
                one_slot['type'], len(slot_data), slot_data[0], slot_data[-1]))

if __name__ == "__main__":
    # test code
    check_type = "IMAGE_QA_FEATURE"

    if check_type == "IMAGE_QA_FEATURE":
        file_list = './data/qa_caption/gen.debug.list'
        str_args = './data/qa_caption/dict.pickle 0 3072 1.0 0 1'
        file_lists = open(file_list).readlines()
        data_provider = pyDataImageFeatureQA(*file_lists, **{'load_data_args': str_args})
    else:
        print "Error: No check type."
        exit()

    for i in xrange(2):
        check_data(data_provider.getHeader(), data_provider.getNextBatch(80))
