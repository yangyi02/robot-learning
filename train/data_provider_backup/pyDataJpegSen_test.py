#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: gaohaoyuan
# @Date:   2015-01-12 15:31:32
# @Last Modified by:   mypicture
# @Last Modified time: 2015-07-31 15:06:50
#!/usr/bin/python
'''
Description: used for PyDataProvider, user SHOULD
derive from PyDataProviderBase and implement the
functions below. The result will be organised
in binary format.
'''

import numpy as np
import struct
import gc
import os
import logging
import cPickle as pickle
import sys
import time
import random

logging.basicConfig(
    format='[%(levelname)s %(asctime)s %(filename)s:%(lineno)s] %(message)s',
)
logger = logging.getLogger('paddle_data_provider')
logger.setLevel(logging.INFO)


class pyDataImageFeatureSen:

    def __init__(self, *file_list, **kwargs):
        '''
        Description: Init with a list of data file
        file_list is the name list of input files.
        kwargs["load_data_args"] is the value of 'load_data_args' which can be set in config.

        kwargs["load_data_args"] is organized as follows
        '', () indicates the name and type of the args
        Each args is seperated by a space:
        'dictionary path'(str)
        'shuffle flag (True means shuffle)'(bool)
        'img_feat_dim'(int)
        'average norm factor for image feature'(float)
        '''
        gc.disable()
        str_conf_args = kwargs['load_data_args'].strip().split()
        #strip white space, then split into a string array with words
        dict_file_path = str_conf_args[0]
        flag_randseq = bool(int(str_conf_args[1]))
        img_feat_dim = int(str_conf_args[2])
        feat_avg_norm_factor = float(str_conf_args[3])
        epoch_num = int(str_conf_args[4])

        logger.info('Dictionary Path: %s' % dict_file_path)
        logger.info('Flag shuffle: %r' % flag_randseq)
        logger.info('Image dimension: %d' % img_feat_dim)
        logger.info('Average norm factor for image feature: %.4f' %
                    feat_avg_norm_factor)

        if os.path.isfile(dict_file_path):
            dct = np.load(open(dict_file_path, 'rb'))
            if dct.get('$$E$$', 0) == 0:
                dct['$$E$$'] = len(dct)
            if dct.get('$$S$$', 0) == 0:
                dct['$$S$$'] = len(dct)
            logger.info('Dictionary loaded with %d words' % len(dct))
        else:
            logger.fatal(
                'Dictionary file [%s] does not exist!' % dict_file_path)

        if len(file_list) == 0:#train list or test/val list
            logger.fatal('No annotation file!')
        else:
            logger.info('There are %d annotation files' % len(file_list))

        tmp_num_files = 0

        fname = file_list[0].strip()
        if os.path.isfile(fname):
            logger.debug("fname %s" % fname)
            img_sen_arr = pickle.load(open(fname, 'rb'))
            #for i in range(len(img_sen_arr)):
            #    logger.info('img_sen_arr: ')
            #    print img_sen_arr[i]['coding_sen']
            tmp_num_files += 1
        else:
            logger.warn('Annotation file %s missing!' % fname)

        # img_sen_arr = img_sen_arr[0:100] #DEBUG!!MUST COMMENT OUT!!

        self.file_list = list(file_list)
        self.img_sen_arr = img_sen_arr
        self.dct = dct
        self.img_feat_dim = img_feat_dim
        self.num_in_batch = 0
        self.batch_num = 0
        self.total_batch_num = len(file_list)
        self.feat_avg_norm_factor = feat_avg_norm_factor
        self.epoch_num = epoch_num
        self.total_epoch = 0
        logger.info('SentenceImageFeatDataProvider Initialization finished')

    def shuffle(self):
        '''
          Description: set shuffle operation
        '''
        pass

    def reset(self):
        '''
          Description: execute some reset operation
          in each pass.
        '''
        pass

    def getHeader(self):
        '''
            Description: Get header info
            Return format:
                slot_num  // Total count of all slots
                use_sequence_flag // 1 if use sequence, 0 if not use sequence
                [(slot_type_1, slot_dim_1),  // A list of all slot type and dim
                 ...,
                 (slot_type_N, slot_dim_N)
                ]
            Note: The type of all header data all int32_t
        '''
        # logger.info('DEUG::getHeader') #DEBUG
        num_slots = 3
        use_sequence = 1

        ret = ''
        ret += struct.pack('ii', num_slots, use_sequence)
        ret += struct.pack('ii', 0, self.img_feat_dim)  # image feature slot
        ret += struct.pack('ii', 3, len(self.dct))  # current word slot
        ret += struct.pack('ii', 3, len(self.dct))  # next word slot
        # logger.info("img feat dim: %d, dct length : %d " %(self.img_feat_dim, len(self.dct)))
        # logger.info('Header: %d slots, %d use_sequence' % (num_slots,
        # use_sequence))
        return ret

    def getNextBatch(self, batch_size):
        '''
            Description: Get a batch of samples
            Return format:
                batch_size // specify the return value of getNextBatch
                // The flowing data was organise by slot, and the order of
                // slots was the same as in header. Different slot type has
                // diffent structure of data.
                // Now there are 4 kinds of data type.
                // 1 Dense type
                sample_num   // sample num in dense type slot
                value0 value1 ... valueN  // dense type slot values
                // 2 Sparse type with weight value
                sample_num
                offset0 offset1 ... offsetN // the offset of sample in values.
                                        // Use CSR format to store sparse value.
                length value0 value1 ... valueN
                length weight0  weight1 … weightN
                // 3 Sparse type without weight value type
                sample_num
                offset0 offset1 ... offsetN
                length value0 value1 ... valueN
                // 4 Index type
                sample_num
                value0 value1 ... valueN
                // If you use sequence, you should specify  sequences offsets in
                // each slot.
                slot1_sequence_num sequence_pos0  ... sequence_posN
                ...
                slotN_sequence_num sequence_pos0  ... sequence_posN
        '''
        # logger.info('DEBUG::getNextBatch') #DEBUG
        # if we have scan the whole data once
        # if num in batch == 0, means a new batch needed
        # logger.info("batch size %d" % batch_size)
        if self.num_in_batch == -1:
            self.num_in_batch = 0
            if self.epoch_num % self.total_batch_num == 0:
                random.shuffle(self.file_list)
                return struct.pack('i', 0)
        begin_time = time.time()
        if self.num_in_batch == 0:
            # logger.info("Begin batch %d" %(self.epoch_num))
            fname = self.file_list[self.epoch_num % self.total_batch_num]
            if self.epoch_num % self.total_batch_num == 0:
                self.total_epoch += 1
                logger.info("Begin epoch %d" % self.total_epoch)
            if os.path.isfile(fname):
                logger.debug("fname %s" % fname)
                self.img_sen_arr = pickle.load(open(fname, 'rb'))
                random.shuffle(self.img_sen_arr)
            else:
                logger.warn('Annotation file %s missing!' % fname)
            self.epoch_num += 1

        start_pos = self.num_in_batch
        image_number = 0
        end_pos = min(self.num_in_batch + batch_size, len(self.img_sen_arr))
        word_length = 0
        for check_one in self.img_sen_arr[start_pos:end_pos]:
            #if word_length + len(check_one['coding_sen_ch'])  < batch_size:
            if word_length + len(check_one['answer'])  < batch_size:
                image_number += 1
                #word_length += len(check_one['coding_sen_ch'])
                word_length += len(check_one['answer'])
            else:
                break

        end_pos = start_pos + image_number
        batch = self.img_sen_arr[start_pos:end_pos]
        #for i in range(len(batch)):
        #    logger.info('batch: ')
        #    print batch[i]['coding_sen']

        if end_pos >= len(self.img_sen_arr):
            self.num_in_batch = -1
            self.batch_num += 1
        else:
            self.num_in_batch = end_pos

        ret_img = ''
        ret_cw = ''
        ret_nw = ''
        ret_seq = ''
        img_seq = ''
        num_samples_batch = 0
        num_sequence_batch = 0

        for one_sample in batch:
            assert(one_sample['feature'].dtype == np.float32)
            img_feat = one_sample['feature'] * self.feat_avg_norm_factor
            #sentence = one_sample['coding_sen_ch']
            #sentence = one_sample['coding_sen']
            sentence = [self.dct.get(word, 0) for word in one_sample['answer']]
            #logger.info(sentence)
            if not sentence[0] == self.dct.get('$$S$$',-1):
                sentence.insert(0, self.dct.get('$$S$$',-1))
                if sentence[0] < 0:
                    logger.fatal('have not get start word')
            if not sentence[-1] == self.dct.get('$$E$$',-1):
                sentence.append(self.dct.get('$$E$$', -1))
                if sentence[-1] < 0:
                    logger.fatal('have not get end word')
            #logger.info(sentence[:-1])
            #logger.info(sentence[1:])
            sentence_len = len(sentence) - 1
            ret_cw += np.array(sentence[:-1], dtype=np.int32).tostring()
            #logger.info('cw: '+str(ret_cw))
            ret_nw += np.array(sentence[1:], dtype=np.int32).tostring()
            #logger.info('nw: '+str(ret_nw))
            ret_img += img_feat.tostring()

            # logger.debug("len: %d, sentence: %s, num_samples_batch: %d" %
            # (len(sentence),' '.join(sentence),num_samples_batch))
            ret_seq = ret_seq + struct.pack('i', num_samples_batch)
            img_seq = img_seq + struct.pack('i', num_sequence_batch)
            num_samples_batch += sentence_len
            num_sequence_batch += 1
            logger.debug('finish samples %d' % num_samples_batch)
            #logger.debug('sentence:%s' % ''.join(one_sample['ch_sentence']))

        # logger.info("img:%d, cw:%d, nw:%d, ret_seq:%d" %
        # (len(ret_img)/4/self.img_feat_dim, len(ret_cw)/4, len(ret_nw)/4,
        # len(ret_seq)/4))
        ret_img = struct.pack('i', num_sequence_batch) + ret_img #image features
        ret_cw = struct.pack('i', num_samples_batch) + ret_cw    #previous word: sentence[::-1]
        ret_nw = struct.pack('i', num_samples_batch) + ret_nw    #next word: sentence[2]
        img_seq = struct.pack('i', num_sequence_batch) + img_seq
        ret_seq = struct.pack('i', num_sequence_batch) + ret_seq
        ret = struct.pack('i', num_samples_batch) + \
            ret_img + ret_cw + ret_nw + img_seq + ret_seq * 2
        end_time = time.time()
        # logger.info("Begin: %f , end: %f, cost:%f" % (begin_time, end_time, end_time - begin_time))
        # logger.info('Pass one bach, num_samples %d, sequence num %d, length %d' % (num_samples_batch, num_sequence_batch,len(ret))) # DEBUG
        # logger.info("img:%d, cw:%d, nw:%d, ret_seq:%d , img_seq: %d " %
        # (len(ret_img)/4/self.img_feat_dim, len(ret_cw)/4, len(ret_nw)/4,
        # len(ret_seq)/4, len(img_seq)/4))
        return ret


class pyDataImageSen:

    def __init__(self, *file_list, **kwargs):
        '''
        Description: Init with a list of data file
        file_list is the name list of input files.
        kwargs["load_data_args"] is the value of 'load_data_args' which can be set in config.

        kwargs["load_data_args"] is organized as follows
        '', () indicates the name and type of the args
        Each args is seperated by a space:
        'dictionary path'(str)
        'shuffle flag (True means shuffle)'(bool)
        'img_feat_dim'(int)
        'average norm factor for image feature'(float)
        '''
        gc.disable()
        str_conf_args = kwargs['load_data_args'].strip().split()
        dict_file_path = str_conf_args[0]
        flag_randseq = bool(int(str_conf_args[1]))
        mean_file_path = str_conf_args[2]
        epoch_num = int(str_conf_args[3])
        if len(str_conf_args) == 5:
            self.generate_seq = int(str_conf_args[4])
        else:
            self.generate_seq = 0
        self.shuffle_flag = 1
        if self.generate_seq:
            self.shuffle_flag = 0

        logger.info('Dictionary Path: %s' % dict_file_path)
        logger.info('Flag shuffle: %r' % flag_randseq)

        if os.path.isfile(dict_file_path):
            dct = np.load(open(dict_file_path, 'rb'))
            if dct.get('$$S$$', 0) == 0:
                dct['$$S$$'] = len(dct)
            if dct.get('$$E$$', 0) == 0:
                dct['$$E$$'] = len(dct)
            logger.info('Dictionary loaded with %d words' % len(dct))
        else:
            logger.fatal(
                'Dictionary file [%s] does not exist!' % dict_file_path)

        if len(file_list) == 0:
            logger.fatal('No annotation file!')
        else:
            logger.info('There are %d annotation files' % len(file_list))

        tmp_num_files = 0

        fname = file_list[0].strip()
        if os.path.isfile(fname):
            logger.debug("fname %s" % fname)
            img_sen_arr = pickle.load(open(fname, 'rb'))
            tmp_num_files += 1
        else:
            logger.warn('Annotation file %s missing!' % fname)

        # img_sen_arr = img_sen_arr[0:100] #DEBUG!!MUST COMMENT OUT!!

        self.file_list = list(file_list)
        self.img_sen_arr = img_sen_arr
        self.dct = dct
        self.num_in_batch = 0
        self.batch_num = 0
        self.total_batch_num = len(file_list)
        self.epoch_num = epoch_num
        self.total_epoch = 0

        # image data related init

        if os.path.isfile(mean_file_path):
            logger.debug("mean file path %s" % mean_file_path)
            self.mean_file = np.load(mean_file_path)
        else:
            logger.warn('Faile to load mean file %s!' % mean_file_path)
        self.lib_name = "decodeJPEG._DeJPEG"
        if os.path.isfile('./decodeJpeg/_DeJPEG.so'):
            logger.info("de jpeg ok")
        logger.info("init conv jpeg mode %s" % self.lib_name)
        self.libmodel = __import__(self.lib_name, fromlist=['_DeJPEG'])
        logger.info("finish loading deJpeg")
        self.image_size = 256
        self.inner_size = 224
        self.mean = self.mean_file['data_mean'].reshape(3, self.image_size, self.image_size)[:, (self.image_size - self.inner_size) / 2:(
            (self.image_size - self.inner_size) / 2 + self.inner_size), (self.image_size - self.inner_size) / 2:((self.image_size - self.inner_size) / 2 + self.inner_size)].reshape(1, -1)[0]
        self.mean_len = len(self.mean)
        logger.info("Mean length is %d" % self.mean_len)
        self.inner_pixels = self.inner_size * self.inner_size
        self.total_pixels = 3 * self.inner_pixels
        self.test = False
        self.multiview = False
        self.data_mult = 1

        #
        logger.info('SentenceImageDataProvider Initialization finished')

    def shuffle(self):
        '''
          Description: set shuffle operation
        '''
        pass

    def reset(self):
        '''
          Description: execute some reset operation
          in each pass.
        '''
        pass

    def getHeader(self):
        '''
            Description: Get header info
            Return format:
                slot_num  // Total count of all slots
                use_sequence_flag // 1 if use sequence, 0 if not use sequence
                [(slot_type_1, slot_dim_1),  // A list of all slot type and dim
                 ...,
                 (slot_type_N, slot_dim_N)
                ]
            Note: The type of all header data all int32_t
        '''
        # logger.info('DEBUG::getHeader') #DEBUG
        if self.generate_seq:
            num_slots = 1
        else:
            num_slots = 3
        use_sequence = 1

        ret = ''
        ret += struct.pack('ii', num_slots, use_sequence)
        ret += struct.pack('ii', 0, self.total_pixels)  # image feature slot
        if not self.generate_seq:
            ret += struct.pack('ii', 3, len(self.dct))  # current word slot
            ret += struct.pack('ii', 3, len(self.dct))  # next word slot
        # logger.info("img feat dim: %d, dct length : %d " %(self.img_feat_dim, len(self.dct)))
        # logger.info('Header: %d slots, %d use_sequence' % (num_slots,
        # use_sequence))
        return ret

    def getJpeg(self, rawdics, img_mat):
        nc_total = len(rawdics)
        jpeg_strs = list(rawdics)
        perm = [idx_tmp for idx_tmp in xrange(nc_total)]
        # self.string="0"*4*nc_total*self.total_pixels
        # self.libmodel.decodeJpegAsString(jpeg_strs, self.img_mat[:nc_total,:], self.image_size, self.inner_size, self.test, self.multiview,self.string)
        # self.libmodel.decodeJpeg(jpeg_strs, img_mat, self.image_size,
        # self.inner_size, self.test, self.multiview,list(perm))
        perm = xrange(img_mat.shape[0])
        self.libmodel.decodeJpeg(
            jpeg_strs, img_mat, self.image_size, self.inner_size, self.test, self.multiview, list(perm))
        self.begin = 0
        self.end = nc_total
        return nc_total

    def getNextBatch(self, batch_size):
        '''
            Description: Get a batch of samples
            Return format:
                batch_size // specify the return value of getNextBatch
                // The flowing data was organise by slot, and the order of
                // slots was the same as in header. Different slot type has
                // diffent structure of data.
                // Now there are 4 kinds of data type.
                // 1 Dense type
                sample_num   // sample num in dense type slot
                value0 value1 ... valueN  // dense type slot values
                // 2 Sparse type with weight value
                sample_num
                offset0 offset1 ... offsetN // the offset of sample in values.
                                        // Use CSR format to store sparse value.
                length value0 value1 ... valueN
                length weight0  weight1 … weightN
                // 3 Sparse type without weight value type
                sample_num
                offset0 offset1 ... offsetN
                length value0 value1 ... valueN
                // 4 Index type
                sample_num
                value0 value1 ... valueN
                // If you use sequence, you should specify  sequences offsets in
                // each slot.
                slot1_sequence_num sequence_pos0  ... sequence_posN
                ...
                slotN_sequence_num sequence_pos0  ... sequence_posN
        '''
        # logger.info('DEBUG::getNextBatch') #DEBUG
        # if we have scan the whole data once
        # if num in batch == 0, means a new batch needed
        # logger.info("batch size %d" % batch_size)
        if self.num_in_batch == -1:
            self.num_in_batch = 0
            if self.epoch_num % self.total_batch_num == 0:
                if self.shuffle_flag:
                    logger.info("Shuffle file list")
                    random.shuffle(self.file_list)
                return struct.pack('i', 0)
        begin_time = time.time()
        if self.num_in_batch == 0:
            # logger.info("Begin batch %d" %(self.epoch_num))
            fname = self.file_list[self.epoch_num %
                                   self.total_batch_num].strip()
            if self.epoch_num % self.total_batch_num == 0:
                self.total_epoch += 1
                logger.info("Begin epoch %d" % self.total_epoch)
            if os.path.isfile(fname):
                logger.debug("fname %s" % fname)
                self.img_sen_arr = pickle.load(open(fname, 'rb'))
                if self.shuffle_flag:
                    random.shuffle(self.img_sen_arr)
            else:
                logger.warn('Annotation file %s missing!' % fname)
            self.epoch_num += 1

        start_pos = self.num_in_batch
        image_number = 0
        end_pos = min(self.num_in_batch + batch_size, len(self.img_sen_arr))
        word_length = 0
        if self.generate_seq:
            ret_img = ''
            img_seq = ''
            num_sequence_batch = 0
            image_data = []
            if end_pos >= len(self.img_sen_arr):
                self.num_in_batch = -1
                self.batch_num += 1
            else:
                self.num_in_batch = end_pos
            batch = self.img_sen_arr[start_pos:end_pos]
            for one_sample in batch:
                image_data.append(one_sample['feature'])
            img_mat_get = np.empty(
                (len(image_data) * self.data_mult, self.total_pixels), dtype=np.float32)
            len_jpeg = self.getJpeg(image_data, img_mat_get)
            img_mat_get -= self.mean
            for i in xrange(len(batch)):
                image_data = img_mat_get[i, :]
                sentence = batch[i]['id'] + ": "
                for q in batch[i]['sentences']:
                    sentence += q
                logger.info(sentence)
                ret_img += image_data.tostring()
                img_seq += struct.pack('i', num_sequence_batch)
                num_sequence_batch += 1

            ret_img = struct.pack('i', num_sequence_batch) + ret_img
            img_seq = struct.pack('i', num_sequence_batch) + img_seq
            ret = struct.pack('i', num_sequence_batch) + ret_img + img_seq
            return ret

        for check_one in self.img_sen_arr[start_pos:end_pos]:
            if word_length + len(check_one['coding_sentences']) - 1 < batch_size:
                image_number += 1
                word_length += len(check_one['coding_sentences'])
            else:
                break

        end_pos = start_pos + image_number
        batch = self.img_sen_arr[start_pos:end_pos]

        if end_pos >= len(self.img_sen_arr):
            self.num_in_batch = -1
            self.batch_num += 1
        else:
            self.num_in_batch = end_pos

        ret_img = ''
        ret_cw = ''
        ret_nw = ''
        ret_seq = ''
        img_seq = ''
        num_samples_batch = 0
        num_sequence_batch = 0

        # get image batch
        image_data = []
        for one_sample in batch:
            image_data.append(one_sample['feature'])
        img_mat_get = np.empty(
            (len(image_data) * self.data_mult, self.total_pixels), dtype=np.float32)
        len_jpeg = self.getJpeg(image_data, img_mat_get)
        img_mat_get -= self.mean

        for i in xrange(len(batch)):
            one_sample = batch[i]
            image_data = img_mat_get[i, :]
            assert(image_data.dtype == np.float32)
            img_feat = image_data
            sentence = one_sample['coding_sentences']
            sentence_len = len(sentence) - 1
            ret_cw += np.array(sentence[:-1], dtype=np.int32).tostring()
            ret_nw += np.array(sentence[1:], dtype=np.int32).tostring()
            ret_img += img_feat.tostring()

            # logger.debug("len: %d, sentence: %s, num_samples_batch: %d" %
            # (len(sentence),' '.join(sentence),num_samples_batch))
            ret_seq = ret_seq + struct.pack('i', num_samples_batch)
            img_seq = img_seq + struct.pack('i', num_sequence_batch)
            num_samples_batch += sentence_len
            num_sequence_batch += 1
            logger.debug('finish samples %d' % num_samples_batch)
            logger.debug('sentence:%s' % ''.join(one_sample['sentences']))
            logger.debug('coding_sentence:%d,%d' % (
                one_sample['coding_sentences'][0], one_sample['coding_sentences'][1]))

        # logger.info("img:%d, cw:%d, nw:%d, ret_seq:%d" %
        # (len(ret_img)/4/self.img_feat_dim, len(ret_cw)/4, len(ret_nw)/4,
        # len(ret_seq)/4))
        ret_img = struct.pack('i', num_sequence_batch) + ret_img
        ret_cw = struct.pack('i', num_samples_batch) + ret_cw
        ret_nw = struct.pack('i', num_samples_batch) + ret_nw
        img_seq = struct.pack('i', num_sequence_batch) + img_seq
        ret_seq = struct.pack('i', num_sequence_batch) + ret_seq
        ret = struct.pack('i', num_samples_batch) + \
            ret_img + ret_cw + ret_nw + img_seq + ret_seq * 2
        end_time = time.time()
        # logger.info("Begin: %f , end: %f, cost:%f" % (begin_time, end_time, end_time - begin_time))
        # logger.info('Pass one bach, num_samples %d, sequence num %d, length %d' % (num_samples_batch, num_sequence_batch,len(ret))) # DEBUG
        # logger.info("img:%d, cw:%d, nw:%d, ret_seq:%d " %
        # (len(ret_img)/4/self.total_pixels, len(ret_cw)/4, len(ret_nw)/4,
        # len(ret_seq)/4))
        return ret


class pyDataImageFeatureBIQA:

    def __init__(self, *file_list, **kwargs):
        '''
        Description: Init with a list of data file
        file_list is the name list of input files.
        kwargs["load_data_args"] is the value of 'load_data_args' which can be set in config.

        kwargs["load_data_args"] is organized as follows
        '', () indicates the name and type of the args
        Each args is seperated by a space:
        'dictionary path'(str)
        'shuffle flag (True means shuffle)'(bool)
        'img_feat_dim'(int)
        'average norm factor for image feature'(float)
        '''
        gc.disable()
        str_conf_args = kwargs['load_data_args'].strip().split()
        dict_file_path = str_conf_args[0]
        flag_randseq = bool(int(str_conf_args[1]))
        img_feat_dim = int(str_conf_args[2])
        feat_avg_norm_factor = float(str_conf_args[3])
        epoch_num = int(str_conf_args[4])

        logger.info('Dictionary Path: %s' % dict_file_path)
        logger.info('Flag shuffle: %r' % flag_randseq)
        logger.info('Image dimension: %d' % img_feat_dim)
        logger.info('Average norm factor for image feature: %.4f' %
                    feat_avg_norm_factor)

        if os.path.isfile(dict_file_path):
            dct = np.load(open(dict_file_path, 'rb'))
            if dct.get('$$E$$', 0) == 0:
                dct['$$E$$'] = len(dct)
            if dct.get('$$S$$', 0) == 0:
                dct['$$S$$'] = len(dct)
            logger.info('Dictionary loaded with %d words' % len(dct))
        else:
            logger.fatal(
                'Dictionary file [%s] does not exist!' % dict_file_path)

        if len(file_list) == 0:
            logger.fatal('No annotation file!')
        else:
            logger.info('There are %d annotation files' % len(file_list))

        tmp_num_files = 0

        fname = file_list[0].strip()
        if os.path.isfile(fname):
            logger.debug("fname %s" % fname)
            img_sen_arr = pickle.load(open(fname, 'rb'))
            tmp_num_files += 1
        else:
            logger.warn('Annotation file %s missing!' % fname)

        # img_sen_arr = img_sen_arr[0:100] #DEBUG!!MUST COMMENT OUT!!

        self.file_list = list(file_list)
        self.img_sen_arr = img_sen_arr
        self.dct = dct
        self.img_feat_dim = img_feat_dim
        self.num_in_batch = 0
        self.batch_num = 0
        self.total_batch_num = len(file_list)
        self.feat_avg_norm_factor = feat_avg_norm_factor
        self.epoch_num = epoch_num
        self.total_epoch = 0
        logger.info('SentenceImageFeatDataProvider Initialization finished')

    def shuffle(self):
        '''
          Description: set shuffle operation
        '''
        pass

    def reset(self):
        '''
          Description: execute some reset operation
          in each pass.
        '''
        pass

    def getHeader(self):
        '''
            Description: Get header info
            Return format:
                slot_num  // Total count of all slots
                use_sequence_flag // 1 if use sequence, 0 if not use sequence
                [(slot_type_1, slot_dim_1),  // A list of all slot type and dim
                 ...,
                 (slot_type_N, slot_dim_N)
                ]
            Note: The type of all header data all int32_t
        '''
        # logger.info('DEBUG::getHeader') #DEBUG
        num_slots = 7
        use_sequence = 1

        ret = ''
        ret += struct.pack('ii', num_slots, use_sequence)

        ret += struct.pack('ii', 0, self.img_feat_dim)  # image feature slot
        # 6 parts:
        # 1. question sentence
        # 2. answer word
        # 3. answer next word
        # 4. answer sentence
        # 5. question word
        # 6. question next word
        ret += struct.pack('ii', 3, len(self.dct)) * 6
        return ret

    def gen_word_part(self, coding, num):
        info = np.array(coding, dtype = np.int32).tostring()
        seq  = struct.pack('i', num)
        num += len(coding)
        return [info, seq, num]

    def getNextBatch(self, batch_size):
        '''
            Description: Get a batch of samples
            Return format:
                batch_size // specify the return value of getNextBatch
                // The flowing data was organise by slot, and the order of
                // slots was the same as in header. Different slot type has
                // diffent structure of data.
                // Now there are 4 kinds of data type.
                // 1 Dense type
                sample_num   // sample num in dense type slot
                value0 value1 ... valueN  // dense type slot values
                // 2 Sparse type with weight value
                sample_num
                offset0 offset1 ... offsetN // the offset of sample in values.
                                        // Use CSR format to store sparse value.
                length value0 value1 ... valueN
                length weight0  weight1 … weightN
                // 3 Sparse type without weight value type
                sample_num
                offset0 offset1 ... offsetN
                length value0 value1 ... valueN
                // 4 Index type
                sample_num
                value0 value1 ... valueN
                // If you use sequence, you should specify  sequences offsets in
                // each slot.
                slot1_sequence_num sequence_pos0  ... sequence_posN
                ...
                slotN_sequence_num sequence_pos0  ... sequence_posN
        '''
        # logger.info('DEBUG::getNextBatch') #DEBUG
        # if we have scan the whole data once
        # if num in batch == 0, means a new batch needed
        # logger.info("batch size %d" % batch_size)
        if self.num_in_batch == -1:
            self.num_in_batch = 0
            if self.epoch_num % self.total_batch_num == 0:
                random.shuffle(self.file_list)
                return struct.pack('i', 0)
        begin_time = time.time()
        if self.num_in_batch == 0:
            # logger.info("Begin batch %d" %(self.epoch_num))
            fname = self.file_list[self.epoch_num %
                                   self.total_batch_num].strip()
            if self.epoch_num % self.total_batch_num == 0:
                self.total_epoch += 1
                logger.info("Begin epoch %d" % self.total_epoch)
            if os.path.isfile(fname):
                logger.debug("fname %s" % fname)
                self.img_sen_arr = pickle.load(open(fname, 'rb'))
                random.shuffle(self.img_sen_arr)
            else:
                logger.warn('Annotation file %s missing!' % fname)
            self.epoch_num += 1

        start_pos = self.num_in_batch
        image_number = 0
        end_pos = min(self.num_in_batch + batch_size, len(self.img_sen_arr))
        q_length = 0
        a_length = 0

        for check_one in self.img_sen_arr[start_pos:end_pos]:
            if max(q_length + len(check_one['coding_q']) - 1, a_length + len(check_one['coding_a']) - 1) < batch_size:
                image_number += 1
                a_length += len(check_one['coding_a'])
                q_length += len(check_one['coding_q'])
            else:
                break

        end_pos = start_pos + image_number
        batch = self.img_sen_arr[start_pos:end_pos]

        if end_pos >= len(self.img_sen_arr):
            self.num_in_batch = -1
            self.batch_num += 1
        else:
            self.num_in_batch = end_pos

        # insert 7 part of data
        # 1. image
        # 2-7. quesion sentence, answer word, answer next word, answer sentence, que word, que next word
        ret_info = [''] * 7
        ret_seq = [''] * 7
        batch_num = [0] * 7

        for i in xrange(len(batch)):
            one_sample = batch[i]
            img_feat = one_sample['feature'] * self.feat_avg_norm_factor
            question = one_sample['coding_q']
            answer = one_sample['coding_a']

            # check question and answer type
            if answer[0] == self.dct.get('$$S$$', -1):
                answer = answer[1:]
            if answer[-1] == self.dct.get('$$E$$', -1):
                answer = answer[:-1]
            if question[0] == self.dct.get('$$S$$', -1):
                question = question[1:]
            if not question[-1] == self.dct.get('$$E$$', -1):
                question = question[:-1]

            start_sign = [self.dct.get('$$S$$', -1)]
            end_sign = [self.dct.get('$$E$$', -1)]

            ret_info[0]  += img_feat.tostring()
            ret_seq[0]   += struct.pack('i', batch_num[0])
            batch_num[0] += 1

            data_detail = [ question, start_sign + answer, answer + end_sign, answer, start_sign + question, question + end_sign ]

            for j in xrange(len(data_detail)):
                one_info, one_seq, batch_num[j+1] = self.gen_word_part(data_detail[j], batch_num[j+1])
                ret_info[j+1] += one_info
                ret_seq[j+1] += one_seq

        #logger.info('Pass one bach' ) # DEBUG
        #logger.info(batch_num) # DEBUG
        for i in xrange(len(ret_info)):
            ret_info[i] = struct.pack('i', batch_num[i]) + ret_info[i]
            ret_seq[i]  = struct.pack('i', batch_num[0]) + ret_seq[i]

        ret = struct.pack('i', max(batch_num)) + ''.join(ret_info) + ''.join(ret_seq)

        return ret

class pyDataImageFeatureAQ:

    def __init__(self, *file_list, **kwargs):
        '''
        Description: Init with a list of data file
        file_list is the name list of input files.
        kwargs["load_data_args"] is the value of 'load_data_args' which can be set in config.

        kwargs["load_data_args"] is organized as follows
        '', () indicates the name and type of the args
        Each args is seperated by a space:
        'dictionary path'(str)
        'shuffle flag (True means shuffle)'(bool)
        'img_feat_dim'(int)
        'average norm factor for image feature'(float)
        '''
        gc.disable()
        str_conf_args = kwargs['load_data_args'].strip().split()
        dict_file_path = str_conf_args[0]
        flag_randseq = bool(int(str_conf_args[1]))
        img_feat_dim = int(str_conf_args[2])
        feat_avg_norm_factor = float(str_conf_args[3])
        epoch_num = int(str_conf_args[4])

        logger.info('Dictionary Path: %s' % dict_file_path)
        logger.info('Flag shuffle: %r' % flag_randseq)
        logger.info('Image dimension: %d' % img_feat_dim)
        logger.info('Average norm factor for image feature: %.4f' %
                    feat_avg_norm_factor)

        if os.path.isfile(dict_file_path):
            dct = np.load(open(dict_file_path, 'rb'))
            if dct.get('$$E$$', 0) == 0:
                dct['$$E$$'] = len(dct)
            if dct.get('$$S$$', 0) == 0:
                dct['$$S$$'] = len(dct)
            logger.info('Dictionary loaded with %d words' % len(dct))
        else:
            logger.fatal(
                'Dictionary file [%s] does not exist!' % dict_file_path)

        if len(file_list) == 0:
            logger.fatal('No annotation file!')
        else:
            logger.info('There are %d annotation files' % len(file_list))

        tmp_num_files = 0

        fname = file_list[0].strip()
        if os.path.isfile(fname):
            logger.debug("fname %s" % fname)
            img_sen_arr = pickle.load(open(fname, 'rb'))
            tmp_num_files += 1
        else:
            logger.warn('Annotation file %s missing!' % fname)

        # img_sen_arr = img_sen_arr[0:100] #DEBUG!!MUST COMMENT OUT!!

        self.file_list = list(file_list)
        self.img_sen_arr = img_sen_arr
        self.dct = dct
        self.img_feat_dim = img_feat_dim
        self.num_in_batch = 0
        self.batch_num = 0
        self.total_batch_num = len(file_list)
        self.feat_avg_norm_factor = feat_avg_norm_factor
        self.epoch_num = epoch_num
        self.total_epoch = 0
        logger.info('SentenceImageFeatDataProvider Initialization finished')

    def shuffle(self):
        '''
          Description: set shuffle operation
        '''
        pass

    def reset(self):
        '''
          Description: execute some reset operation
          in each pass.
        '''
        pass

    def getHeader(self):
        '''
            Description: Get header info
            Return format:
                slot_num  // Total count of all slots
                use_sequence_flag // 1 if use sequence, 0 if not use sequence
                [(slot_type_1, slot_dim_1),  // A list of all slot type and dim
                 ...,
                 (slot_type_N, slot_dim_N)
                ]
            Note: The type of all header data all int32_t
        '''
        # logger.info('DEBUG::getHeader') #DEBUG
        num_slots = 4
        use_sequence = 1

        ret = ''
        ret += struct.pack('ii', num_slots, use_sequence)
        ret += struct.pack('ii', 0, self.img_feat_dim)  # image feature slot
        ret += struct.pack('ii', 3, len(self.dct))  # question sentence slot
        ret += struct.pack('ii', 3, len(self.dct))  # current word slot
        ret += struct.pack('ii', 3, len(self.dct))  # next word slot
        # logger.info("img feat dim: %d, dct length : %d " %(self.img_feat_dim, len(self.dct)))
        # logger.info('Header: %d slots, %d use_sequence' % (num_slots,
        # use_sequence))
        return ret

    def getNextBatch(self, batch_size):
        '''
            Description: Get a batch of samples
            Return format:
                batch_size // specify the return value of getNextBatch
                // The flowing data was organise by slot, and the order of
                // slots was the same as in header. Different slot type has
                // diffent structure of data.
                // Now there are 4 kinds of data type.
                // 1 Dense type
                sample_num   // sample num in dense type slot
                value0 value1 ... valueN  // dense type slot values
                // 2 Sparse type with weight value
                sample_num
                offset0 offset1 ... offsetN // the offset of sample in values.
                                        // Use CSR format to store sparse value.
                length value0 value1 ... valueN
                length weight0  weight1 … weightN
                // 3 Sparse type without weight value type
                sample_num
                offset0 offset1 ... offsetN
                length value0 value1 ... valueN
                // 4 Index type
                sample_num
                value0 value1 ... valueN
                // If you use sequence, you should specify  sequences offsets in
                // each slot.
                slot1_sequence_num sequence_pos0  ... sequence_posN
                ...
                slotN_sequence_num sequence_pos0  ... sequence_posN
        '''
        # logger.info('DEBUG::getNextBatch') #DEBUG
        # if we have scan the whole data once
        # if num in batch == 0, means a new batch needed
        # logger.info("batch size %d" % batch_size)
        if self.num_in_batch == -1:
            self.num_in_batch = 0
            if self.epoch_num % self.total_batch_num == 0:
                random.shuffle(self.file_list)
                return struct.pack('i', 0)
        begin_time = time.time()
        if self.num_in_batch == 0:
            # logger.info("Begin batch %d" %(self.epoch_num))
            fname = self.file_list[self.epoch_num %
                                   self.total_batch_num].strip()
            if self.epoch_num % self.total_batch_num == 0:
                self.total_epoch += 1
                logger.info("Begin epoch %d" % self.total_epoch)
            if os.path.isfile(fname):
                logger.debug("fname %s" % fname)
                self.img_sen_arr = pickle.load(open(fname, 'rb'))
                random.shuffle(self.img_sen_arr)
            else:
                logger.warn('Annotation file %s missing!' % fname)
            self.epoch_num += 1

        start_pos = self.num_in_batch
        image_number = 0
        end_pos = min(self.num_in_batch + batch_size, len(self.img_sen_arr))
        q_length = 0
        a_length = 0

        for check_one in self.img_sen_arr[start_pos:end_pos]:
            if max(q_length + len(check_one['coding_q']) - 1, a_length + len(check_one['coding_a']) - 1) < batch_size:
                image_number += 1
                a_length += len(check_one['coding_a'])
                q_length += len(check_one['coding_q'])
            else:
                break

        end_pos = start_pos + image_number
        batch = self.img_sen_arr[start_pos:end_pos]

        if end_pos >= len(self.img_sen_arr):
            self.num_in_batch = -1
            self.batch_num += 1
        else:
            self.num_in_batch = end_pos

        ret_img = ''
        ret_qw = ''
        ret_cw = ''
        ret_nw = ''
        ret_seq = ''
        ret_q_seq = ''
        img_seq = ''
        num_samples_batch = 0
        num_sequence_batch = 0
        num_samples_q_batch = 0

        for i in xrange(len(batch)):
            one_sample = batch[i]
            img_feat = one_sample['feature'] * self.feat_avg_norm_factor
            question = one_sample['coding_q']
            answer = one_sample['coding_a']

            # check question and answer type
            if answer[0] == self.dct.get('$$S$$', -1):
                answer = answer[1:]
            if answer[-1] == self.dct.get('$$E$$', -1):
                answer = answer[:-1]
            if question[0] == self.dct.get('$$S$$', -1):
                question = question[1:]
            if not question[-1] == self.dct.get('$$E$$', -1):
                question = question[:-1]

            start_sign_str = struct.pack('i', self.dct.get('$$S$$', -1))
            end_sign_str = struct.pack('i', self.dct.get('$$E$$', -1))

            ret_qw += np.array(answer, dtype=np.int32).tostring()
            ret_cw += start_sign_str + np.array(question, dtype=np.int32).tostring()
            ret_nw += np.array(question, dtype=np.int32).tostring() + end_sign_str
            ret_img += img_feat.tostring()

            # logger.debug("len: %d, sentence: %s, num_samples_batch: %d" %
            # (len(sentence),' '.join(sentence),num_samples_batch))
            ret_seq = ret_seq + struct.pack('i', num_samples_batch)
            ret_q_seq = ret_q_seq + struct.pack('i', num_samples_q_batch)
            img_seq = img_seq + struct.pack('i', num_sequence_batch)
            num_samples_q_batch += len(answer)
            num_samples_batch += len(question) + 1
            num_sequence_batch += 1

        # logger.info("img:%d, cw:%d, nw:%d, ret_seq:%d" %
        # (len(ret_img)/4/self.img_feat_dim, len(ret_cw)/4, len(ret_nw)/4,
        # len(ret_seq)/4))
        ret_img = struct.pack('i', num_sequence_batch) + ret_img
        ret_qw = struct.pack('i', num_samples_q_batch) + ret_qw
        ret_cw = struct.pack('i', num_samples_batch) + ret_cw
        ret_nw = struct.pack('i', num_samples_batch) + ret_nw
        img_seq = struct.pack('i', num_sequence_batch) + img_seq
        ret_seq = struct.pack('i', num_sequence_batch) + ret_seq
        ret_q_seq = struct.pack('i', num_sequence_batch) + ret_q_seq
        ret = struct.pack('i', num_samples_batch) + ret_img + \
            ret_qw + ret_cw + ret_nw + img_seq + ret_q_seq + ret_seq * 2
        end_time = time.time()
        # logger.info("Begin: %f , end: %f, cost:%f" % (begin_time, end_time, end_time - begin_time))
        # logger.info('Pass one bach, num_samples %d, sequence num %d, length %d' % (num_samples_batch, num_sequence_batch,len(ret))) # DEBUG
        # logger.info("img:%d, cw:%d, nw:%d, ret_seq:%d " %
        # (len(ret_img)/4/self.total_pixels, len(ret_cw)/4, len(ret_nw)/4,
        # len(ret_seq)/4))
        return ret

class pyDataImageFeatureQA:

    def __init__(self, *file_list, **kwargs):
        '''
        Description: Init with a list of data file
        file_list is the name list of input files.
        kwargs["load_data_args"] is the value of 'load_data_args' which can be set in config.

        kwargs["load_data_args"] is organized as follows
        '', () indicates the name and type of the args
        Each args is seperated by a space:
        'dictionary path'(str)
        'shuffle flag (True means shuffle)'(bool)
        'img_feat_dim'(int)
        'average norm factor for image feature'(float)
        '''
        gc.disable() #disable automatic garbage collection
        str_conf_args = kwargs['load_data_args'].strip().split()
        dict_file_path = str_conf_args[0]
        flag_randseq = bool(int(str_conf_args[1]))
        img_feat_dim = int(str_conf_args[2])
        feat_avg_norm_factor = float(str_conf_args[3])
        epoch_num = int(str_conf_args[4])

        logger.info('Dictionary Path: %s' % dict_file_path)
        logger.info('Flag shuffle: %r' % flag_randseq)
        logger.info('Image dimension: %d' % img_feat_dim)
        logger.info('Average norm factor for image feature: %.4f' %
                    feat_avg_norm_factor)

        if os.path.isfile(dict_file_path):
            dct = np.load(open(dict_file_path, 'rb'))
            if dct.get('$$S$$', 0) == 0:
                dct['$$S$$'] = len(dct)
            if dct.get('$$E$$', 0) == 0:
                dct['$$E$$'] = len(dct)
            logger.info('Dictionary loaded with %d words' % len(dct))
        else:
            logger.fatal(
                'Dictionary file [%s] does not exist!' % dict_file_path)

        if len(file_list) == 0:
            logger.fatal('No annotation file!')
        else:
            logger.info('There are %d annotation files' % len(file_list))

        tmp_num_files = 0

        fname = file_list[0].strip()
        if os.path.isfile(fname):
            logger.debug("fname %s" % fname)
            img_sen_arr = pickle.load(open(fname, 'rb'))
            tmp_num_files += 1
        else:
            logger.warn('Annotation file %s missing!' % fname)

        # img_sen_arr = img_sen_arr[0:100] #DEBUG!!MUST COMMENT OUT!!

        self.file_list = list(file_list)
        self.img_sen_arr = img_sen_arr
        self.dct = dct
        self.img_feat_dim = img_feat_dim
        self.num_in_batch = 0
        self.batch_num = 0
        self.total_batch_num = len(file_list)
        self.feat_avg_norm_factor = feat_avg_norm_factor
        self.epoch_num = epoch_num
        self.total_epoch = 0
        logger.info('SentenceImageFeatDataProvider Initialization finished')

    def shuffle(self):
        '''
          Description: set shuffle operation
        '''
        pass

    def reset(self):
        '''
          Description: execute some reset operation
          in each pass.
        '''
        pass

    def getHeader(self):
        '''
            Description: Get header info
            Return format:
                slot_num  // Total count of all slots
                use_sequence_flag // 1 if use sequence, 0 if not use sequence
                [(slot_type_1, slot_dim_1),  // A list of all slot type and dim
                 ...,
                 (slot_type_N, slot_dim_N)
                ]
            Note: The type of all header data all int32_t
        '''
        # logger.info('DEBUG::getHeader') #DEBUG
        num_slots = 4
        use_sequence = 1

        ret = ''
        ret += struct.pack('ii', num_slots, use_sequence)
        ret += struct.pack('ii', 0, self.img_feat_dim)  # image feature slot
        ret += struct.pack('ii', 3, len(self.dct))  # question sentence slot
        ret += struct.pack('ii', 3, len(self.dct))  # current word slot
        ret += struct.pack('ii', 3, len(self.dct))  # next word slot
        # logger.info("img feat dim: %d, dct length : %d " %(self.img_feat_dim, len(self.dct)))
        # logger.info('Header: %d slots, %d use_sequence' % (num_slots,
        # use_sequence))
        return ret

    def getNextBatch(self, batch_size):
        '''
            Description: Get a batch of samples
            Return format:
                batch_size // specify the return value of getNextBatch
                // The flowing data was organise by slot, and the order of
                // slots was the same as in header. Different slot type has
                // diffent structure of data.
                // Now there are 4 kinds of data type.
                // 1 Dense type
                sample_num   // sample num in dense type slot
                value0 value1 ... valueN  // dense type slot values
                // 2 Sparse type with weight value
                sample_num
                offset0 offset1 ... offsetN // the offset of sample in values.
                                        // Use CSR format to store sparse value.
                length value0 value1 ... valueN
                length weight0  weight1 … weightN
                // 3 Sparse type without weight value type
                sample_num
                offset0 offset1 ... offsetN
                length value0 value1 ... valueN
                // 4 Index type
                sample_num
                value0 value1 ... valueN
                // If you use sequence, you should specify  sequences offsets in
                // each slot.
                slot1_sequence_num sequence_pos0  ... sequence_posN
                ...
                slotN_sequence_num sequence_pos0  ... sequence_posN
        '''
        # logger.info('DEBUG::getNextBatch') #DEBUG
        # if we have scan the whole data once
        # if num in batch == 0, means a new batch needed
        # logger.info("batch size %d" % batch_size)
        logger.info("epoch_num:%d, num_in_batch:%d, total_batch_num:%d, total_epoch:%d" % (self.epoch_num, self.num_in_batch, self.total_batch_num, self.total_epoch))
        if self.num_in_batch == -1:
            self.num_in_batch = 0
            if self.epoch_num % self.total_batch_num == 0:
                random.shuffle(self.file_list)
                return struct.pack('i', 0)
        begin_time = time.time()
        if self.num_in_batch == 0:
            # logger.info("Begin batch %d" %(self.epoch_num))
            fname = self.file_list[self.epoch_num %
                                   self.total_batch_num].strip()
            if self.epoch_num % self.total_batch_num == 0:
                self.total_epoch += 1
                logger.info("Begin epoch %d" % self.total_epoch)
            if os.path.isfile(fname):
                logger.debug("fname %s" % fname)
                self.img_sen_arr = pickle.load(open(fname, 'rb'))
                random.shuffle(self.img_sen_arr)
            else:
                logger.warn('Annotation file %s missing!' % fname)
            self.epoch_num += 1

        start_pos = self.num_in_batch
        image_number = 0
        end_pos = min(self.num_in_batch + batch_size, len(self.img_sen_arr))
        q_length = 0
        a_length = 0

        for check_one in self.img_sen_arr[start_pos:end_pos]:
            if max(q_length + len(check_one['coding_q']) - 1, a_length + len(check_one['coding_a']) - 1) < batch_size:
                image_number += 1
                a_length += len(check_one['coding_a'])
                q_length += len(check_one['coding_q'])
            else:
                break

        end_pos = start_pos + image_number
        batch = self.img_sen_arr[start_pos:end_pos]

        if end_pos >= len(self.img_sen_arr):
            self.num_in_batch = -1
            self.batch_num += 1
        else:
            self.num_in_batch = end_pos

        ret_img = ''
        ret_qw = ''
        ret_cw = ''
        ret_nw = ''
        ret_seq = ''
        ret_q_seq = ''
        img_seq = ''
        num_samples_batch = 0
        num_sequence_batch = 0
        num_samples_q_batch = 0

        for i in xrange(len(batch)):
            one_sample = batch[i]
            img_feat = one_sample['feature'] * self.feat_avg_norm_factor
            question = one_sample['coding_q']
            answer = one_sample['coding_a']
            if answer[0] == self.dct.get('$$S$$', -1):
                answer = answer[1:]
            if answer[-1] == self.dct.get('$$E$$', -1):
                answer = answer[:-1]
            if question[0] == self.dct.get('$$S$$', -1):
                question = question[1:]
            if question[-1] == self.dct.get('$$E$$', -1):
                question = question[:-1]
            answer.insert(0, self.dct.get('$$S$$', -1))
            answer.append(self.dct.get('$$E$$', -1))

            answer_len = len(answer) - 1
            ret_qw += np.array(question, dtype=np.int32).tostring()
            ret_cw += np.array(answer[:-1], dtype=np.int32).tostring()
            ret_nw += np.array(answer[1:], dtype=np.int32).tostring()
            ret_img += img_feat.tostring()

            # logger.debug("len: %d, sentence: %s, num_samples_batch: %d" %
            # (len(sentence),' '.join(sentence),num_samples_batch))
            ret_seq = ret_seq + struct.pack('i', num_samples_batch)
            ret_q_seq = ret_q_seq + struct.pack('i', num_samples_q_batch)
            img_seq = img_seq + struct.pack('i', num_sequence_batch)
            num_samples_q_batch += len(question)
            num_samples_batch += answer_len
            num_sequence_batch += 1
            logger.debug('finish samples %d' % num_samples_batch)
            logger.debug('question:%s' % ''.join(one_sample['question']))
            logger.debug('answer:%s' % ''.join(one_sample['answer']))

        # logger.info("img:%d, cw:%d, nw:%d, ret_seq:%d" %
        # (len(ret_img)/4/self.img_feat_dim, len(ret_cw)/4, len(ret_nw)/4,
        # len(ret_seq)/4))
        ret_img = struct.pack('i', num_sequence_batch) + ret_img
        ret_qw = struct.pack('i', num_samples_q_batch) + ret_qw
        ret_cw = struct.pack('i', num_samples_batch) + ret_cw
        ret_nw = struct.pack('i', num_samples_batch) + ret_nw
        img_seq = struct.pack('i', num_sequence_batch) + img_seq
        ret_seq = struct.pack('i', num_sequence_batch) + ret_seq
        ret_q_seq = struct.pack('i', num_sequence_batch) + ret_q_seq
        ret = struct.pack('i', num_samples_batch) + ret_img + \
            ret_qw + ret_cw + ret_nw + img_seq + ret_q_seq + ret_seq * 2
        end_time = time.time()
        # logger.info("Begin: %f , end: %f, cost:%f" % (begin_time, end_time, end_time - begin_time))
        # logger.info('Pass one bach, num_samples %d, sequence num %d, length %d' % (num_samples_batch, num_sequence_batch,len(ret))) # DEBUG
        # logger.info("img:%d, cw:%d, nw:%d, ret_seq:%d " %
        # (len(ret_img)/4/self.total_pixels, len(ret_cw)/4, len(ret_nw)/4,
        # len(ret_seq)/4))
        return ret

class pyDataImageFeatureQA2:

    def __init__(self, *file_list, **kwargs):
        '''
        Description: Init with a list of data file
        file_list is the name list of input files.
        kwargs["load_data_args"] is the value of 'load_data_args' which can be set in config.

        kwargs["load_data_args"] is organized as follows
        '', () indicates the name and type of the args
        Each args is seperated by a space:
        'dictionary path'(str)
        'shuffle flag (True means shuffle)'(bool)
        'img_feat_dim'(int)
        'average norm factor for image feature'(float)
        '''
        gc.disable()
        str_conf_args = kwargs['load_data_args'].strip().split()
        dict_file_path = str_conf_args[0]
        flag_randseq = bool(int(str_conf_args[1]))
        img_feat_dim = int(str_conf_args[2])
        feat_avg_norm_factor = float(str_conf_args[3])
        epoch_num = int(str_conf_args[4])

        logger.info('Dictionary Path: %s' % dict_file_path)
        logger.info('Flag shuffle: %r' % flag_randseq)
        logger.info('Image dimension: %d' % img_feat_dim)
        logger.info('Average norm factor for image feature: %.4f' %
                    feat_avg_norm_factor)

        if os.path.isfile(dict_file_path):
            dct = np.load(open(dict_file_path, 'rb'))
            if dct.get('$$S$$', 0) == 0:
                dct['$$S$$'] = len(dct)
            if dct.get('$$E$$', 0) == 0:
                dct['$$E$$'] = len(dct)
            logger.info('Dictionary loaded with %d words' % len(dct))
        else:
            logger.fatal(
                'Dictionary file [%s] does not exist!' % dict_file_path)

        if len(file_list) == 0:
            logger.fatal('No annotation file!')
        else:
            logger.info('There are %d annotation files' % len(file_list))

        tmp_num_files = 0

        fname = file_list[0].strip()
        if os.path.isfile(fname):
            logger.debug("fname %s" % fname)
            img_sen_arr = pickle.load(open(fname, 'rb'))
            tmp_num_files += 1
        else:
            logger.warn('Annotation file %s missing!' % fname)

        # img_sen_arr = img_sen_arr[0:100] #DEBUG!!MUST COMMENT OUT!!

        self.file_list = list(file_list)
        self.img_sen_arr = img_sen_arr
        self.dct = dct
        self.img_feat_dim = img_feat_dim
        self.num_in_batch = 0
        self.batch_num = 0
        self.total_batch_num = len(file_list)
        self.feat_avg_norm_factor = feat_avg_norm_factor
        self.epoch_num = epoch_num
        self.total_epoch = 0
        logger.info('SentenceImageFeatDataProvider Initialization finished')

    def shuffle(self):
        '''
          Description: set shuffle operation
        '''
        pass

    def reset(self):
        '''
          Description: execute some reset operation
          in each pass.
        '''
        pass

    def getHeader(self):
        '''
            Description: Get header info
            Return format:
                slot_num  // Total count of all slots
                use_sequence_flag // 1 if use sequence, 0 if not use sequence
                [(slot_type_1, slot_dim_1),  // A list of all slot type and dim
                 ...,
                 (slot_type_N, slot_dim_N)
                ]
            Note: The type of all header data all int32_t
        '''
        # logger.info('DEBUG::getHeader') #DEBUG
        num_slots = 4
        use_sequence = 1

        ret = ''
        ret += struct.pack('ii', num_slots, use_sequence)
        ret += struct.pack('ii', 0, self.img_feat_dim)  # image feature slot
        ret += struct.pack('ii', 3, len(self.dct))  # question sentence slot
        ret += struct.pack('ii', 3, len(self.dct))  # current word slot
        ret += struct.pack('ii', 3, len(self.dct))  # next word slot
        # logger.info("img feat dim: %d, dct length : %d " %(self.img_feat_dim, len(self.dct)))
        # logger.info('Header: %d slots, %d use_sequence' % (num_slots,
        # use_sequence))
        return ret

    def getNextBatch(self, batch_size):
        '''
            Description: Get a batch of samples
            Return format:
                batch_size // specify the return value of getNextBatch
                // The flowing data was organise by slot, and the order of
                // slots was the same as in header. Different slot type has
                // diffent structure of data.
                // Now there are 4 kinds of data type.
                // 1 Dense type
                sample_num   // sample num in dense type slot
                value0 value1 ... valueN  // dense type slot values
                // 2 Sparse type with weight value
                sample_num
                offset0 offset1 ... offsetN // the offset of sample in values.
                                        // Use CSR format to store sparse value.
                length value0 value1 ... valueN
                length weight0  weight1 … weightN
                // 3 Sparse type without weight value type
                sample_num
                offset0 offset1 ... offsetN
                length value0 value1 ... valueN
                // 4 Index type
                sample_num
                value0 value1 ... valueN
                // If you use sequence, you should specify  sequences offsets in
                // each slot.
                slot1_sequence_num sequence_pos0  ... sequence_posN
                ...
                slotN_sequence_num sequence_pos0  ... sequence_posN
        '''
        # logger.info('DEBUG::getNextBatch') #DEBUG
        # if we have scan the whole data once
        # if num in batch == 0, means a new batch needed
        # logger.info("batch size %d" % batch_size)
        if self.num_in_batch == -1:
            self.num_in_batch = 0
            if self.epoch_num % self.total_batch_num == 0:
                random.shuffle(self.file_list)
                return struct.pack('i', 0)
        begin_time = time.time()
        if self.num_in_batch == 0:
            # logger.info("Begin batch %d" %(self.epoch_num))
            fname = self.file_list[self.epoch_num %
                                   self.total_batch_num].strip()
            if self.epoch_num % self.total_batch_num == 0:
                self.total_epoch += 1
                logger.info("Begin epoch %d" % self.total_epoch)
            if os.path.isfile(fname):
                logger.debug("fname %s" % fname)
                self.img_sen_arr = pickle.load(open(fname, 'rb'))
                random.shuffle(self.img_sen_arr)
            else:
                logger.warn('Annotation file %s missing!' % fname)
            self.epoch_num += 1

        start_pos = self.num_in_batch
        image_number = 0
        end_pos = min(self.num_in_batch + batch_size, len(self.img_sen_arr))
        q_length = 0
        a_length = 0

        for check_one in self.img_sen_arr[start_pos:end_pos]:
            if max(q_length + len(check_one['coding_q']) - 1, a_length + len(check_one['coding_a']) - 1) < batch_size:
                image_number += 1
                a_length += len(check_one['coding_a'])
                q_length += len(check_one['coding_q'])
            else:
                break

        end_pos = start_pos + image_number
        batch = self.img_sen_arr[start_pos:end_pos]

        if end_pos >= len(self.img_sen_arr):
            self.num_in_batch = -1
            self.batch_num += 1
        else:
            self.num_in_batch = end_pos

        ret_img = ''
        ret_qw = ''
        ret_cw = ''
        ret_nw = ''
        ret_seq = ''
        ret_q_seq = ''
        img_seq = ''
        num_samples_batch = 0
        num_sequence_batch = 0
        num_samples_q_batch = 0

        for i in xrange(len(batch)):
            one_sample = batch[i]
            img_feat = one_sample['feature'] * self.feat_avg_norm_factor
            question = one_sample['coding_q'][:-1]
            answer = one_sample['coding_a'][:-1]
            answer.insert(0, len(self.dct) - 1)
            answer_len = len(answer) - 1
            ret_qw += np.array(question, dtype=np.int32).tostring()
            ret_cw += np.array(answer[:-1], dtype=np.int32).tostring()
            ret_nw += np.array(answer[1:], dtype=np.int32).tostring()
            ret_img += img_feat.tostring()

            # logger.debug("len: %d, sentence: %s, num_samples_batch: %d" %
            # (len(sentence),' '.join(sentence),num_samples_batch))
            ret_seq = ret_seq + struct.pack('i', num_samples_batch)
            ret_q_seq = ret_q_seq + struct.pack('i', num_samples_q_batch)
            img_seq = img_seq + struct.pack('i', num_sequence_batch)
            num_samples_q_batch += len(question)
            num_samples_batch += answer_len
            num_sequence_batch += 1
            logger.debug('finish samples %d' % num_samples_batch)
            logger.debug('question:%s' % ''.join(one_sample['question']))
            logger.debug('answer:%s' % ''.join(one_sample['answer']))

        # logger.info("img:%d, cw:%d, nw:%d, ret_seq:%d" %
        # (len(ret_img)/4/self.img_feat_dim, len(ret_cw)/4, len(ret_nw)/4,
        # len(ret_seq)/4))
        ret_img = struct.pack('i', num_sequence_batch) + ret_img
        ret_qw = struct.pack('i', num_samples_q_batch) + ret_qw
        ret_cw = struct.pack('i', num_samples_batch) + ret_cw
        ret_nw = struct.pack('i', num_samples_batch) + ret_nw
        img_seq = struct.pack('i', num_sequence_batch) + img_seq
        ret_seq = struct.pack('i', num_sequence_batch) + ret_seq
        ret_q_seq = struct.pack('i', num_sequence_batch) + ret_q_seq
        ret = struct.pack('i', num_samples_batch) + ret_img + \
            ret_qw + ret_cw + ret_nw + img_seq + ret_q_seq + ret_seq * 2
        end_time = time.time()
        # logger.info("Begin: %f , end: %f, cost:%f" % (begin_time, end_time, end_time - begin_time))
        # logger.info('Pass one bach, num_samples %d, sequence num %d, length %d' % (num_samples_batch, num_sequence_batch,len(ret))) # DEBUG
        # logger.info("img:%d, cw:%d, nw:%d, ret_seq:%d " %
        # (len(ret_img)/4/self.total_pixels, len(ret_cw)/4, len(ret_nw)/4,
        # len(ret_seq)/4))
        return ret


class pyDataImageFeatureQA3:

    def __init__(self, *file_list, **kwargs):
        '''
        Description: Init with a list of data file
        file_list is the name list of input files.
        kwargs["load_data_args"] is the value of 'load_data_args' which can be set in config.

        kwargs["load_data_args"] is organized as follows
        '', () indicates the name and type of the args
        Each args is seperated by a space:
        'dictionary path'(str)
        'shuffle flag (True means shuffle)'(bool)
        'img_feat_dim'(int)
        'average norm factor for image feature'(float)
        '''
        gc.disable()
        str_conf_args = kwargs['load_data_args'].strip().split()
        dict_file_path = str_conf_args[0]
        flag_randseq = bool(int(str_conf_args[1]))
        img_feat_dim = int(str_conf_args[2])
        feat_avg_norm_factor = float(str_conf_args[3])
        epoch_num = int(str_conf_args[4])

        logger.info('Dictionary Path: %s' % dict_file_path)
        logger.info('Flag shuffle: %r' % flag_randseq)
        logger.info('Image dimension: %d' % img_feat_dim)
        logger.info('Average norm factor for image feature: %.4f' %
                    feat_avg_norm_factor)

        if os.path.isfile(dict_file_path):
            dct = np.load(open(dict_file_path, 'rb'))
            if dct.get('$$S$$', 0) == 0:
                dct['$$S$$'] = len(dct)
            if dct.get('$$E$$', 0) == 0:
                dct['$$E$$'] = len(dct)
            logger.info('Dictionary loaded with %d words' % len(dct))
        else:
            logger.fatal(
                'Dictionary file [%s] does not exist!' % dict_file_path)

        if len(file_list) == 0:
            logger.fatal('No annotation file!')
        else:
            logger.info('There are %d annotation files' % len(file_list))

        tmp_num_files = 0

        fname = file_list[0].strip()
        if os.path.isfile(fname):
            logger.debug("fname %s" % fname)
            img_sen_arr = pickle.load(open(fname, 'rb'))
            tmp_num_files += 1
        else:
            logger.warn('Annotation file %s missing!' % fname)

        # img_sen_arr = img_sen_arr[0:100] #DEBUG!!MUST COMMENT OUT!!

        self.file_list = list(file_list)
        self.img_sen_arr = img_sen_arr
        self.dct = dct
        self.img_feat_dim = img_feat_dim
        self.num_in_batch = 0
        self.batch_num = 0
        self.total_batch_num = len(file_list)
        self.feat_avg_norm_factor = feat_avg_norm_factor
        self.epoch_num = epoch_num
        self.total_epoch = 0
        logger.info('SentenceImageFeatDataProvider Initialization finished')

    def shuffle(self):
        '''
          Description: set shuffle operation
        '''
        pass

    def reset(self):
        '''
          Description: execute some reset operation
          in each pass.
        '''
        pass

    def getHeader(self):
        '''
            Description: Get header info
            Return format:
                slot_num  // Total count of all slots
                use_sequence_flag // 1 if use sequence, 0 if not use sequence
                [(slot_type_1, slot_dim_1),  // A list of all slot type and dim
                 ...,
                 (slot_type_N, slot_dim_N)
                ]
            Note: The type of all header data all int32_t
        '''
        # logger.info('DEBUG::getHeader') #DEBUG
        num_slots = 4
        use_sequence = 1

        ret = ''
        ret += struct.pack('ii', num_slots, use_sequence)
        ret += struct.pack('ii', 0, self.img_feat_dim)  # image feature slot
        ret += struct.pack('ii', 3, len(self.dct))  # question sentence slot
        ret += struct.pack('ii', 3, len(self.dct))  # current word slot
        ret += struct.pack('ii', 3, len(self.dct))  # next word slot
        # logger.info("img feat dim: %d, dct length : %d " %(self.img_feat_dim, len(self.dct)))
        # logger.info('Header: %d slots, %d use_sequence' % (num_slots,
        # use_sequence))
        return ret

    def getNextBatch(self, batch_size):
        '''
            Description: Get a batch of samples
            Return format:
                batch_size // specify the return value of getNextBatch
                // The flowing data was organise by slot, and the order of
                // slots was the same as in header. Different slot type has
                // diffent structure of data.
                // Now there are 4 kinds of data type.
                // 1 Dense type
                sample_num   // sample num in dense type slot
                value0 value1 ... valueN  // dense type slot values
                // 2 Sparse type with weight value
                sample_num
                offset0 offset1 ... offsetN // the offset of sample in values.
                                        // Use CSR format to store sparse value.
                length value0 value1 ... valueN
                length weight0  weight1 … weightN
                // 3 Sparse type without weight value type
                sample_num
                offset0 offset1 ... offsetN
                length value0 value1 ... valueN
                // 4 Index type
                sample_num
                value0 value1 ... valueN
                // If you use sequence, you should specify  sequences offsets in
                // each slot.
                slot1_sequence_num sequence_pos0  ... sequence_posN
                ...
                slotN_sequence_num sequence_pos0  ... sequence_posN
        '''
        # logger.info('DEBUG::getNextBatch') #DEBUG
        # if we have scan the whole data once
        # if num in batch == 0, means a new batch needed
        # logger.info("batch size %d" % batch_size)
        if self.num_in_batch == -1:
            self.num_in_batch = 0
            if self.epoch_num % self.total_batch_num == 0:
                random.shuffle(self.file_list)
                return struct.pack('i', 0)
        begin_time = time.time()
        if self.num_in_batch == 0:
            # logger.info("Begin batch %d" %(self.epoch_num))
            fname = self.file_list[self.epoch_num %
                                   self.total_batch_num].strip()
            if self.epoch_num % self.total_batch_num == 0:
                self.total_epoch += 1
                logger.info("Begin epoch %d" % self.total_epoch)
            if os.path.isfile(fname):
                logger.debug("fname %s" % fname)
                self.img_sen_arr = pickle.load(open(fname, 'rb'))
                random.shuffle(self.img_sen_arr)
            else:
                logger.warn('Annotation file %s missing!' % fname)
            self.epoch_num += 1

        start_pos = self.num_in_batch
        image_number = 0
        end_pos = min(self.num_in_batch + batch_size, len(self.img_sen_arr))
        q_length = 0
        a_length = 0

        for check_one in self.img_sen_arr[start_pos:end_pos]:
            if max(q_length + len(check_one['coding_q']) - 1, a_length + len(check_one['coding_a']) - 1) < batch_size:
                image_number += 1
                a_length += len(check_one['coding_a'])
                q_length += len(check_one['coding_q'])
            else:
                break

        end_pos = start_pos + image_number
        batch = self.img_sen_arr[start_pos:end_pos]

        if end_pos >= len(self.img_sen_arr):
            self.num_in_batch = -1
            self.batch_num += 1
        else:
            self.num_in_batch = end_pos

        ret_img = ''
        ret_qw = ''
        ret_cw = ''
        ret_nw = ''
        ret_seq = ''
        ret_q_seq = ''
        img_seq = ''
        num_samples_batch = 0
        num_sequence_batch = 0
        num_samples_q_batch = 0

        for i in xrange(len(batch)):
            one_sample = batch[i]
            img_feat = one_sample['feature'] * self.feat_avg_norm_factor
            question = one_sample['coding_q']
            answer = one_sample['coding_a']
            answer_len = len(answer) - 1
            ret_qw += np.array(question, dtype=np.int32).tostring()
            ret_cw += np.array(answer[:-1], dtype=np.int32).tostring()
            ret_nw += np.array(answer[1:], dtype=np.int32).tostring()
            ret_img += img_feat.tostring()

            # logger.debug("len: %d, sentence: %s, num_samples_batch: %d" %
            # (len(sentence),' '.join(sentence),num_samples_batch))
            ret_seq = ret_seq + struct.pack('i', num_samples_batch)
            ret_q_seq = ret_q_seq + struct.pack('i', num_samples_q_batch)
            img_seq = img_seq + struct.pack('i', num_sequence_batch)
            num_samples_q_batch += len(question)
            num_samples_batch += answer_len
            num_sequence_batch += 1
            logger.debug('finish samples %d' % num_samples_batch)
            logger.debug('question:%s' % ''.join(one_sample['question']))
            logger.debug('answer:%s' % ''.join(one_sample['answer']))

        # logger.info("img:%d, cw:%d, nw:%d, ret_seq:%d" %
        # (len(ret_img)/4/self.img_feat_dim, len(ret_cw)/4, len(ret_nw)/4,
        # len(ret_seq)/4))
        ret_img = struct.pack('i', num_sequence_batch) + ret_img
        ret_qw = struct.pack('i', num_samples_q_batch) + ret_qw
        ret_cw = struct.pack('i', num_samples_batch) + ret_cw
        ret_nw = struct.pack('i', num_samples_batch) + ret_nw
        img_seq = struct.pack('i', num_sequence_batch) + img_seq
        ret_seq = struct.pack('i', num_sequence_batch) + ret_seq
        ret_q_seq = struct.pack('i', num_sequence_batch) + ret_q_seq
        ret = struct.pack('i', num_samples_batch) + ret_img + \
            ret_qw + ret_cw + ret_nw + img_seq + ret_q_seq + ret_seq * 2
        end_time = time.time()
        # logger.info("Begin: %f , end: %f, cost:%f" % (begin_time, end_time, end_time - begin_time))
        # logger.info('Pass one bach, num_samples %d, sequence num %d, length %d' % (num_samples_batch, num_sequence_batch,len(ret))) # DEBUG
        # logger.info("img:%d, cw:%d, nw:%d, ret_seq:%d " %
        # (len(ret_img)/4/self.total_pixels, len(ret_cw)/4, len(ret_nw)/4,
        # len(ret_seq)/4))
        return ret


class pyDataQA:

    def __init__(self, *file_list, **kwargs):
        '''
        Description: Init with a list of data file
        file_list is the name list of input files.
        kwargs["load_data_args"] is the value of 'load_data_args' which can be set in config.

        kwargs["load_data_args"] is organized as follows
        '', () indicates the name and type of the args
        Each args is seperated by a space:
        'dictionary path'(str)
        'shuffle flag (True means shuffle)'(bool)
        'img_feat_dim'(int)
        'average norm factor for image feature'(float)
        '''
        gc.disable()
        str_conf_args = kwargs['load_data_args'].strip().split()
        dict_file_path = str_conf_args[0]
        flag_randseq = bool(int(str_conf_args[1]))
        epoch_num = int(str_conf_args[2])

        logger.info('Dictionary Path: %s' % dict_file_path)
        logger.info('Flag shuffle: %r' % flag_randseq)

        if os.path.isfile(dict_file_path):
            dct = np.load(open(dict_file_path, 'rb'))
            if dct.get('$$E$$', 0) == 0:
                dct['$$E$$'] = len(dct)
            if dct.get('$$S$$', 0) == 0:
                dct['$$S$$'] = len(dct)
            logger.info('Dictionary loaded with %d words' % len(dct))
        else:
            logger.fatal(
                'Dictionary file [%s] does not exist!' % dict_file_path)

        if len(file_list) == 0:
            logger.fatal('No annotation file!')
        else:
            logger.info('There are %d annotation files' % len(file_list))

        tmp_num_files = 0

        fname = file_list[0].strip()
        if os.path.isfile(fname):
            logger.debug("fname %s" % fname)
            img_sen_arr = pickle.load(open(fname, 'rb'))
            tmp_num_files += 1
        else:
            logger.warn('Annotation file %s missing!' % fname)

        # img_sen_arr = img_sen_arr[0:100] #DEBUG!!MUST COMMENT OUT!!

        self.file_list = list(file_list)
        self.img_sen_arr = img_sen_arr
        self.dct = dct
        self.num_in_batch = 0
        self.batch_num = 0
        self.total_batch_num = len(file_list)
        self.epoch_num = epoch_num
        self.total_epoch = 0
        logger.info('SentenceImageFeatDataProvider Initialization finished')

    def shuffle(self):
        '''
          Description: set shuffle operation
        '''
        pass

    def reset(self):
        '''
          Description: execute some reset operation
          in each pass.
        '''
        pass

    def getHeader(self):
        '''
            Description: Get header info
            Return format:
                slot_num  // Total count of all slots
                use_sequence_flag // 1 if use sequence, 0 if not use sequence
                [(slot_type_1, slot_dim_1),  // A list of all slot type and dim
                 ...,
                 (slot_type_N, slot_dim_N)
                ]
            Note: The type of all header data all int32_t
        '''
        # logger.info('DEBUG::getHeader') #DEBUG
        num_slots = 3
        use_sequence = 1

        ret = ''
        ret += struct.pack('ii', num_slots, use_sequence)
        ret += struct.pack('ii', 3, len(self.dct))  # question sentence slot
        ret += struct.pack('ii', 3, len(self.dct))  # current word slot
        ret += struct.pack('ii', 3, len(self.dct))  # next word slot
        # logger.info("img feat dim: %d, dct length : %d " %(self.img_feat_dim, len(self.dct)))
        # logger.info('Header: %d slots, %d use_sequence' % (num_slots,
        # use_sequence))
        return ret

    def getNextBatch(self, batch_size):
        '''
            Description: Get a batch of samples
            Return format:
                batch_size // specify the return value of getNextBatch
                // The flowing data was organise by slot, and the order of
                // slots was the same as in header. Different slot type has
                // diffent structure of data.
                // Now there are 4 kinds of data type.
                // 1 Dense type
                sample_num   // sample num in dense type slot
                value0 value1 ... valueN  // dense type slot values
                // 2 Sparse type with weight value
                sample_num
                offset0 offset1 ... offsetN // the offset of sample in values.
                                        // Use CSR format to store sparse value.
                length value0 value1 ... valueN
                length weight0  weight1 … weightN
                // 3 Sparse type without weight value type
                sample_num
                offset0 offset1 ... offsetN
                length value0 value1 ... valueN
                // 4 Index type
                sample_num
                value0 value1 ... valueN
                // If you use sequence, you should specify  sequences offsets in
                // each slot.
                slot1_sequence_num sequence_pos0  ... sequence_posN
                ...
                slotN_sequence_num sequence_pos0  ... sequence_posN
        '''
        # logger.info('DEBUG::getNextBatch') #DEBUG
        # if we have scan the whole data once
        # if num in batch == 0, means a new batch needed
        # logger.info("batch size %d" % batch_size)
        if self.num_in_batch == -1:
            self.num_in_batch = 0
            if self.epoch_num % self.total_batch_num == 0:
                random.shuffle(self.file_list)
                return struct.pack('i', 0)
        begin_time = time.time()
        if self.num_in_batch == 0:
            # logger.info("Begin batch %d" %(self.epoch_num))
            fname = self.file_list[self.epoch_num %
                                   self.total_batch_num].strip()
            if self.epoch_num % self.total_batch_num == 0:
                self.total_epoch += 1
                logger.info("Begin epoch %d" % self.total_epoch)
            if os.path.isfile(fname):
                logger.debug("fname %s" % fname)
                self.img_sen_arr = pickle.load(open(fname, 'rb'))
                random.shuffle(self.img_sen_arr)
            else:
                logger.warn('Annotation file %s missing!' % fname)
            self.epoch_num += 1

        start_pos = self.num_in_batch
        image_number = 0
        end_pos = min(self.num_in_batch + batch_size, len(self.img_sen_arr))
        q_length = 0
        a_length = 0

        for check_one in self.img_sen_arr[start_pos:end_pos]:
            if max(q_length + len(check_one['coding_q']) - 1, a_length + len(check_one['coding_a']) - 1) < batch_size:
                image_number += 1
                a_length += len(check_one['coding_a'])
                q_length += len(check_one['coding_q'])
            else:
                break

        end_pos = start_pos + image_number
        batch = self.img_sen_arr[start_pos:end_pos]

        if end_pos >= len(self.img_sen_arr):
            self.num_in_batch = -1
            self.batch_num += 1
        else:
            self.num_in_batch = end_pos

        ret_qw = ''
        ret_cw = ''
        ret_nw = ''
        ret_seq = ''
        ret_q_seq = ''
        num_samples_batch = 0
        num_sequence_batch = 0
        num_samples_q_batch = 0

        for i in xrange(len(batch)):
            one_sample = batch[i]
            question = one_sample['coding_q']
            answer = one_sample['coding_a']
            answer.insert(0, len(self.dct) - 1)
            answer_len = len(answer) - 1
            ret_qw += np.array(question, dtype=np.int32).tostring()
            ret_cw += np.array(answer[:-1], dtype=np.int32).tostring()
            ret_nw += np.array(answer[1:], dtype=np.int32).tostring()

            # logger.debug("len: %d, sentence: %s, num_samples_batch: %d" %
            # (len(sentence),' '.join(sentence),num_samples_batch))
            ret_seq = ret_seq + struct.pack('i', num_samples_batch)
            ret_q_seq = ret_q_seq + struct.pack('i', num_samples_q_batch)
            num_samples_q_batch += len(question)
            num_samples_batch += answer_len
            num_sequence_batch += 1
            logger.debug('finish samples %d' % num_samples_batch)
            logger.debug('question:%s' % ''.join(one_sample['question']))
            logger.debug('answer:%s' % ''.join(one_sample['answer']))

        # logger.info("img:%d, cw:%d, nw:%d, ret_seq:%d" %
        # (len(ret_img)/4/self.img_feat_dim, len(ret_cw)/4, len(ret_nw)/4,
        # len(ret_seq)/4))
        ret_qw = struct.pack('i', num_samples_q_batch) + ret_qw
        ret_cw = struct.pack('i', num_samples_batch) + ret_cw
        ret_nw = struct.pack('i', num_samples_batch) + ret_nw
        ret_seq = struct.pack('i', num_sequence_batch) + ret_seq
        ret_q_seq = struct.pack('i', num_sequence_batch) + ret_q_seq
        ret = struct.pack('i', num_samples_batch) + \
            ret_qw + ret_cw + ret_nw + ret_q_seq + ret_seq * 2
        end_time = time.time()
        # logger.info("Begin: %f , end: %f, cost:%f" % (begin_time, end_time, end_time - begin_time))
        # logger.info('Pass one bach, num_samples %d, sequence num %d, length %d' % (num_samples_batch, num_sequence_batch,len(ret))) # DEBUG
        # logger.info("img:%d, cw:%d, nw:%d, ret_seq:%d " %
        # (len(ret_img)/4/self.total_pixels, len(ret_cw)/4, len(ret_nw)/4,
        # len(ret_seq)/4))
        return ret


class pyDataImageQA:

    def __init__(self, *file_list, **kwargs):
        '''
        Description: Init with a list of data file
        file_list is the name list of input files.
        kwargs["load_data_args"] is the value of 'load_data_args' which can be set in config.

        kwargs["load_data_args"] is organized as follows
        '', () indicates the name and type of the args
        Each args is seperated by a space:
        'dictionary path'(str)
        'shuffle flag (True means shuffle)'(bool)
        'img_feat_dim'(int)
        'average norm factor for image feature'(float)
        '''
        gc.disable()
        str_conf_args = kwargs['load_data_args'].strip().split()
        dict_file_path = str_conf_args[0]
        flag_randseq = bool(int(str_conf_args[1]))
        mean_file_path = str_conf_args[2]
        epoch_num = int(str_conf_args[3])

        logger.info('Dictionary Path: %s' % dict_file_path)
        logger.info('Flag shuffle: %r' % flag_randseq)

        if os.path.isfile(dict_file_path):
            dct = np.load(open(dict_file_path, 'rb'))
            if dct.get('$$E$$', 0) == 0:
                dct['$$E$$'] = len(dct)
            if dct.get('$$S$$', 0) == 0:
                dct['$$S$$'] = len(dct)
            logger.info('Dictionary loaded with %d words' % len(dct))
        else:
            logger.fatal(
                'Dictionary file [%s] does not exist!' % dict_file_path)

        if len(file_list) == 0:
            logger.fatal('No annotation file!')
        else:
            logger.info('There are %d annotation files' % len(file_list))

        tmp_num_files = 0

        fname = file_list[0].strip()
        if os.path.isfile(fname):
            logger.debug("fname %s" % fname)
            img_sen_arr = pickle.load(open(fname, 'rb'))
            tmp_num_files += 1
        else:
            logger.warn('Annotation file %s missing!' % fname)

        # img_sen_arr = img_sen_arr[0:100] #DEBUG!!MUST COMMENT OUT!!

        self.file_list = list(file_list)
        self.img_sen_arr = img_sen_arr
        self.dct = dct
        self.num_in_batch = 0
        self.batch_num = 0
        self.total_batch_num = len(file_list)
        self.epoch_num = epoch_num
        self.total_epoch = 0

        # image data related init

        if os.path.isfile(mean_file_path):
            logger.debug("mean file path %s" % mean_file_path)
            self.mean_file = np.load(mean_file_path)
        else:
            logger.warn('Faile to load mean file %s!' % mean_file_path)
        self.lib_name = "decodeJPEG._DeJPEG"
        if os.path.isfile('./decodeJpeg/_DeJPEG.so'):
            logger.info("de jpeg ok")
        logger.info("init conv jpeg mode %s" % self.lib_name)
        self.libmodel = __import__(self.lib_name, fromlist=['_DeJPEG'])
        logger.info("finish loading deJpeg")
        self.image_size = 256
        self.inner_size = 224
        self.mean = self.mean_file['data_mean'].reshape(3, self.image_size, self.image_size)[:, (self.image_size - self.inner_size) / 2:(
            (self.image_size - self.inner_size) / 2 + self.inner_size), (self.image_size - self.inner_size) / 2:((self.image_size - self.inner_size) / 2 + self.inner_size)].reshape(1, -1)[0]
        self.mean_len = len(self.mean)
        logger.info("Mean length is %d" % self.mean_len)
        self.inner_pixels = self.inner_size * self.inner_size
        self.total_pixels = 3 * self.inner_pixels
        self.test = True
        self.multiview = False
        self.data_mult = 1
        self.shuffle_flag = True

        #
        logger.info('SentenceImageDataProvider Initialization finished')

    def shuffle(self):
        '''
          Description: set shuffle operation
        '''
        pass

    def reset(self):
        '''
          Description: execute some reset operation
          in each pass.
        '''
        pass

    def getHeader(self):
        '''
            Description: Get header info
            Return format:
                slot_num  // Total count of all slots
                use_sequence_flag // 1 if use sequence, 0 if not use sequence
                [(slot_type_1, slot_dim_1),  // A list of all slot type and dim
                 ...,
                 (slot_type_N, slot_dim_N)
                ]
            Note: The type of all header data all int32_t
        '''
        # logger.info('DEBUG::getHeader') #DEBUG
        num_slots = 4
        use_sequence = 1

        ret = ''
        ret += struct.pack('ii', num_slots, use_sequence)
        ret += struct.pack('ii', 0, self.total_pixels)  # image feature slot
        ret += struct.pack('ii', 3, len(self.dct))  # question sentence slot
        ret += struct.pack('ii', 3, len(self.dct))  # current word slot
        ret += struct.pack('ii', 3, len(self.dct))  # next word slot
        # logger.info("img feat dim: %d, dct length : %d " %(self.img_feat_dim, len(self.dct)))
        # logger.info('Header: %d slots, %d use_sequence' % (num_slots,
        # use_sequence))
        return ret

    def getJpeg(self, rawdics, img_mat):
        nc_total = len(rawdics)
        jpeg_strs = list(rawdics)
        perm = [idx_tmp for idx_tmp in xrange(nc_total)]
        # self.string="0"*4*nc_total*self.total_pixels
        # self.libmodel.decodeJpegAsString(jpeg_strs, self.img_mat[:nc_total,:], self.image_size, self.inner_size, self.test, self.multiview,self.string)
        # self.libmodel.decodeJpeg(jpeg_strs, img_mat, self.image_size,
        # self.inner_size, self.test, self.multiview,list(perm))
        perm = xrange(img_mat.shape[0])
        self.libmodel.decodeJpeg(
            jpeg_strs, img_mat, self.image_size, self.inner_size, self.test, self.multiview, list(perm))
        self.begin = 0
        self.end = nc_total
        return nc_total

    def getNextBatch(self, batch_size):
        '''
            Description: Get a batch of samples
            Return format:
                batch_size // specify the return value of getNextBatch
                // The flowing data was organise by slot, and the order of
                // slots was the same as in header. Different slot type has
                // diffent structure of data.
                // Now there are 4 kinds of data type.
                // 1 Dense type
                sample_num   // sample num in dense type slot
                value0 value1 ... valueN  // dense type slot values
                // 2 Sparse type with weight value
                sample_num
                offset0 offset1 ... offsetN // the offset of sample in values.
                                        // Use CSR format to store sparse value.
                length value0 value1 ... valueN
                length weight0  weight1 … weightN
                // 3 Sparse type without weight value type
                sample_num
                offset0 offset1 ... offsetN
                length value0 value1 ... valueN
                // 4 Index type
                sample_num
                value0 value1 ... valueN
                // If you use sequence, you should specify  sequences offsets in
                // each slot.
                slot1_sequence_num sequence_pos0  ... sequence_posN
                ...
                slotN_sequence_num sequence_pos0  ... sequence_posN
        '''
        # logger.info('DEBUG::getNextBatch') #DEBUG
        # if we have scan the whole data once
        # if num in batch == 0, means a new batch needed
        # logger.info("batch size %d" % batch_size)
        if self.num_in_batch == -1:
            self.num_in_batch = 0
            if self.epoch_num % self.total_batch_num == 0:
                if self.shuffle_flag:
                    random.shuffle(self.file_list)
                return struct.pack('i', 0)
        # begin_time = time.time()
        if self.num_in_batch == 0:
            # logger.info("Begin batch %d" %(self.epoch_num))
            fname = self.file_list[self.epoch_num %
                                   self.total_batch_num].strip()
            if self.epoch_num % self.total_batch_num == 0:
                self.total_epoch += 1
                logger.info("Begin epoch %d" % self.total_epoch)
            if os.path.isfile(fname):
                logger.debug("fname %s" % fname)
                self.img_sen_arr = pickle.load(open(fname, 'rb'))
                if self.shuffle_flag:
                    random.shuffle(self.img_sen_arr)
            else:
                logger.warn('Annotation file %s missing!' % fname)
            self.epoch_num += 1

        start_pos = self.num_in_batch
        image_number = 0
        end_pos = min(self.num_in_batch + batch_size, len(self.img_sen_arr))
        q_length = 0
        a_length = 1  # for batch problem add start sign
        # a_length = 0

        for check_one in self.img_sen_arr[start_pos:end_pos]:
            if max(q_length + len(check_one['coding_q']) - 1, a_length + len(check_one['coding_a']) - 1) < batch_size:
                image_number += 1
                a_length += len(check_one['coding_a'])
                q_length += len(check_one['coding_q'])
            else:
                break

        end_pos = start_pos + image_number
        batch = self.img_sen_arr[start_pos:end_pos]

        if end_pos >= len(self.img_sen_arr):
            self.num_in_batch = -1
            self.batch_num += 1
        else:
            self.num_in_batch = end_pos

        ret_img = ''
        ret_qw = ''
        ret_cw = ''
        ret_nw = ''
        ret_seq = ''
        ret_q_seq = ''
        img_seq = ''
        num_samples_batch = 0
        num_sequence_batch = 0
        num_samples_q_batch = 0

        # get image batch
        image_data = []
        for one_sample in batch:
            image_data.append(one_sample['feature'])
        img_mat_get = np.empty(
            (len(image_data) * self.data_mult, self.total_pixels), dtype=np.float32)
        len_jpeg = self.getJpeg(image_data, img_mat_get)
        img_mat_get -= self.mean

        for i in xrange(len(batch)):
            one_sample = batch[i]
            image_data = img_mat_get[i, :]
            assert(image_data.dtype == np.float32)
            img_feat = image_data
            question = one_sample['coding_q']
            answer = one_sample['coding_a']
            answer.insert(0, len(self.dct) - 1)
            answer_len = len(answer) - 1
            ret_qw += np.array(question, dtype=np.int32).tostring()
            ret_cw += np.array(answer[:-1], dtype=np.int32).tostring()
            ret_nw += np.array(answer[1:], dtype=np.int32).tostring()
            ret_img += img_feat.tostring()

            # logger.debug("len: %d, sentence: %s, num_samples_batch: %d" %
            # (len(sentence),' '.join(sentence),num_samples_batch))
            ret_seq = ret_seq + struct.pack('i', num_samples_batch)
            ret_q_seq = ret_q_seq + struct.pack('i', num_samples_q_batch)
            img_seq = img_seq + struct.pack('i', num_sequence_batch)
            num_samples_q_batch += len(question)
            num_samples_batch += answer_len
            num_sequence_batch += 1
            logger.debug('finish samples %d' % num_samples_batch)
            logger.debug('question:%s' % ''.join(one_sample['question']))
            logger.debug('answer:%s' % ''.join(one_sample['answer']))

        # logger.info("img:%d, cw:%d, nw:%d, ret_seq:%d" %
        # (len(ret_img)/4/self.img_feat_dim, len(ret_cw)/4, len(ret_nw)/4,
        # len(ret_seq)/4))
        ret_img = struct.pack('i', num_sequence_batch) + ret_img
        ret_qw = struct.pack('i', num_samples_q_batch) + ret_qw
        ret_cw = struct.pack('i', num_samples_batch) + ret_cw
        ret_nw = struct.pack('i', num_samples_batch) + ret_nw
        img_seq = struct.pack('i', num_sequence_batch) + img_seq
        ret_seq = struct.pack('i', num_sequence_batch) + ret_seq
        ret_q_seq = struct.pack('i', num_sequence_batch) + ret_q_seq
        ret = struct.pack('i', num_samples_batch) + ret_img + \
            ret_qw + ret_cw + ret_nw + img_seq + ret_q_seq + ret_seq * 2
        end_time = time.time()
        # logger.info("Begin: %f , end: %f, cost:%f" % (begin_time, end_time, end_time - begin_time))
        #logger.info('Pass one bach, num_samples %d, sequence num %d, q_number %d' % (num_samples_batch, num_sequence_batch,num_samples_q_batch)) # DEBUG
        # logger.info("img:%d, cw:%d, nw:%d, ret_seq:%d " %
        # (len(ret_img)/4/self.total_pixels, len(ret_cw)/4, len(ret_nw)/4,
        # len(ret_seq)/4))
        return ret


class pyDataImageFeatureQue:

    def __init__(self, *file_list, **kwargs):
        '''
        Description: Init with a list of data file
        file_list is the name list of input files.
        kwargs["load_data_args"] is the value of 'load_data_args' which can be set in config.

        kwargs["load_data_args"] is organized as follows
        '', () indicates the name and type of the args
        Each args is seperated by a space:
        'dictionary path'(str)
        'shuffle flag (True means shuffle)'(bool)
        'img_feat_dim'(int)
        'average norm factor for image feature'(float)
        '''
        gc.disable()
        str_conf_args = kwargs['load_data_args'].strip().split()
        dict_file_path = str_conf_args[0]
        flag_randseq = bool(int(str_conf_args[1]))
        img_feat_dim = int(str_conf_args[2])
        feat_avg_norm_factor = float(str_conf_args[3])
        epoch_num = int(str_conf_args[4])

        logger.info('Dictionary Path: %s' % dict_file_path)
        logger.info('Flag shuffle: %r' % flag_randseq)
        logger.info('Image dimension: %d' % img_feat_dim)
        logger.info('Average norm factor for image feature: %.4f' %
                    feat_avg_norm_factor)

        if os.path.isfile(dict_file_path):
            dct = np.load(open(dict_file_path, 'rb'))
            if dct.get('$$E$$', 0) == 0:
                dct['$$E$$'] = len(dct)
            if dct.get('$$S$$', 0) == 0:
                dct['$$S$$'] = len(dct)
            logger.info('Dictionary loaded with %d words' % len(dct))
        else:
            logger.fatal(
                'Dictionary file [%s] does not exist!' % dict_file_path)

        if len(file_list) == 0:
            logger.fatal('No annotation file!')
        else:
            logger.info('There are %d annotation files' % len(file_list))

        tmp_num_files = 0

        fname = file_list[0].strip()
        if os.path.isfile(fname):
            logger.debug("fname %s" % fname)
            img_sen_arr = pickle.load(open(fname, 'rb'))
            tmp_num_files += 1
        else:
            logger.warn('Annotation file %s missing!' % fname)

        # img_sen_arr = img_sen_arr[0:100] #DEBUG!!MUST COMMENT OUT!!

        self.file_list = list(file_list)
        self.img_sen_arr = img_sen_arr
        self.dct = dct
        self.img_feat_dim = img_feat_dim
        self.num_in_batch = 0
        self.batch_num = 0
        self.total_batch_num = len(file_list)
        self.feat_avg_norm_factor = feat_avg_norm_factor
        self.epoch_num = epoch_num
        self.total_epoch = 0
        self.shuffle_flag = True
        logger.info('SentenceImageFeatDataProvider Initialization finished')

    def shuffle(self):
        '''
          Description: set shuffle operation
        '''
        pass

    def reset(self):
        '''
          Description: execute some reset operation
          in each pass.
        '''
        pass

    def getHeader(self):
        '''
            Description: Get header info
            Return format:
                slot_num  // Total count of all slots
                use_sequence_flag // 1 if use sequence, 0 if not use sequence
                [(slot_type_1, slot_dim_1),  // A list of all slot type and dim
                 ...,
                 (slot_type_N, slot_dim_N)
                ]
            Note: The type of all header data all int32_t
        '''
        # logger.info('DEBUG::getHeader') #DEBUG
        num_slots = 3
        use_sequence = 1

        ret = ''
        ret += struct.pack('ii', num_slots, use_sequence)
        ret += struct.pack('ii', 0, self.img_feat_dim)  # image feature slot
        ret += struct.pack('ii', 3, len(self.dct))  # current word slot
        ret += struct.pack('ii', 3, len(self.dct))  # next word slot
        # logger.info("img feat dim: %d, dct length : %d " %(self.img_feat_dim, len(self.dct)))
        # logger.info('Header: %d slots, %d use_sequence' % (num_slots,
        # use_sequence))
        return ret

    def getNextBatch(self, batch_size):
        '''
            Description: Get a batch of samples
            Return format:
                batch_size // specify the return value of getNextBatch
                // The flowing data was organise by slot, and the order of
                // slots was the same as in header. Different slot type has
                // diffent structure of data.
                // Now there are 4 kinds of data type.
                // 1 Dense type
                sample_num   // sample num in dense type slot
                value0 value1 ... valueN  // dense type slot values
                // 2 Sparse type with weight value
                sample_num
                offset0 offset1 ... offsetN // the offset of sample in values.
                                        // Use CSR format to store sparse value.
                length value0 value1 ... valueN
                length weight0  weight1 … weightN
                // 3 Sparse type without weight value type
                sample_num
                offset0 offset1 ... offsetN
                length value0 value1 ... valueN
                // 4 Index type
                sample_num
                value0 value1 ... valueN
                // If you use sequence, you should specify  sequences offsets in
                // each slot.
                slot1_sequence_num sequence_pos0  ... sequence_posN
                ...
                slotN_sequence_num sequence_pos0  ... sequence_posN
        '''
        # logger.info('DEBUG::getNextBatch') #DEBUG
        # if we have scan the whole data once
        # if num in batch == 0, means a new batch needed
        # logger.info("batch size %d" % batch_size)
        if self.num_in_batch == -1:
            self.num_in_batch = 0
            if self.epoch_num % self.total_batch_num == 0:
                if self.shuffle_flag:
                    random.shuffle(self.file_list)
                return struct.pack('i', 0)
        begin_time = time.time()
        if self.num_in_batch == 0:
            # logger.info("Begin batch %d" %(self.epoch_num))
            fname = self.file_list[self.epoch_num % self.total_batch_num]
            if self.epoch_num % self.total_batch_num == 0:
                self.total_epoch += 1
                logger.info("Begin epoch %d" % self.total_epoch)
            if os.path.isfile(fname):
                logger.debug("fname %s" % fname)
                self.img_sen_arr = pickle.load(open(fname, 'rb'))
                if self.shuffle_flag:
                    random.shuffle(self.img_sen_arr)
            else:
                logger.warn('Annotation file %s missing!' % fname)
            self.epoch_num += 1

        start_pos = self.num_in_batch
        image_number = 0
        end_pos = min(self.num_in_batch + batch_size, len(self.img_sen_arr))
        word_length = 0
        for check_one in self.img_sen_arr[start_pos:end_pos]:
            if word_length + len(check_one['coding_q']) < batch_size:
                image_number += 1
                word_length += len(check_one['coding_q'])
            else:
                break

        end_pos = start_pos + image_number
        batch = self.img_sen_arr[start_pos:end_pos]

        if end_pos >= len(self.img_sen_arr):
            self.num_in_batch = -1
            self.batch_num += 1
        else:
            self.num_in_batch = end_pos

        ret_img = ''
        ret_cw = ''
        ret_nw = ''
        ret_seq = ''
        img_seq = ''
        num_samples_batch = 0
        num_sequence_batch = 0

        for one_sample in batch:
            assert(one_sample['feature'].dtype == np.float32)
            img_feat = one_sample['feature'] * self.feat_avg_norm_factor
            sentence = one_sample['coding_q']
            sentence.insert(0, len(self.dct) - 1)
            sentence_len = len(sentence) - 1
            ret_cw += np.array(sentence[:-1], dtype=np.int32).tostring()
            ret_nw += np.array(sentence[1:], dtype=np.int32).tostring()
            ret_img += img_feat.tostring()

            # logger.debug("len: %d, sentence: %s, num_samples_batch: %d" %
            # (len(sentence),' '.join(sentence),num_samples_batch))
            ret_seq = ret_seq + struct.pack('i', num_samples_batch)
            img_seq = img_seq + struct.pack('i', num_sequence_batch)
            num_samples_batch += sentence_len
            num_sequence_batch += 1
        # logger.info("img:%d, cw:%d, nw:%d, ret_seq:%d" %
        # (len(ret_img)/4/self.img_feat_dim, len(ret_cw)/4, len(ret_nw)/4,
        # len(ret_seq)/4))
        ret_img = struct.pack('i', num_sequence_batch) + ret_img
        ret_cw = struct.pack('i', num_samples_batch) + ret_cw
        ret_nw = struct.pack('i', num_samples_batch) + ret_nw
        img_seq = struct.pack('i', num_sequence_batch) + img_seq
        ret_seq = struct.pack('i', num_sequence_batch) + ret_seq
        ret = struct.pack('i', num_samples_batch) + \
            ret_img + ret_cw + ret_nw + img_seq + ret_seq * 2
        # end_time = time.time()
        # logger.info("Begin: %f , end: %f, cost:%f" % (begin_time, end_time, end_time - begin_time))
        # logger.info('Pass one bach, num_samples %d, sequence num %d, length %d' % (num_samples_batch, num_sequence_batch,len(ret))) # DEBUG
        # logger.info("img:%d, cw:%d, nw:%d, ret_seq:%d , img_seq: %d " %
        # (len(ret_img)/4/self.img_feat_dim, len(ret_cw)/4, len(ret_nw)/4,
        # len(ret_seq)/4, len(img_seq)/4))
        return ret


class pyDataImageFeatureALL:

    def __init__(self, *file_list, **kwargs):
        '''
        Description: Init with a list of data file
        file_list is the name list of input files.
        kwargs["load_data_args"] is the value of 'load_data_args' which can be set in config.

        kwargs["load_data_args"] is organized as follows
        '', () indicates the name and type of the args
        Each args is seperated by a space:
        'dictionary path'(str)
        'shuffle flag (True means shuffle)'(bool)
        'img_feat_dim'(int)
        'average norm factor for image feature'(float)
        '''
        gc.disable()
        str_conf_args = kwargs['load_data_args'].strip().split()
        logger.info("begin pyDataImageFeatureALL")
        logger.info(str_conf_args)
        ch_dict_file_path = str_conf_args[0]
        en_dict_file_path = str_conf_args[1]
        img_feat_dim = int(str_conf_args[2])
        feat_avg_norm_factor = float(str_conf_args[3])
        epoch_num = int(str_conf_args[4])

        logger.info('Dictionary Path: ch:%s, en:%s' % (ch_dict_file_path, en_dict_file_path))
        logger.info('Image dimension: %d' % img_feat_dim)
        logger.info('Average norm factor for image feature: %.4f' %
                    feat_avg_norm_factor)

        if os.path.isfile(ch_dict_file_path):
            dct = np.load(open(ch_dict_file_path, 'rb'))
            if dct.get('$$E$$', 0) == 0:
                dct['$$E$$'] = len(dct)
            if dct.get('$$S$$', 0) == 0:
                dct['$$S$$'] = len(dct)
            logger.info('Chinese Dictionary loaded with %d words' % len(dct))
        else:
            logger.fatal(
                'Dictionary file [%s] does not exist!' % ch_dict_file_path)
        self.ch_dic = dct

        if os.path.isfile(en_dict_file_path):
            dct = np.load(open(en_dict_file_path, 'rb'))
            if dct.get('$$E$$', 0) == 0:
                dct['$$E$$'] = len(dct)
            if dct.get('$$S$$', 0) == 0:
                dct['$$S$$'] = len(dct)
            logger.info('English Dictionary loaded with %d words' % len(dct))
        else:
            logger.fatal(
                'Dictionary file [%s] does not exist!' % en_dict_file_path)
        self.en_dic = dct

        if len(file_list) == 0:
            logger.fatal('No annotation file!')
        else:
            logger.info('There are %d annotation files' % len(file_list))

        tmp_num_files = 0

        fname = file_list[0].strip()
        if os.path.isfile(fname):
            logger.debug("fname %s" % fname)
            img_sen_arr = pickle.load(open(fname, 'rb'))
            tmp_num_files += 1
        else:
            logger.warn('Annotation file %s missing!' % fname)

        # img_sen_arr = img_sen_arr[0:100] #DEBUG!!MUST COMMENT OUT!!

        self.file_list = list(file_list)
        self.img_sen_arr = img_sen_arr

        self.img_feat_dim = img_feat_dim
        self.num_in_batch = 0
        self.batch_num = 0
        self.total_batch_num = len(file_list)
        self.feat_avg_norm_factor = feat_avg_norm_factor
        self.epoch_num = epoch_num
        self.total_epoch = 0
        logger.info('SentenceImageFeatDataProvider Initialization finished')

    def shuffle(self):
        '''
          Description: set shuffle operation
        '''
        pass

    def reset(self):
        '''
          Description: execute some reset operation
          in each pass.
        '''
        pass

    def getHeader(self):
        '''
            Description: Get header info
            Return format:
                slot_num  // Total count of all slots
                use_sequence_flag // 1 if use sequence, 0 if not use sequence
                [(slot_type_1, slot_dim_1),  // A list of all slot type and dim
                 ...,
                 (slot_type_N, slot_dim_N)
                ]
            Note: The type of all header data all int32_t
        '''
        # logger.info('DEBUG::getHeader') #DEBUG
        num_slots = 8
        use_sequence = 1

        ret = ''
        ret += struct.pack('ii', num_slots, use_sequence)
        ret += struct.pack('ii', 0, self.img_feat_dim)  # image feature slot
        ret += struct.pack('ii', 3, len(self.ch_dic))  # question sentence slot
        ret += struct.pack('ii', 3, len(self.ch_dic))  # current word slot
        ret += struct.pack('ii', 3, len(self.ch_dic))  # next word slot
        # ch sentence current word slot
        ret += struct.pack('ii', 3, len(self.ch_dic))
        ret += struct.pack('ii', 3, len(self.ch_dic))  # next word slot
        # en sentence current word slot
        ret += struct.pack('ii', 3, len(self.en_dic))
        ret += struct.pack('ii', 3, len(self.en_dic))  # next word slot
        # logger.info('Header: %d slots, %d use_sequence' % (num_slots,
        # use_sequence))
        return ret

    def getNextBatch(self, batch_size):
        '''
            Description: Get a batch of samples
            Return format:
                batch_size // specify the return value of getNextBatch
                // The flowing data was organise by slot, and the order of
                // slots was the same as in header. Different slot type has
                // diffent structure of data.
                // Now there are 4 kinds of data type.
                // 1 Dense type
                sample_num   // sample num in dense type slot
                value0 value1 ... valueN  // dense type slot values
                // 2 Sparse type with weight value
                sample_num
                offset0 offset1 ... offsetN // the offset of sample in values.
                                        // Use CSR format to store sparse value.
                length value0 value1 ... valueN
                length weight0  weight1 … weightN
                // 3 Sparse type without weight value type
                sample_num
                offset0 offset1 ... offsetN
                length value0 value1 ... valueN
                // 4 Index type
                sample_num
                value0 value1 ... valueN
                // If you use sequence, you should specify  sequences offsets in
                // each slot.
                slot1_sequence_num sequence_pos0  ... sequence_posN
                ...
                slotN_sequence_num sequence_pos0  ... sequence_posN
        '''
        # logger.info('DEBUG::getNextBatch') #DEBUG
        # if we have scan the whole data once
        # if num in batch == 0, means a new batch needed
        # logger.info("batch size %d" % batch_size)
        if self.num_in_batch == -1:
            self.num_in_batch = 0
            if self.epoch_num % self.total_batch_num == 0:
                random.shuffle(self.file_list)
                return struct.pack('i', 0)
        begin_time = time.time()
        if self.num_in_batch == 0:
            # logger.info("Begin batch %d" %(self.epoch_num))
            fname = self.file_list[self.epoch_num %
                                   self.total_batch_num].strip()
            if self.epoch_num % self.total_batch_num == 0:
                self.total_epoch += 1
                logger.info("Begin epoch %d" % self.total_epoch)
            if os.path.isfile(fname):
                logger.debug("fname %s" % fname)
                self.img_sen_arr = pickle.load(open(fname, 'rb'))
                random.shuffle(self.img_sen_arr)
            else:
                logger.warn('Annotation file %s missing!' % fname)
            self.epoch_num += 1

        start_pos = self.num_in_batch
        image_number = 0
        end_pos = min(self.num_in_batch + batch_size, len(self.img_sen_arr))
        q_length = 0
        a_length = 0
        ch_sen_length = 0
        en_sen_length = 0

        for check_one in self.img_sen_arr[start_pos:end_pos]:
            if max(q_length + len(check_one['coding_q']),
                   a_length + len(check_one['coding_a']),
                   ch_sen_length + len(check_one['coding_sen_ch']),
                   en_sen_length + len(check_one['coding_sen_en'])
                   ) < batch_size:
                image_number += 1
                a_length += len(check_one['coding_a'])
                q_length += len(check_one['coding_q'])
                ch_sen_length += len(check_one['coding_sen_ch'])
                en_sen_length += len(check_one['coding_sen_en'])
            else:
                break

        end_pos = start_pos + image_number
        batch = self.img_sen_arr[start_pos:end_pos]

        if end_pos >= len(self.img_sen_arr):
            self.num_in_batch = -1
            self.batch_num += 1
        else:
            self.num_in_batch = end_pos

        ret_img = ''
        ret_qw = ''
        ret_cw = ''
        ret_nw = ''
        ret_seq = ''
        ret_q_seq = ''
        img_seq = ''
        ret_ch_sen_w = ''
        ret_ch_sen_n = ''
        ret_en_sen_w = ''
        ret_en_sen_n = ''
        ret_ch_sen_seq = ''
        ret_en_sen_seq = ''
        num_samples_batch = 0
        num_sequence_batch = 0
        num_samples_q_batch = 0
        num_samples_ch_sen_batch = 0
        num_samples_en_sen_batch = 0

        for i in xrange(len(batch)):
            one_sample = batch[i]
            img_feat = one_sample['feature'] * self.feat_avg_norm_factor
            question = one_sample['coding_q']
            answer = one_sample['coding_a']
            ch_sen = one_sample['coding_sen_ch']
            en_sen = one_sample['coding_sen_en']
            answer.insert(0, len(self.ch_dic) - 1)
            ch_sen.insert(0, len(self.ch_dic) - 1)
            en_sen.insert(0, len(self.en_dic) - 1)
            answer_len = len(answer) - 1
            ret_qw += np.array(question, dtype=np.int32).tostring()
            ret_cw += np.array(answer[:-1], dtype=np.int32).tostring()
            ret_nw += np.array(answer[1:], dtype=np.int32).tostring()
            ret_ch_sen_w += np.array(ch_sen[:-1], dtype=np.int32).tostring()
            ret_ch_sen_n += np.array(ch_sen[1:], dtype=np.int32).tostring()
            ret_en_sen_w += np.array(en_sen[:-1], dtype=np.int32).tostring()
            ret_en_sen_n += np.array(en_sen[1:], dtype=np.int32).tostring()
            ret_img += img_feat.tostring()

            # logger.debug("len: %d, sentence: %s, num_samples_batch: %d" %
            # (len(sentence),' '.join(sentence),num_samples_batch))
            ret_seq = ret_seq + struct.pack('i', num_samples_batch)
            ret_q_seq = ret_q_seq + struct.pack('i', num_samples_q_batch)
            ret_ch_sen_seq += struct.pack('i', num_samples_ch_sen_batch)
            ret_en_sen_seq += struct.pack('i', num_samples_en_sen_batch)
            img_seq = img_seq + struct.pack('i', num_sequence_batch)
            num_samples_q_batch += len(question)
            num_samples_batch += answer_len
            num_sequence_batch += 1
            num_samples_ch_sen_batch += len(ch_sen) - 1
            num_samples_en_sen_batch += len(en_sen) - 1

            logger.debug('finish samples %d' % num_samples_batch)
            logger.debug('question:%s' % ''.join(one_sample['question']))
            logger.debug('answer:%s' % ''.join(one_sample['answer']))
            logger.debug('ch_sentence:%s' % ''.join(one_sample['ch_sentence']))
            if len(one_sample['en_sentence']) < 2 :
                logger.debug('error en_sentence %d '  % i)
            else:
                logger.debug('en_sentence:%s' %
                         ' '.join(one_sample['en_sentence']))

        # logger.info("img:%d, cw:%d, nw:%d, ret_seq:%d" %
        # (len(ret_img)/4/self.img_feat_dim, len(ret_cw)/4, len(ret_nw)/4,
        # len(ret_seq)/4))
        ret_img = struct.pack('i', num_sequence_batch) + ret_img
        ret_qw = struct.pack('i', num_samples_q_batch) + ret_qw
        ret_cw = struct.pack('i', num_samples_batch) + ret_cw
        ret_nw = struct.pack('i', num_samples_batch) + ret_nw
        ret_ch_sen_w = struct.pack(
            'i', num_samples_ch_sen_batch) + ret_ch_sen_w
        ret_ch_sen_n = struct.pack(
            'i', num_samples_ch_sen_batch) + ret_ch_sen_n
        ret_en_sen_w = struct.pack(
            'i', num_samples_en_sen_batch) + ret_en_sen_w
        ret_en_sen_n = struct.pack(
            'i', num_samples_en_sen_batch) + ret_en_sen_n
        img_seq = struct.pack('i', num_sequence_batch) + img_seq
        ret_seq = struct.pack('i', num_sequence_batch) + ret_seq
        ret_q_seq = struct.pack('i', num_sequence_batch) + ret_q_seq
        ret_ch_sen_seq = struct.pack('i', num_sequence_batch) + ret_ch_sen_seq
        ret_en_sen_seq = struct.pack('i', num_sequence_batch) + ret_en_sen_seq

        ret = struct.pack('i', num_samples_batch) + ret_img + \
            ret_qw + ret_cw + ret_nw + ret_ch_sen_w + ret_ch_sen_n + \
            ret_en_sen_w + ret_en_sen_n + img_seq + ret_q_seq + \
            ret_seq * 2 + ret_ch_sen_seq * 2 + ret_en_sen_seq * 2
        end_time = time.time()
        # logger.info("Begin: %f , end: %f, cost:%f" % (begin_time, end_time, end_time - begin_time))
        # logger.info('Pass one bach, num_samples %d, sequence num %d, length %d' % (num_samples_batch, num_sequence_batch,len(ret))) # DEBUG
        # logger.info("img:%d, cw:%d, nw:%d, ret_seq:%d " %
        # (len(ret_img)/4/self.total_pixels, len(ret_cw)/4, len(ret_nw)/4,
        # len(ret_seq)/4))
        return ret


class pyDataImageFeatureCH:

    def __init__(self, *file_list, **kwargs):
        '''
        Description: Init with a list of data file
        file_list is the name list of input files.
        kwargs["load_data_args"] is the value of 'load_data_args' which can be set in config.

        kwargs["load_data_args"] is organized as follows
        '', () indicates the name and type of the args
        Each args is seperated by a space:
        'dictionary path'(str)
        'shuffle flag (True means shuffle)'(bool)
        'img_feat_dim'(int)
        'average norm factor for image feature'(float)
        '''
        gc.disable()
        str_conf_args = kwargs['load_data_args'].strip().split()
        logger.info("begin pyDataImageFeatureALL")
        logger.info(str_conf_args)
        ch_dict_file_path = str_conf_args[0]
        img_feat_dim = int(str_conf_args[1])
        feat_avg_norm_factor = float(str_conf_args[2])
        epoch_num = int(str_conf_args[3])

        logger.info('Dictionary Path: ch:%s' % (ch_dict_file_path))
        logger.info('Image dimension: %d' % img_feat_dim)
        logger.info('Average norm factor for image feature: %.4f' %
                    feat_avg_norm_factor)

        if os.path.isfile(ch_dict_file_path):
            dct = np.load(open(ch_dict_file_path, 'rb'))
            if dct.get('$$E$$', 0) == 0:
                dct['$$E$$'] = len(dct)
            if dct.get('$$S$$', 0) == 0:
                dct['$$S$$'] = len(dct)
            logger.info('Chinese Dictionary loaded with %d words' % len(dct))
        else:
            logger.fatal(
                'Dictionary file [%s] does not exist!' % ch_dict_file_path)
        self.ch_dic = dct

        if len(file_list) == 0:
            logger.fatal('No annotation file!')
        else:
            logger.info('There are %d annotation files' % len(file_list))

        tmp_num_files = 0

        fname = file_list[0].strip()
        if os.path.isfile(fname):
            logger.debug("fname %s" % fname)
            img_sen_arr = pickle.load(open(fname, 'rb'))
            tmp_num_files += 1
        else:
            logger.warn('Annotation file %s missing!' % fname)

        # img_sen_arr = img_sen_arr[0:100] #DEBUG!!MUST COMMENT OUT!!

        self.file_list = list(file_list)
        self.img_sen_arr = img_sen_arr

        self.img_feat_dim = img_feat_dim
        self.num_in_batch = 0
        self.batch_num = 0
        self.total_batch_num = len(file_list)
        self.feat_avg_norm_factor = feat_avg_norm_factor
        self.epoch_num = epoch_num
        self.total_epoch = 0
        logger.info('SentenceImageFeatDataProvider Initialization finished')

    def shuffle(self):
        '''
          Description: set shuffle operation
        '''
        pass

    def reset(self):
        '''
          Description: execute some reset operation
          in each pass.
        '''
        pass

    def getHeader(self):
        '''
            Description: Get header info
            Return format:
                slot_num  // Total count of all slots
                use_sequence_flag // 1 if use sequence, 0 if not use sequence
                [(slot_type_1, slot_dim_1),  // A list of all slot type and dim
                 ...,
                 (slot_type_N, slot_dim_N)
                ]
            Note: The type of all header data all int32_t
        '''
        # logger.info('DEBUG::getHeader') #DEBUG
        num_slots = 6
        use_sequence = 1

        ret = ''
        ret += struct.pack('ii', num_slots, use_sequence)
        ret += struct.pack('ii', 0, self.img_feat_dim)  # image feature slot
        ret += struct.pack('ii', 3, len(self.ch_dic))  # question sentence slot
        ret += struct.pack('ii', 3, len(self.ch_dic))  # current word slot
        ret += struct.pack('ii', 3, len(self.ch_dic))  # next word slot
        # ch sentence current word slot
        ret += struct.pack('ii', 3, len(self.ch_dic))
        ret += struct.pack('ii', 3, len(self.ch_dic))  # next word slot
        # logger.info('Header: %d slots, %d use_sequence' % (num_slots,
        # use_sequence))
        return ret

    def getNextBatch(self, batch_size):
        '''
            Description: Get a batch of samples
            Return format:
                batch_size // specify the return value of getNextBatch
                // The flowing data was organise by slot, and the order of
                // slots was the same as in header. Different slot type has
                // diffent structure of data.
                // Now there are 4 kinds of data type.
                // 1 Dense type
                sample_num   // sample num in dense type slot
                value0 value1 ... valueN  // dense type slot values
                // 2 Sparse type with weight value
                sample_num
                offset0 offset1 ... offsetN // the offset of sample in values.
                                        // Use CSR format to store sparse value.
                length value0 value1 ... valueN
                length weight0  weight1 … weightN
                // 3 Sparse type without weight value type
                sample_num
                offset0 offset1 ... offsetN
                length value0 value1 ... valueN
                // 4 Index type
                sample_num
                value0 value1 ... valueN
                // If you use sequence, you should specify  sequences offsets in
                // each slot.
                slot1_sequence_num sequence_pos0  ... sequence_posN
                ...
                slotN_sequence_num sequence_pos0  ... sequence_posN
        '''
        # logger.info('DEBUG::getNextBatch') #DEBUG
        # if we have scan the whole data once
        # if num in batch == 0, means a new batch needed
        # logger.info("batch size %d" % batch_size)
        if self.num_in_batch == -1:
            self.num_in_batch = 0
            if self.epoch_num % self.total_batch_num == 0:
                random.shuffle(self.file_list)
                return struct.pack('i', 0)
        if self.num_in_batch == 0:
            # logger.info("Begin batch %d" %(self.epoch_num))
            fname = self.file_list[self.epoch_num %
                                   self.total_batch_num].strip()
            if self.epoch_num % self.total_batch_num == 0:
                self.total_epoch += 1
                logger.info("Begin epoch %d" % self.total_epoch)
            if os.path.isfile(fname):
                logger.debug("fname %s" % fname)
                self.img_sen_arr = pickle.load(open(fname, 'rb'))
                random.shuffle(self.img_sen_arr)
            else:
                logger.warn('Annotation file %s missing!' % fname)
            self.epoch_num += 1

        start_pos = self.num_in_batch
        image_number = 0
        end_pos = min(self.num_in_batch + batch_size, len(self.img_sen_arr))
        q_length = 0
        a_length = 0
        ch_sen_length = 0

        for check_one in self.img_sen_arr[start_pos:end_pos]:
            if max(q_length + len(check_one['coding_q']),
                   a_length + len(check_one['coding_a']),
                   ch_sen_length + len(check_one['coding_sen_ch'])
                   ) < batch_size:
                image_number += 1
                a_length += len(check_one['coding_a'])
                q_length += len(check_one['coding_q'])
                ch_sen_length += len(check_one['coding_sen_ch'])
            else:
                break

        end_pos = start_pos + image_number
        batch = self.img_sen_arr[start_pos:end_pos]

        if end_pos >= len(self.img_sen_arr):
            self.num_in_batch = -1
            self.batch_num += 1
        else:
            self.num_in_batch = end_pos

        ret_img = ''
        ret_qw = ''
        ret_cw = ''
        ret_nw = ''
        ret_seq = ''
        ret_q_seq = ''
        img_seq = ''
        ret_ch_sen_w = ''
        ret_ch_sen_n = ''
        ret_ch_sen_seq = ''
        num_samples_batch = 0
        num_sequence_batch = 0
        num_samples_q_batch = 0
        num_samples_ch_sen_batch = 0

        for i in xrange(len(batch)):
            one_sample = batch[i]
            img_feat = one_sample['feature'] * self.feat_avg_norm_factor
            question = one_sample['coding_q']
            answer = one_sample['coding_a']
            ch_sen = one_sample['coding_sen_ch']
            answer.insert(0, len(self.ch_dic) - 1)
            ch_sen.insert(0, len(self.ch_dic) - 1)
            answer_len = len(answer) - 1
            ret_qw += np.array(question, dtype=np.int32).tostring()
            ret_cw += np.array(answer[:-1], dtype=np.int32).tostring()
            ret_nw += np.array(answer[1:], dtype=np.int32).tostring()
            ret_ch_sen_w += np.array(ch_sen[:-1], dtype=np.int32).tostring()
            ret_ch_sen_n += np.array(ch_sen[1:], dtype=np.int32).tostring()
            ret_img += img_feat.tostring()

            # logger.debug("len: %d, sentence: %s, num_samples_batch: %d" %
            # (len(sentence),' '.join(sentence),num_samples_batch))
            ret_seq = ret_seq + struct.pack('i', num_samples_batch)
            ret_q_seq = ret_q_seq + struct.pack('i', num_samples_q_batch)
            ret_ch_sen_seq += struct.pack('i', num_samples_ch_sen_batch)
            img_seq = img_seq + struct.pack('i', num_sequence_batch)
            num_samples_q_batch += len(question)
            num_samples_batch += answer_len
            num_sequence_batch += 1
            num_samples_ch_sen_batch += len(ch_sen) - 1

            logger.debug('finish samples %d' % num_samples_batch)
            logger.debug('question:%s' % ''.join(one_sample['question']))
            logger.debug('answer:%s' % ''.join(one_sample['answer']))
            logger.debug('ch_sentence:%s' % ''.join(one_sample['ch_sentence']))

        # logger.info("img:%d, cw:%d, nw:%d, ret_seq:%d" %
        # (len(ret_img)/4/self.img_feat_dim, len(ret_cw)/4, len(ret_nw)/4,
        # len(ret_seq)/4))
        ret_img = struct.pack('i', num_sequence_batch) + ret_img
        ret_qw = struct.pack('i', num_samples_q_batch) + ret_qw
        ret_cw = struct.pack('i', num_samples_batch) + ret_cw
        ret_nw = struct.pack('i', num_samples_batch) + ret_nw
        ret_ch_sen_w = struct.pack(
            'i', num_samples_ch_sen_batch) + ret_ch_sen_w
        ret_ch_sen_n = struct.pack(
            'i', num_samples_ch_sen_batch) + ret_ch_sen_n
        img_seq = struct.pack('i', num_sequence_batch) + img_seq
        ret_seq = struct.pack('i', num_sequence_batch) + ret_seq
        ret_q_seq = struct.pack('i', num_sequence_batch) + ret_q_seq
        ret_ch_sen_seq = struct.pack('i', num_sequence_batch) + ret_ch_sen_seq

        ret = struct.pack('i', num_samples_batch) + ret_img + \
            ret_qw + ret_cw + ret_nw + ret_ch_sen_w + ret_ch_sen_n + \
            img_seq + ret_q_seq + \
            ret_seq * 2 + \
            ret_ch_sen_seq * 2
        # logger.info("Begin: %f , end: %f, cost:%f" % (begin_time, end_time, end_time - begin_time))
        # logger.info('Pass one bach, num_samples %d, sequence num %d, length %d' % (num_samples_batch, num_sequence_batch,len(ret))) # DEBUG
        # logger.info("img:%d, cw:%d, nw:%d, ret_seq:%d " %
        # (len(ret_img)/4/self.total_pixels, len(ret_cw)/4, len(ret_nw)/4,
        # len(ret_seq)/4))
        return ret


def check_data(input_header, input_data):
    # check header to get information

    num_slots, use_sequence = struct.unpack('ii', input_header[0:8])
    if ( struct.unpack('i', input_data[0:4])[0] == 0):
        logger.info("Finish batch")
        return 0

    slots_info = []
    # check each data info
    for i in xrange(num_slots):
        pos_begin = i * 8 + 8
        pos_end = pos_begin + 8
        slot_type, slot_dim = struct.unpack(
            'ii', input_header[pos_begin:pos_end])
        one_slot_info = {'type': slot_type, 'dim': slot_dim}
        print "oen:" , one_slot_info
        slots_info.append(one_slot_info)

    logger.info("Num of Slots: %d, %d, Num of Sequnece: %d" %
                (num_slots, len(slots_info), use_sequence))

    # checking data info
    batch_sample_number = struct.unpack('i', input_data[0:4])[0]
    pos_begin = 4
    # print slots_info
    for one_slot in slots_info:
        samples_number = struct.unpack(
            'i', input_data[pos_begin:pos_begin + 4])[0]
        logger.info("sample number is %d" % (samples_number))
        pos_begin += 4
        if one_slot['type'] == 0:
            pos_end = pos_begin + samples_number * one_slot['dim'] * 4
            slot_data = np.frombuffer(
                input_data[pos_begin: pos_end], dtype=np.float32)
            logger.info("type 0 data: len(%d) %f, %f, %f, mean: %f" % (
                len(slot_data) / one_slot['dim'], slot_data[0], slot_data[1], slot_data[-1], np.mean(slot_data)))
        if one_slot['type'] == 3:
            pos_end = pos_begin + samples_number * 4
            slot_data = np.frombuffer(
                input_data[pos_begin: pos_end], dtype=np.int32)
            logger.info("type %d data: len(%d) %d, %d" % (
                one_slot['type'], len(slot_data), slot_data[0], slot_data[-1]))
            logger.info(slot_data)
            #for i in slot_data:
            #    print >> sys.stderr, i, " ",
            #print >> sys.stderr, ""
            for i in slot_data:
                if i > one_slot['dim']:
                    logger.info("error data %d", i)
                else:
                    pass

        pos_begin = pos_end

    # check sequence info
    if use_sequence:
        for one_slot in slots_info:
            samples_number = struct.unpack(
                'i', input_data[pos_begin:pos_begin + 4])[0]
            pos_begin += 4
            pos_end = pos_begin + samples_number * 4
            slot_data = np.frombuffer(
                input_data[pos_begin: pos_end], dtype=np.int32)
            pos_begin = pos_end
            logger.info("type %d data: len(%d) %d, %d" % (
                one_slot['type'], len(slot_data), slot_data[0], slot_data[-1]))

if __name__ == "__main__":
    # test code

    check_type = "IMAGE_QA_FEATURE"

    if check_type == "IMAGE_QA_FEATURE":
        #file_list = './data/from_image_QA_FEATURE//all.list'
        #str_args = './data/from_image_QA_FEATURE/0205.dict 0 6144 1.0 0'
        file_list = './data/0702/data_own_3072/train.qa.list'
        str_args = './data/0702/data_own_3072/0702.dict 0 3072 1.0 0'
        file_lists = open(file_list).readlines()
        data_provider = pyDataImageFeatureQA(
            *file_lists, **{'load_data_args': str_args})
    elif check_type == "IMAGE_AQ_FEATURE":
        file_list = './data/0702/data_own_3072/train.qa.list'
        str_args = './data/0702/data_own_3072/0702.dict 0 3072 1.0 0'
        file_lists = open(file_list).readlines()
        data_provider = pyDataImageFeatureAQ(
            *file_lists, **{'load_data_args': str_args})
    elif check_type == "IMAGE_AQQA_FEATURE":
        file_list = './data/0702/data_own_3072/train.qa.list'
        str_args = './data/0702/data_own_3072/0702.dict 0 3072 1.0 0'
        file_lists = open(file_list).readlines()
        data_provider = pyDataImageFeatureBIQA(
            *file_lists, **{'load_data_args': str_args})
    elif check_type == "QA":
        file_list = './data/from_image_QA_FEATURE//all.list'
        str_args = './data/from_image_QA_FEATURE/0205.dict 0 0'
        file_lists = open(file_list).readlines()
        data_provider = pyDataQA(
            *file_lists, **{'load_data_args': str_args})
    elif check_type == "IMAGE_FEATURE_Gen_Q":
        file_list = './data/from_image_QA_FEATURE//all.list'
        str_args = './data/from_image_QA_FEATURE/0205.dict 0 6144 1.0 0'
        file_lists = open(file_list).readlines()
        data_provider = pyDataImageFeatureQue(
            *file_lists, **{'load_data_args': str_args})
    elif check_type == "IMAGE_QA":
        file_list = './data/image_qa_6144/all.list'
        str_args = './data/image_qa_6144/0205.dict 0 ./data/image_qa_6144/batches.meta 0'
        file_lists = open(file_list).readlines()
        data_provider = pyDataImageQA(
            *file_lists, **{'load_data_args': str_args})
    elif check_type == "ALL_INFO":
        file_list = './data/from_image_qa_sen_google_3072//all.list'
        str_args = './data/from_image_qa_sen_google_3072/ch_0423.dict ' + \
            './data/from_image_qa_sen_google_3072/en_0423.dict ' + '3072 ' + \
            '1 0'
        file_lists = open(file_list).readlines()
        data_provider = pyDataImageFeatureALL(
            *file_lists, **{'load_data_args': str_args})
    elif check_type == "CH_INFO":
        file_list = './data/from_image_qa_sen_google_3072//all.list'
        str_args = './data/from_image_qa_sen_google_3072/ch_0423.dict ' + \
            '3072 ' + '1 0'
        file_lists = open(file_list).readlines()
        data_provider = pyDataImageFeatureCH(
            *file_lists, **{'load_data_args': str_args})
    elif check_type == "IMAGE_SEN":
        file_list = './data/from_image//test.list'
        str_args = './data/from_image/train.dict 0 ./data/from_image_QA/batches.meta 0 1'
        file_lists = open(file_list).readlines()
        data_provider = pyDataImageSen(
            *file_lists, **{'load_data_args': str_args})
    elif check_type == "IMAGE_FEATURE_SEN":
        #file_list = './data/from_image_ch_google_3072/all.list'
        #str_args = './data/from_image_ch_google_3072/ch_0423.dict 0 3072 1.0 0'
        #file_list = './data/clothes/file.list'
        #str_args = './data/clothes/clothes.dict 0 4096 1.0 0'
        file_list = './data/0702/data_own_3072/train.sen.list'
        str_args = './data/0702/data_own_3072/0702.dict 0 3072 1.0 0'
        file_lists = open(file_list).readlines()
        data_provider = pyDataImageFeatureSen(
            *file_lists, **{'load_data_args': str_args})
    elif check_type == "IMAGE_FEATURE":
        file_list = './temp_batch/test.list'
        str_args = './data/from_image_QA/batches.meta'
        file_lists = open(file_list).readlines()
        data_provider = pyDataImage(
            *file_lists, **{'load_data_args': str_args})
    else:
        print "Error: No check type."
        exit()

    # str_args = './data/from_image_QA/0205.dict 0
    # ./data/from_image_QA/batches.meta 0'

    # data_provider = pyDataImageFeatureSen(*file_lists, **{'load_data_args':str_args})
    # data_provider = pyDataImageQA(*file_lists, **{'load_data_args':str_args})

    for i in xrange(2):
        check_data(data_provider.getHeader(), data_provider.getNextBatch(800))
