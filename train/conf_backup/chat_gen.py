# -*- coding: utf-8 -*-

from math import sqrt
import trainer.recurrent_units as recurrent

model_type('recurrent_nn')

# data setting
gen_list = get_config_arg('gen_list', str, './data/gen.all.list')
gen_question_file = get_config_arg('gen_question_file', str, './questions.txt')
gen_result_file = get_config_arg('gen_result_file', str, './answers.txt')

# dictionary setting
dict_file = get_config_arg('dict_file', str, './data/tieba.30m.dict.list')
dict_pkl = get_config_arg('dict_pkl', str, './data/tieba.30m.dict.pickle')

# feature dimension setting
img_feat_dim = 3072
word_embedding_dim = 128
hidden_dim = 256
dictionary_dim = len(open(dict_file).readlines())
start_index = dictionary_dim-2
end_index = dictionary_dim-1

# data provider setting
TestData(
    PyData(
        files = gen_list,
        load_data_module = 'pyDataImageFeatureQA',
        load_data_object = 'pyDataImageFeatureQA',
        load_data_args = dict_pkl + ' 0 ' + str(img_feat_dim) + ' 1.0 0' + ' 1'
    )
)

# hyperparameter setting
Settings(
    batch_size = 1,
    learning_rate = 0,
)

##### network #####
Inputs('img_feat', 'send_id', 'question')

# data layers
DataLayer(name = 'img_feat', size = img_feat_dim)
DataLayer(name = 'question', size = dictionary_dim)
DataLayer(name = 'send_id', size = 1)

# question embedding input: question
MixedLayer(
    name = 'question_embedding',
    size = word_embedding_dim,
    bias = False,
    inputs = TableProjection('question',
        parameter_name = 'word_embedding',
    ),
)

# question hidden input: encoder
recurrent.GatedRecurrentLayerGroup(
    name = 'encoder',
    size = hidden_dim,
    active_type = 'tanh',
    gate_active_type = 'sigmoid',
    inputs = [FullMatrixProjection('question_embedding')],
)

# get last of encoder
Layer(
    name = 'encoder_last',
    type = 'seqlastins',
    active_type = '',
    bias = False,
    inputs = [Input('encoder')],
)

# nonlinear transform of encoder
MixedLayer(
    name = 'encoder_last_projected',
    active_type = 'tanh',
    size = hidden_dim,
    bias = False,
    inputs = FullMatrixProjection('encoder_last'),
)

##### answer part #####
RecurrentLayerGroupBegin('decoder_layer_group',
    in_links = [],
    out_links = ['predict_word'],
    seq_reversed = False,
    generator = Generator(
        max_num_frames = 20,
        beam_size = 5,
        num_results_per_sample = 1,
    ),
)

predict_word_memory = Memory(
    name = 'predict_word',
    size = dictionary_dim,
    boot_with_const_id = start_index,
)

MixedLayer(
    name = 'predict_word_embedding',
    size = word_embedding_dim,
    bias = False,
    inputs = TableProjection(predict_word_memory,
        parameter_name = 'word_embedding',
    ),
)

encoder_last_memory = Memory(
    name = 'encoder_last_memory',
    boot_layer = 'encoder_last_projected',
    boot_bias = False,
    size = hidden_dim,
    is_sequence = False,
)

MixedLayer(
    name = 'encoder_last_memory',
    size = hidden_dim,
    bias = False,
    inputs = IdentityProjection(encoder_last_memory),
)

decoder_state_memory = Memory(
    name = 'decoder',
    boot_layer = 'encoder_last_projected',
    boot_bias = False,
    size = hidden_dim,
    is_sequence = False,
)

recurrent.GatedRecurrentUnit(
    name = 'decoder',
    size = hidden_dim,
    active_type = 'tanh',
    gate_active_type = 'sigmoid',
    #para_prefix = '_decoder',
    out_memory = decoder_state_memory,
    inputs = [encoder_last_memory,
        FullMatrixProjection('predict_word_embedding')],
)

img_feat_memory = Memory(
    name = 'img_feat_memory',
    size = img_feat_dim,
    boot_layer = 'img_feat',
    is_sequence = False,
)

MixedLayer(
    name = 'img_feat_memory',
    size = img_feat_dim,
    bias = False,
    inputs = IdentityProjection(img_feat_memory),
)

# qa embedding output: qa_embedding 
MixedLayer(
    name = 'qa_embedding',
    size = word_embedding_dim,
    active_type = 'tanh',
    bias = True,
    inputs = [FullMatrixProjection('decoder'),
        FullMatrixProjection('predict_word_embedding'),
    ],
)

# output
MixedLayer(
    name = 'qa_output',
    size = dictionary_dim,
    active_type = 'softmax',
    bias = True,
    inputs = TransposedFullMatrixProjection('qa_embedding',
        parameter_name = 'word_embedding',
    ),
)

Layer(
    name = 'predict_word',
    type = 'maxid',
    inputs = ['qa_output'],
)

Layer(
    name = 'eos_check',
    type = 'eos_id',
    eos_id = end_index,
    inputs = ['predict_word'],
)

RecurrentLayerGroupEnd('decoder_layer_group')

# Write questions to file
Evaluator(
    name = 'question_printer',
    type = 'seq_text_printer',
    dict_file = dict_file,
    result_file = gen_question_file,
    #delimited = False,
    inputs = ['send_id', 'question'],
)

# Write answers to file
Evaluator(
    name = 'answer_printer',
    type = 'seq_text_printer',
    dict_file = dict_file,
    result_file = gen_result_file,
    #delimited = False,
    inputs = ['send_id', 'predict_word'],
)
