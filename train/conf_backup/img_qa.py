# -*- coding: utf-8 -*-

from math import sqrt
import trainer.local_recurrent_units as recurrent

model_type('recurrent_nn')

# data setting
train_list = get_config_arg('train_list', str, './train.list')
test_list = get_config_arg('test_list', str, './val.list')

# dictionary setting
dict_file = get_config_arg('dict_file', str, './dict.txt')
dict_pkl = get_config_arg('dict_pkl', str, './dict.pkl')

# feature dimension setting
#img_feat_dim = 3072
img_feat_dim = 4096
word_embedding_dim = 256
hidden_dim = 256
dict_dim = len(open(dict_file).readlines())

# data provider setting
TrainData(
    PyData(
        files = train_list,
        load_data_module = 'pyJoin_train_new',
        load_data_object = 'processData',
        load_data_args = ' '.join([dict_pkl, str(img_feat_dim), '1.0'])
        # async_load_data = True,
    )
)
TestData(
    PyData(
        files = test_list,
        load_data_module = 'pyJoin_val_new',
        load_data_object = 'processData',
        load_data_args = ' '.join([dict_pkl, str(img_feat_dim), '1.0'])
    )
)

# hyperparameter setting
default_initial_mean(0)
default_initial_strategy(0) # 0 for normal, 1 for uniform
default_initial_smart(True)
default_decay_rate(1e-4)
default_num_batches_regularization(1)
default_gradient_clipping_threshold(25)

Settings(
    #batch_size = 128,
    batch_size = 16,
    algorithm = 'sgd',
    learning_method = 'adadelta',
    ada_epsilon = 0.01,
    ada_rou = 0.99,
    learning_rate = 1e-3,
    learning_rate_decay_a = 5e-7,
    learning_rate_decay_b = 0.5,
)

##### network #####
Inputs('img_feat', 'question', 'word', 'next_word')
Outputs('qa_cost')

# data layers
DataLayer(name = 'img_feat', size = img_feat_dim)
DataLayer(name = 'question', size = dict_dim)
DataLayer(name = 'word', size = dict_dim)
DataLayer(name = 'next_word', size = dict_dim)

# question embedding input: question
MixedLayer(
    name = 'question_embedding',
    size = word_embedding_dim,
    bias = False,
    inputs = TableProjection('question',
        parameter_name = 'word_embedding',
        initial_std = 1 / sqrt(word_embedding_dim),
    ),
)

# answer embedding input: word
MixedLayer(
    name = 'word_embedding',
    size = word_embedding_dim,
    bias = False,
    inputs = TableProjection('word',
        parameter_name = 'word_embedding',
        initial_std = 1 / sqrt(word_embedding_dim),
    ),
)

# image embedding input: img_expand
Layer(
    name = 'img_expand',
    type = 'expand',
    bias = False,
    inputs = ['img_feat', 'word'],
)

# question hidden input: encoder
recurrent.LstmRecurrentLayerGroup(
    name = 'encoder',
    size = hidden_dim,
    active_type = 'tanh',
    state_active_type = '',
    gate_active_type = 'sigmoid',
    state_name = 'encoder_state',
    inputs = [FullMatrixProjection('question_embedding', initial_std = 1 / sqrt(hidden_dim))],
)

# get last of encoder
Layer(
    name = 'encoder_last',
    type = 'seqlastins',
    active_type = '',
    bias = False,
    inputs = [Input('encoder')],
)

# get last of encoder state memorty
Layer(
    name = 'encoder_state_last',
    type = 'seqlastins',
    active_type = '',
    bias = False,
    inputs = [Input('encoder_state')],
)

##### answer part #####
recurrent.LstmRecurrentLayerGroup(
    name = 'decoder',
    size = hidden_dim,
    active_type = 'tanh',
    state_active_type = '',
    gate_active_type = 'sigmoid',
    out_boot_layer = 'encoder_last',
    state_boot_layer = 'encoder_state_last',
    inputs = [FullMatrixProjection('word_embedding', initial_std = sqrt(1 / hidden_dim))],
)

# nonlinear transform of decoder
MixedLayer(
    name = 'img_qa_decoder',
    size = hidden_dim,
    active_type = 'stanh',
    inputs = [
        FullMatrixProjection('img_expand', initial_std = sqrt(1 / hidden_dim)),
        FullMatrixProjection('decoder', initial_std = sqrt(1 / hidden_dim)),
    ],
    #drop_rate = 0.5
)

# qa embedding output: qa_embedding
MixedLayer(
    name = 'qa_embedding',
    size = word_embedding_dim,
    bias = Bias(initial_std = 0),
    active_type = 'stanh',
    inputs = FullMatrixProjection('img_qa_decoder', initial_std = sqrt(1 / word_embedding_dim)),
)

# output
MixedLayer(
    name = 'qa_output',
    size = dict_dim,
    active_type = 'softmax',
    bias = Bias(initial_std = 0),
    inputs = TransposedFullMatrixProjection('qa_embedding',
        parameter_name = 'word_embedding',
        initial_std = 1 / sqrt(word_embedding_dim),
    ),
)

# cost
Layer(
    name = 'qa_cost',
    type = 'multi-class-cross-entropy',
    inputs = ['qa_output', 'next_word'],
)

Evaluator(
    name = 'qa_classification_error',
    type = 'classification_error',
    inputs = ['qa_output', 'next_word'],
)
