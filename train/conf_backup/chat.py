# -*- coding: utf-8 -*-

from math import sqrt
import trainer.recurrent_units as recurrent

model_type('recurrent_nn')

# data setting
train_list = get_config_arg('train_list', str, './data/train.all.list')
test_list = get_config_arg('test_list', str, './data/val.all.list')

# dictionary setting
dict_file = get_config_arg('dict_file', str, './data/tieba.30m.dict.list')
dict_pkl = get_config_arg('dict_pkl', str, './data/tieba.30m.dict.pickle')

# feature dimension setting
img_feat_dim = 3072
word_embedding_dim = 128
hidden_dim = 256
dictionary_dim = len(open(dict_file).readlines())

# data provider setting
TrainData(
    PyData(
        files = train_list,
        load_data_module = 'pyJoin_train',
        load_data_object = 'pyDataImageFeatureQA',
        load_data_args = dict_pkl + ' 0 ' + str(img_feat_dim) + ' 1.0 0',
        #async_load_data = True,
    )
)
TestData(
    PyData(
        files = test_list,
        load_data_module = 'pyJoin_test',
        load_data_object = 'pyDataImageFeatureQA',
        load_data_args = dict_pkl + ' 0 ' + str(img_feat_dim) + ' 1.0 0',
    )
)

# hyperparameter setting
default_initial_mean(0)
default_initial_strategy(0) # 0 for normal, 1 for uniform
default_initial_smart(True)
default_decay_rate(1e-4)
default_num_batches_regularization(1)
default_gradient_clipping_threshold(25)

Settings(
    batch_size = 128,
    algorithm = 'sgd',
    learning_method = 'adadelta',
    ada_epsilon = 0.01,
    ada_rou = 0.99,
    learning_rate = 1e-3,
    learning_rate_decay_a = 5e-7,
    learning_rate_decay_b = 0.5,
)

##### network #####
Inputs('img_feat', 'question', 'a_word', 'a_next_word')
Outputs('qa_cost')

# data layers
DataLayer(name = 'img_feat', size = img_feat_dim)
DataLayer(name = 'question', size = dictionary_dim)
DataLayer(name = 'a_word', size = dictionary_dim)
DataLayer(name = 'a_next_word', size = dictionary_dim)

# question embedding input: question
MixedLayer(
    name = 'question_embedding',
    size = word_embedding_dim,
    bias = False,
    inputs = TableProjection('question',
        parameter_name = 'word_embedding',
        initial_std = 1 / sqrt(word_embedding_dim),
    ),
)

# answer embedding input: a_word
MixedLayer(
    name = 'a_word_embedding',
    size = word_embedding_dim,
    bias = False,
    inputs = TableProjection('a_word',
        parameter_name = 'word_embedding',
        initial_std = 1 / sqrt(word_embedding_dim),
    ),
)

# image embedding input: img_expand
Layer(
    name = 'img_expand',
    type = 'expand',
    bias = False,
    inputs = ['img_feat', 'a_word'],
)

# question hidden input: encoder
recurrent.GatedRecurrentLayerGroup(
    name = 'encoder',
    size = hidden_dim,
    active_type = 'tanh',
    gate_active_type = 'sigmoid',
    inputs = [FullMatrixProjection('question_embedding', initial_std = 1 / sqrt(hidden_dim))],
)

# get last of encoder
Layer(
    name = 'encoder_last',
    type = 'seqlastins',
    active_type = '',
    bias = False,
    inputs = [Input('encoder')],
)

# nonlinear transform of encoder
MixedLayer(
    name = 'encoder_last_projected',
    active_type = 'tanh',
    size = hidden_dim,
    bias = False,
    inputs = FullMatrixProjection('encoder_last', initial_std = 1 / sqrt(hidden_dim)),
)

##### answer part #####
RecurrentLayerGroupBegin('decoder_layer_group',
    in_links = ['a_word_embedding'],
    out_links = ['qa_output'],
    seq_reversed = False,
)

encoder_last_memory = Memory(
    name = 'encoder_last_memory',
    boot_layer = 'encoder_last_projected',
    boot_bias = False,
    size = hidden_dim,
    is_sequence = False,
)

MixedLayer(
    name = 'encoder_last_memory',
    size = hidden_dim,
    bias = False,
    inputs = IdentityProjection(encoder_last_memory),
)

decoder_state_memory = Memory(
    name = 'decoder',
    boot_layer = 'encoder_last_projected',
    boot_bias = False,
    size = hidden_dim,
    is_sequence = False,
)

recurrent.GatedRecurrentUnit(
    name = 'decoder',
    size = hidden_dim,
    active_type = 'tanh',
    gate_active_type = 'sigmoid',
    #para_prefix = '_decoder',
    out_memory = decoder_state_memory,
    inputs = [encoder_last_memory,
        FullMatrixProjection('a_word_embedding', initial_std= sqrt(1 / hidden_dim))],
)

# qa embedding output: qa_embedding
MixedLayer(
    name = 'qa_embedding',
    size = word_embedding_dim,
    active_type = 'tanh',
    bias = True,
    inputs = [FullMatrixProjection('decoder', initial_std = sqrt(1 / hidden_dim)),
        FullMatrixProjection('a_word_embedding', initial_std = sqrt(1 / hidden_dim)),
    ],
)

# output
MixedLayer(
    name = 'qa_output',
    size = dictionary_dim,
    active_type = 'softmax',
    bias = Bias(initial_std = 0),
    inputs = TransposedFullMatrixProjection('qa_embedding',
        parameter_name = 'word_embedding',
        initial_std = 1 / sqrt(word_embedding_dim),
    ),
)

RecurrentLayerGroupEnd('decoder_layer_group')

# cost
Layer(
    name = 'qa_cost',
    type = 'multi-class-cross-entropy',
    inputs = ['qa_output', 'a_next_word'],
)

Evaluator(
    name = 'qa_classification_error',
    type = 'classification_error',
    inputs = ['qa_output', 'a_next_word'],
)
