# -*- coding: utf-8 -*-

from math import sqrt
import os
import sys

# data setting
train_list = get_config_arg('train_list', str, './train.list')
test_list = get_config_arg('test_list', str, './val.list')

# dictionary setting
dict_file = get_config_arg('dict_file', str, './dict.txt')
dict_pkl = get_config_arg('dict_pkl', str, './dict.pkl')

# feature dimension setting
img_feat_dim = get_config_arg('img_feat_dim', int, 4096)
word_embedding_dim = 512
hidden_dim = 512
multimodal_dim = 1024
dict_dim = len(open(dict_file).readlines())

# hyperparameter setting
default_decay_rate(8e-4)
default_initial_std(1.0 / sqrt(hidden_dim))

Settings(
    #batch_size = 16,
    batch_size = 128,
    algorithm = 'sgd',
    learning_rate = 5e-3,
    learning_rate_decay_a = 5e-7,
    learning_rate_decay_b = 0.5,
    average_window = 0.5,
    max_average_window = 2500,
    l1weight = 0,
    l2weight = 25,
    c1 = 0.0001,
    backoff = 0.5,
    owlqn_steps = 100,
    max_backoff = 5,
    # usage_ratio = 1.,
    num_batches_per_send_parameter = 1,
    num_batches_per_get_parameter = 1,
)

# data provider setting
#TrainData(
#    PyData(
#        files = train_list,
#        load_data_module = 'pyDataJpegSen_train',
#        load_data_object = 'pyDataImageFeatureSen',
#        load_data_args = ' '.join([dict_pkl, '0', str(img_feat_dim), '1.0', '0'])
#    )
#)
#TestData(
#    PyData(
#        files = test_list,
#        load_data_module = 'pyDataJpegSen_test',
#        load_data_object = 'pyDataImageFeatureSen',
#        load_data_args = ' '.join([dict_pkl, '0', str(img_feat_dim), '1.0', '0'])
#    )
#)
TrainData(
    PyData(
        files = train_list,
        load_data_module = 'pyJunhua_train',
        load_data_object = 'processData',
        load_data_args = ' '.join([dict_pkl, str(img_feat_dim), '1.0'])
        # async_load_data = True,
    )
)
TestData(
    PyData(
        files = test_list,
        load_data_module = 'pyJunhua_val',
        load_data_object = 'processData',
        load_data_args = ' '.join([dict_pkl, str(img_feat_dim), '1.0'])
    )
)

##### network #####
Inputs('img_feat', 'word', 'next_word')
Outputs('cost')

# data layers
DataLayer(name = 'img_feat', size = img_feat_dim)
DataLayer(name = 'word', size = dict_dim)
DataLayer(name = 'next_word', size = dict_dim)

MixedLayer(
    name = 'word_embedding',
    size = word_embedding_dim,
    bias = False,
    inputs = TableProjection('word',
        parameter_name = 'word_embedding',
        initial_std = 0,
    ),
)

Layer(
    name = 'img_expand',
    type = 'expand',
    bias = False,
    inputs = ['img_feat', 'word'],
)

# hidden1
MixedLayer(
    name = 'hidden1',
    size = hidden_dim,
    active_type = 'stanh',
    bias = True,
    inputs = [
        FullMatrixProjection('word_embedding'),
        #        FullMatrixProjection('img_feat'),
    ]
)

# rnn1
Layer(
    name = 'rnn1',
    type = 'lstmemory',
    active_type = 'relu',
    active_state_type = 'linear',
    active_gate_type = 'sigmoid',
    bias = Bias(initial_std = 0),
    inputs = Input('hidden1', initial_std = 0),
)

# hidden2
MixedLayer(
    name = 'hidden2',
    size = multimodal_dim,
    active_type = 'stanh',
    inputs = [
        FullMatrixProjection('hidden1'),
        FullMatrixProjection('img_expand',
                            learning_rate = 0.01,
                            momentum = 0.9,
                            decay_rate = 0.05,
                            initial_mean = 0.0,
                            initial_std = 0.01),
        FullMatrixProjection('rnn1', initial_std = 0, learning_rate = 1)
    ],
    drop_rate = 0.5
)

# hidden3
#Layer(
#    name = 'hidden3',
#    type = 'mixed',
#    size = word_embedding_dim,
#    active_type = 'stanh',
#    inputs = FullMatrixProjection(
#        'hidden2',
#        initial_std = sqrt(1. / multimodal_dim)),
#)

# output
Layer(
    name = 'output',
    type = 'fc',
    size = dict_dim,
    active_type = 'softmax',
    bias = Bias(learning_rate = 0.01,
        momentum = 0.9,
        initial_mean = 0,
        initial_std = 0),
    inputs = [
        Input('hidden2',
              learning_rate = 0.01,
              momentum = 0.9,
              decay_rate = 0.05,
              initial_mean = 0.0,
              initial_std = 0.01)],
    #inputs = TransposedFullMatrixProjection(
    #    'hidden3',
    #    parameter_name = 'wordvecs'),
)

# cost
Layer(
    name = 'cost',
    type = 'multi-class-cross-entropy',
    inputs = ['output', 'next_word'],
)

Evaluator(
    name = 'classification_error',
    type = 'classification_error',
    inputs = ['output', 'next_word'],
)
