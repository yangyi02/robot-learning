# edit-mode: -*- python -*-
# coding:gbk

from math import sqrt
import os
import sys

import trainer.local_recurrent_units as recurrent
model_type('recurrent_nn')

from trainer.recurrent_units import LstmRecurrentUnit

img_feat_dim = get_config_arg('img_feat_dim', int, 4096)
#img_feat_dim = 4096
#img_feat_dim = 3072  # NEED to modified
generating = get_config_arg('generating', int, 0)
train_list = get_config_arg('train_list', str, './train.list')
dict_pkl = get_config_arg('dict_pkl', str, './dict.pkl')
if generating == 1:
    str_args = ' '.join([dict_pkl, '0', str(img_feat_dim), '1.0', '0', str(generating)])
else:
    str_args = ' '.join([dict_pkl, '0', str(img_feat_dim), '1.0', '0'])
test_list = get_config_arg('test_list', str, './val.list')
#test_list = os.path.join(data_root, 'demo.list')
ch_dict_path = get_config_arg('dict_file', str, './dict.txt')
#print "data_root:", data_root
#print "ch_dict_path", ch_dict_path
#en_dict_path = os.path.join(data_root, 'en_word.list.sort')
ch_word_index_dim = len(open(ch_dict_path).readlines())
#en_word_index_dim = len(open(en_dict_path).readlines())

drop_rate = 0.5
default_decay_rate(8e-4)

#print >> sys.stderr, "dict_path", ch_dict_path, en_dict_path
# NEED to modified based on the learned word dictionary

word_embedding_dim = 512
hidden_dim = 512
hidden_dim2 = 1024

default_initial_std(1 / sqrt(hidden_dim)) #1/40


if generating==1: #testing
    Inputs("img_feat", "ch_word")
    Outputs("predicted_id")
else: #training
    Inputs("img_feat", "ch_word", "ch_next_word")
    # Output slots:
    # multiclass cross entropy
    Outputs("ch_cost")

if generating==1:
    TestData(PyData(files=test_list,
                load_data_module="pyDataJpegSen_test",
                load_data_object="pyDataImageFeatureSen",
                load_data_args=str_args
                ))
    # img_feat
    Layer(
        name="img_feat",
        type="data",
        device=0,
        size=img_feat_dim,
    )
    # ch_word
    Layer(
        name="ch_word",
        type="data",
        size=ch_word_index_dim,
    )
else:
    TrainData(PyData(files=train_list,
                     load_data_module="pyDataJpegSen_train",
                     load_data_object="pyDataImageFeatureSen",
                     load_data_args=str_args
                     ))
    TestData(PyData(files=test_list,
                load_data_module="pyDataJpegSen_test",
                load_data_object="pyDataImageFeatureSen",
                load_data_args=str_args
                ))
    # img_feat
    Layer(
        name="img_feat",
        type="data",
        size=img_feat_dim,
    )
    # ch_word
    Layer(
        name="ch_word",
        type="data",
        size=ch_word_index_dim,
    )
    # ch_next_word
    Layer(
        name="ch_next_word",
        type="data",
        size=ch_word_index_dim,
    )

# Image GOOGLE feat

# ch sentence info
# ch_word_embedding input: ch_word
Layer(
    name="ch_word_embedding",
    type="mixed",
    size=word_embedding_dim,
    bias=False,
    inputs=TableProjection(
        "ch_word",
        initial_std=0),
)

# img_expand_ch using img_feat and a_word
Layer(
    name="img_expand_ch",
    type="expand",
    bias=False,
    inputs=["img_feat", "ch_word"],
)

# hidden1_ch
Layer(
    name="hidden1_ch",
    type="mixed",
    device=-1,
    size=hidden_dim,
    active_type="stanh",
    bias=True,
    inputs=[
        FullMatrixProjection("ch_word_embedding"),
        #        FullMatrixProjection("img_feat"),
    ]
)

# rnn1_ch
RecurrentLayerGroupBegin('rnn1_ch' + '_layer_group',
                         in_links = ['hidden1_ch'],
                         out_links = ['rnn1_ch'],
                         seq_reversed = False)

LstmRecurrentUnit(name = 'rnn1_ch',
                  size = hidden_dim/4,
                  active_type = 'relu',
                  state_active_type = 'linear',
                  gate_active_type = 'sigmoid',
                  inputs = [IdentityProjection('hidden1_ch')],
                  para_prefix = None,
                  error_clipping_threshold = 0,
                )

RecurrentLayerGroupEnd('rnn1_ch' + '_layer_group')

# hidden2_ch
Layer(
    name="hidden2_ch",
    type="mixed",
    size=hidden_dim2,
    active_type="stanh",
    inputs=[
        FullMatrixProjection("hidden1_ch"),
        FullMatrixProjection("img_expand_ch",
                            learning_rate=0.01,
                            momentum=0.9,
                            decay_rate=0.05,
                            initial_mean=0.0,
                            initial_std=0.01),
        FullMatrixProjection("rnn1_ch", initial_std=0, learning_rate=1)
    ],
    drop_rate=drop_rate
)

# hidden3_ch
#Layer(
#    name="hidden3_ch",
#    type="mixed",
#    device=-1,
#    size=word_embedding_dim,
#    active_type="stanh",
#    inputs=FullMatrixProjection(
#        "hidden2_ch",
#        initial_std=sqrt(1. / hidden_dim2)),
#)

# output_ch
Layer(
    name="output_ch",
    type="fc",
    size=ch_word_index_dim,
    active_type="softmax",
    bias=Bias(learning_rate=0.01,  # momentum=0.9,
                momentum=0.9,
                initial_mean=0,
                initial_std=0),
    inputs = [
        Input("hidden2_ch",
              learning_rate=0.01,
              momentum=0.9,
              decay_rate=0.05,
              initial_mean=0.0,
              initial_std=0.01)],
    #inputs=TransposedFullMatrixProjection(
    #    "hidden3_ch",
    #    parameter_name="wordvecs_ch"),
)

# ch sentence cost
if generating==1: #testing
    Layer(
       name = "predicted_id",
       type = "maxid",
       inputs = "output_ch",
    )
else:
    Layer(
        name="ch_cost",
        type="multi-class-cross-entropy",
        inputs=["output_ch", "ch_next_word"],
    )

    Layer(
        name="ch_error_layer",
        type="classification_error",
        inputs=["output_ch", "ch_next_word"]
    )

    Evaluator(
        name="ch_error_classfication",
        type="sum",
        inputs="ch_error_layer",
    )

Settings(
    algorithm='sgd',
    batch_size = 128,
    average_window = 0.5,
    max_average_window = 2500,
    learning_rate = 5e-3,
    learning_rate_decay_a = 5e-7,
    learning_rate_decay_b = 0.5,
    l1weight = 0,
    l2weight = 25,
    c1 = 0.0001,
    backoff = 0.5,
    owlqn_steps = 100,
    max_backoff = 5,
    # usage_ratio = 1.,
    num_batches_per_send_parameter = 1,
    num_batches_per_get_parameter = 1,
)
