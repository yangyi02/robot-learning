# -*- coding: utf-8 -*-

from math import sqrt
import trainer.local_recurrent_units as recurrent

model_type('recurrent_nn')

# data setting
gen_list = get_config_arg('gen_list', str, './gen.list')
result_file = get_config_arg('result_file', str, './result.txt')

# dictionary setting
dict_file = get_config_arg('dict_file', str, './dict.txt')
dict_pkl = get_config_arg('dict_pkl', str, './dict.pkl')

# feature dimension setting
img_feat_dim = 3072
word_embedding_dim = 256
hidden_dim = 256
dict_dim = len(open(dict_file).readlines())
start_index = dict_dim-2
end_index = dict_dim-1

# hyperparameter setting
Settings(
    batch_size = 1,
    learning_rate = 0,
)

# data provider setting
TestData(
    PyData(
        files = gen_list,
        load_data_module = 'pyJoin_test_new',
        load_data_object = 'processData',
        load_data_args = ' '.join([dict_pkl, str(img_feat_dim), '1.0'])
    )
)

##### network #####
Inputs('question_id', 'img_feat', 'question')

# data layers
DataLayer(name = 'question_id', size = 1)
DataLayer(name = 'img_feat', size = img_feat_dim)
DataLayer(name = 'question', size = dict_dim)

# question embedding input: question
MixedLayer(
    name = 'question_embedding',
    size = word_embedding_dim,
    bias = False,
    inputs = TableProjection('question',
        parameter_name = 'word_embedding',
    ),
)

# question hidden input: encoder
recurrent.LstmRecurrentLayerGroup(
    name = 'encoder',
    size = hidden_dim,
    active_type = 'tanh',
    state_active_type = '',
    gate_active_type = 'sigmoid',
    state_name = 'encoder_state',
    inputs = [FullMatrixProjection('question_embedding')],
)

# get last of encoder
Layer(
    name = 'encoder_last',
    type = 'seqlastins',
    active_type = '',
    bias = False,
    inputs = [Input('encoder')],
)

# get last of encoder state memorty
Layer(
    name = 'encoder_state_last',
    type = 'seqlastins',
    active_type = '',
    bias = False,
    inputs = [Input('encoder_state')],
)

##### answer part #####
RecurrentLayerGroupBegin('decoder_layer_group',
    in_links = [],
    out_links = ['predict_word'],
    seq_reversed = False,
    generator = Generator(
        max_num_frames = 20,
        beam_size = 5,
        num_results_per_sample = 1,
    ),
)

predict_word_memory = Memory(
    name = 'predict_word',
    size = dict_dim,
    boot_with_const_id = start_index,
)

MixedLayer(
    name = 'predict_word_embedding',
    size = word_embedding_dim,
    bias = False,
    inputs = TableProjection(predict_word_memory,
        parameter_name = 'word_embedding',
    ),
)

MixedLayer(
    name = 'decoder_transform_input',
    size = hidden_dim * 4,
    bias = False,
    inputs = FullMatrixProjection('predict_word_embedding',
        parameter_name = '_decoder_transform_input.w0'),
)

recurrent.LstmRecurrentUnit(
    name = 'decoder',
    size = hidden_dim,
    active_type = 'tanh',
    state_active_type = '',
    gate_active_type = 'sigmoid',
    out_boot_layer = 'encoder_last',
    state_boot_layer = 'encoder_state_last',
    inputs = [IdentityProjection('decoder_transform_input')],
)

img_feat_memory = Memory(
    name = 'img_feat_memory',
    size = img_feat_dim,
    boot_layer = 'img_feat',
    is_sequence = False,
)

MixedLayer(
    name = 'img_feat_memory',
    size = img_feat_dim,
    bias = False,
    inputs = IdentityProjection(img_feat_memory),
)

# nonlinear transform of decoder
MixedLayer(
    name = 'img_qa_decoder',
    size = hidden_dim,
    bias = Bias(parameter_name = '_img_qa_decoder.wbias'),
    active_type = 'stanh',
    inputs = [
        FullMatrixProjection(img_feat_memory, parameter_name = '_img_qa_decoder.w0'),
        FullMatrixProjection('decoder', parameter_name = '_img_qa_decoder.w1'),
    ],
    #drop_rate = 0.5
)

# qa embedding output: qa_embedding
MixedLayer(
    name = 'qa_embedding',
    size = word_embedding_dim,
    bias = Bias(parameter_name = '_qa_embedding.wbias'),
    active_type = 'stanh',
    inputs = FullMatrixProjection('img_qa_decoder', parameter_name = '_qa_embedding.w0'),
)

# output
MixedLayer(
    name = 'qa_output',
    size = dict_dim,
    active_type = 'softmax',
    bias = Bias(parameter_name = '_qa_output.wbias'),
    inputs = TransposedFullMatrixProjection('qa_embedding',
        parameter_name = 'word_embedding',
    ),
)

Layer(
    name = 'predict_word',
    type = 'maxid',
    inputs = ['qa_output'],
)

Layer(
    name = 'eos_check',
    type = 'eos_id',
    eos_id = end_index,
    inputs = ['predict_word'],
)

RecurrentLayerGroupEnd('decoder_layer_group')

# Write question and answer pairs to file
Evaluator(
    name = 'question_answer_printer',
    type = 'seq_text_printer',
    dict_file = dict_file,
    result_file = result_file,
    #delimited = False,
    inputs = ['question_id', 'question', 'predict_word'],
)
