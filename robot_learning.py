# -*- coding: utf-8 -*-
"""
Created on Mon Dec  7 16:25:55 2015

@author: yangyi05
"""

import sys
import os
from os import path
import io
import logging
import optparse
import json
from config import conf
from prepare import make_dict, make_batch, utils
from make_model_diagram import make_diagram
from draw import parse_log_and_draw
from evaluate import make_json, evaluate
from html import make_html

logging.basicConfig(format='[%(levelname)s %(asctime)s %(filename)s:%(lineno)s] %(message)s')
logging.getLogger().setLevel(logging.INFO)

class RobotLearning(object):
    """
    This class defines the whole process of multi-task learning for
    (image) question-answering, captioning, and dialog.
    """
    def __init__(self, opts):
        """
        Initialize configuration for data preparation, training, evaluation and testing.
        """
        # self.task_conf: global configuration for multi-task learning.
        self.task_conf = conf.get_task_conf(opts)
        # self.data_confs: array storing data configuration w.r.t. every task.
        self.data_confs = conf.get_data_confs(self.task_conf)
        # self.eval_confs: array storing evaluation configuration w.r.t. every task.
        self.eval_confs = conf.get_eval_confs(self.task_conf, self.data_confs)
        # self.test_confs: array storing testing configuration w.r.t. every task.
        self.test_confs = conf.get_test_confs(self.task_conf, self.data_confs)

    def prepare_data(self):
        """
        Prepare image features, word dictionary, training and validation batches for mutli-task.
        The batches created in each task will be used to make final multi-task batches.
        The word dictionary created in each task will be used to make final multi-task dictionary.
        """
        # Prepare image feature for each task
        # This assumes the image recongition model is already pre-trained.
        for data_conf in self.data_confs:
            self.prepare_feature(data_conf)
        # Prepare training and testing text files for every dataset.
        # We transfer data from different datasets to a single text format.
        # Later we will create batches from these text files.
        for data_conf in self.data_confs:
            self.prepare_text(data_conf)
        # Prepare word dictionary and count word frequency for every dataset.
        # Later we will merge all the dictionaries into one dictionary for multi-task learning.
        for data_conf in self.data_confs:
            self.prepare_dictionary(data_conf)
        # Prepare batches that contain image features and natural languages for every dataset.
        # Later we will create file lists from these batch files.
        for data_conf in self.data_confs:
            self.prepare_batch(data_conf)
        # Prepare training and validation file list for each dataset.
        # Later we will merge all the file lists into one file list for multi-task learning.
        for data_conf in self.data_confs:
            utils.list_folders(data_conf['train_batch_dir'], data_conf['train_list'])
            utils.list_folders(data_conf['val_batch_dir'], data_conf['val_list'])
        # Prepare evaluation file list for every dataset.
        # We will not merge the evaluation file lists because we want to see the performance
        # of our multi-task model on every dataset.
        for eval_conf in self.eval_confs:
            utils.list_folders(eval_conf['eval_batch_dir'], eval_conf['eval_list'])
        # Prepare testing file list for every dataset.
        # We will not merge the testing file lists because we want to see the results
        # of our multi-task model on every dataset.
        for test_conf in self.test_confs:
            utils.list_folders(test_conf['test_batch_dir'], test_conf['test_list'])
        # Prepare ground-truth json files for every dataset.
        # We will use these json files because the benchmark evaluation code use them as inputs.
        # (TODO: write my own benchmark evaluation code to get rid of ground-truth json files.)
        for data_conf in self.data_confs:
            self.prepare_gt_json(data_conf)

    @staticmethod
    def prepare_feature(data_conf):
        """
        Prepare image features files for training and testing.
        """
        # Prepare a image file list that contains image paths.
        image_list = data_conf['image_list']
        if image_list is not '' and not path.exists(image_list):
            utils.list_folders(data_conf['image_folders'], image_list)
        logging.info('Image list save to %s', image_list)
        # Extract image features for every image in the image file list.
        feature_module = data_conf['feature_module']
        feature_func = data_conf['feature_func']
        image_feature_dir = data_conf['image_feature_dir']
        oversample = data_conf['image_feature_oversample']
        if feature_func is not '':
            if not path.exists(image_feature_dir):
                module = __import__(feature_module, fromlist=[feature_func])
                getattr(module, feature_func)(image_list, image_feature_dir, oversample)
            utils.list_folders(image_feature_dir, data_conf['img_feat_list'])
        logging.info('Image features save to %s', image_feature_dir)

    @staticmethod
    def prepare_text(data_conf):
        """
        Prepare training and testing text files.
        Due to different dataset format, you will need to write hacky codes a new dataset.
        After preparation, all text files should follow the same data format.
        """
        prepare_module = data_conf['prepare_module']
        module = __import__(prepare_module, fromlist=['prepare_text'])
        prepare_func = getattr(module, 'prepare_text')
        train_data_file = data_conf['train_data_file']
        val_data_file = data_conf['val_data_file']
        debug = data_conf['debug']
        if not path.exists(train_data_file) or not path.exists(val_data_file):
            prepare_func(train_data_file, val_data_file, debug)
        logging.info('Training data save to %s', train_data_file)
        logging.info('Validation data save to %s', val_data_file)

    @staticmethod
    def prepare_gt_json(data_conf):
        """
        Make ground truth json files for evaluation. 
        This is because both image caption and VQA evaluation code load ground truth json files.
        Due to different dataset format, you will need to write hacky codes a new json file.
        After preparation, all dataset follow the same data format.
        """
        prepare_module = data_conf['prepare_module']
        module = __import__(prepare_module, fromlist=['prepare_json'])
        prepare_func = getattr(module, 'prepare_json')
        question_gt_file = data_conf['question_gt_file']
        answer_gt_file = data_conf['answer_gt_file']
        val_data_file = data_conf['val_data_file']
        if question_gt_file is '': # A image caption dataset
            if not path.exists(answer_gt_file):
                prepare_func(val_data_file, answer_gt_file)
        elif not path.exists(question_gt_file) or not path.exists(answer_gt_file):
            prepare_func(val_data_file, question_gt_file, answer_gt_file)
        logging.info('Ground truth questions save to %s', question_gt_file)
        logging.info('Ground truth answers save to %s', answer_gt_file)

    @staticmethod
    def prepare_dictionary(data_conf):
        """
        Make word dictionary as well as compute word frequency count
        """
        word_freq_file = data_conf['word_freq_file']
        if not path.exists(word_freq_file):
            data = io.open(data_conf['train_data_file'], 'rb').readlines()
            make_dict.compute_frequency(data, word_freq_file)
        dict_pkl = data_conf['dict_pkl']
        dict_file = data_conf['dict_file']
        if not path.exists(dict_pkl) or not path.exists(dict_file):
            dict_freq_thresh = data_conf['dict_freq_thresh']
            make_dict.make_dictionary(word_freq_file, dict_freq_thresh, dict_pkl, dict_file)
        logging.info('Data dictionary save to %s', dict_pkl)

    @staticmethod
    def prepare_batch(data_conf):
        """
        Make training and validation batches.
        Note that the batch files here only contain image ids and texts.
        The image features are stored in another image feature batch directory.
        The dictionary is also stored in a dictionary pickle file.
        During training and testing, dictionary and all image features will be loaded into memory.
        This will save a lot of disks because usually one image can correspond to multiple texts.
        """
        train_data_file = data_conf['train_data_file']
        val_data_file = data_conf['val_data_file']
        train_batch_dir = data_conf['train_batch_dir']
        val_batch_dir = data_conf['val_batch_dir']
        batch_size = data_conf['batch_size']
        if not path.exists(train_batch_dir) or not path.exists(val_batch_dir):
            make_batch.make_batch(train_data_file, batch_size, train_batch_dir)
            make_batch.make_batch(val_data_file, batch_size, val_batch_dir)
        logging.info('Train data batch save to %s', train_batch_dir)
        logging.info('Validation data batch save to %s', val_batch_dir)

    def prepare_task_data(self):
        """
        Make multi-task word dictionary and training / validation file lists.
        Our task uses multiple dataset and multiple forms of language tasks (caption, qa, dialog).
        """
        self.prepare_task_dictionary()
        self.prepare_task_feature_list()
        self.prepare_task_file_list()

    def prepare_task_dictionary(self):
        """
        Make dictionary for multi-task training and testing.
        Our task uses multiple dataset and multiple forms of language tasks (caption, qa, dialog).
        """
        word_freq_files, dict_pkls = [], []
        for data_conf in self.data_confs:
            word_freq_files.append(data_conf['word_freq_file'])
            dict_pkls.append(data_conf['dict_pkl'])
        # Compute word frequency for multi-task learning
        word_freq_file = self.task_conf['word_freq_file']
        if not path.exists(word_freq_file):
            make_dict.merge_frequency(word_freq_files, word_freq_file)
        # Merge dictionaries for multi-task learning
        dict_pkl = self.task_conf['dict_pkl']
        dict_file = self.task_conf['dict_file']
        if not path.exists(dict_pkl) or not path.exists(dict_file):
            make_dict.merge_dictionary(dict_pkls, dict_pkl, dict_file)
        logging.info('Dictionary save to %s', dict_pkl)

    def prepare_task_file_list(self):
        """
        Make file list for training and testing batches.
        Our task uses multiple dataset and multiple forms of language tasks (caption, qa, dialog).
        """
        train_batch_dirs, val_batch_dirs = [], []
        for data_conf in self.data_confs:
            train_batch_dirs.append(data_conf['train_batch_dir'])
            val_batch_dirs.append(data_conf['val_batch_dir'])
        utils.list_folders(train_batch_dirs, self.task_conf['train_list'])
        logging.info('File list save to %s', self.task_conf['train_list'])
        utils.list_folders(val_batch_dirs, self.task_conf['val_list'])
        logging.info('File list save to %s', self.task_conf['val_list'])

    def prepare_task_feature_list(self):
        """
        Make file list for image features.
        Our task uses multiple dataset and multiple forms of language tasks (caption, qa, dialog).
        """
        feature_lists = []
        for data_conf in self.data_confs:
            feature_lists.append(data_conf['img_feat_list'])
        utils.merge_lists(feature_lists, self.task_conf['img_feat_list'])
        logging.info('Image feature list save to %s', self.task_conf['img_feat_list'])

    def draw_net(self):
        """
        Draw the network graph
        """
        train_net_graph = self.task_conf['train_net_graph']
        if not path.exists(train_net_graph):
            train_list = self.task_conf['train_list']
            val_list = self.task_conf['val_list']
            dict_pkl = self.task_conf['dict_pkl']
            dict_file = self.task_conf['dict_file']
            conf_args = 'train_list=%s,test_list=%s,dict_file=%s,dict_pkl=%s' \
                    % (train_list, val_list, dict_file, dict_pkl)
            make_diagram(self.task_conf['train_conf'], 'net.dot', conf_args)
            os.system('dot -Tjpg net.dot -o %s' % train_net_graph)
            os.system('rm net.dot')
        logging.info('Draw net save to %s', train_net_graph)

        test_net_graph = self.task_conf['test_net_graph']
        if not path.exists(test_net_graph):
            test_list = self.task_conf['val_list']
            result_file = ''
            dict_pkl = self.task_conf['dict_pkl']
            dict_file = self.task_conf['dict_file']
            conf_args = 'gen_list=%s,result_file=%s,dict_file=%s,dict_pkl=%s' \
                    % (test_list, result_file, dict_file, dict_pkl)
            make_diagram(self.task_conf['test_conf'], 'net.dot', conf_args)
            os.system('dot -Tpng net.dot -o %s' % test_net_graph)
            os.system('rm net.dot')
        logging.info('Draw net save to %s', test_net_graph)

    def train(self):
        """
        Train multi-task model
        """
        train_log = self.task_conf['train_log']
        if not path.exists(train_log):
            train_list = self.task_conf['train_list']
            val_list = self.task_conf['val_list']
            dict_pkl = self.task_conf['dict_pkl']
            dict_file = self.task_conf['dict_file']
            img_feat_dim = self.task_conf['img_feat_dim']
            img_feat_list = self.task_conf['img_feat_list']
            conf_args = 'train_list=%s,test_list=%s,dict_file=%s,dict_pkl=%s,img_feat_list=%s,img_feat_dim=%d' \
                    % (train_list, val_list, dict_file, dict_pkl, img_feat_list, img_feat_dim)

            train_conf = self.task_conf['train_conf']
            num_train_epoch = self.task_conf['num_train_epoch']
            model_dir = self.task_conf['model_dir']
            if self.task_conf['debug'] and not self.task_conf['use_gpu']:
                cmd = 'paddle_trainer --config=%s --use_gpu=0 --trainer_count=1 ' \
                        '--log_period=200 --test_period=0 --num_passes=%d --save_dir=%s ' \
                        '--show_layer_stat=True --config_args %s 2>&1 | tee %s' \
                        % (train_conf, num_train_epoch, model_dir, conf_args, train_log)
            else:
                cmd = 'paddle_trainer --config=%s --use_gpu=1 --trainer_count=2 ' \
                        '--log_period=200 --test_period=0 --num_passes=%d --save_dir=%s ' \
                        '--config_args %s 2>&1 | tee %s' \
                         % (train_conf, num_train_epoch, model_dir, conf_args, train_log)
            os.system(cmd)
        logging.info('Train logs save to %s', train_log)

    def draw_error_curve(self):
        """
        Draw training and testing error curves from train log
        """
        train_curve = self.task_conf['train_curve']
        if not path.exists(train_curve):
            parse_log_and_draw.main(self.task_conf['train_log'], train_curve,
                                    self.task_conf['name'])
        logging.info('Train error curves save to %s', train_curve)

    def evaluate(self):
        """
        Evaluate multi-task model on multiple tasks
        """
        for eval_conf in self.eval_confs:
            eval_log = eval_conf['eval_log']
            if not path.exists(eval_log):
                test_list = eval_conf['eval_list']
                dict_pkl = self.task_conf['dict_pkl']
                dict_file = self.task_conf['dict_file']
                img_feat_dim = self.task_conf['img_feat_dim']
                img_feat_list = self.task_conf['img_feat_list']
                conf_args = 'train_list=%s,test_list=%s,dict_file=%s,dict_pkl=%s,img_feat_list=%s,img_feat_dim=%d' \
                        % (test_list, test_list, dict_file, dict_pkl, img_feat_list, img_feat_dim)

                train_conf = self.task_conf['train_conf']
                num_train_epoch = self.task_conf['num_train_epoch']
                model_dir = self.task_conf['model_dir']
                test_pass = num_train_epoch - 1
                cmd = 'paddle_trainer --config=%s --use_gpu=1 --trainer_count=2 ' \
                        '--log_period=200 --test_period=0 --num_passes=%d --save_dir=%s ' \
                        '--job=test --test_pass=%d ' \
                        '--config_args %s 2>&1 | tee %s' \
                        % (train_conf, num_train_epoch, model_dir, test_pass, conf_args, eval_log)
                os.system(cmd)
            logging.info('Evaluation log save to %s', eval_log)

    def predict(self):
        """
        Predict results of multi-task model on multiple tasks
        """
        for test_conf in self.test_confs:
            result_file = test_conf['result_file']
            if not path.exists(result_file):
                test_list = test_conf['test_list']
                dict_pkl = self.task_conf['dict_pkl']
                dict_file = self.task_conf['dict_file']
                img_feat_dim = self.task_conf['img_feat_dim']
                img_feat_list = self.task_conf['img_feat_list']
                conf_args = 'gen_list=%s,result_file=%s,dict_file=%s,dict_pkl=%s,img_feat_list=%s,img_feat_dim=%d' \
                        % (test_list, result_file, dict_file, dict_pkl, img_feat_list, img_feat_dim)

                test_conf = self.task_conf['test_conf']
                num_train_epoch = self.task_conf['num_train_epoch']
                model_dir = self.task_conf['model_dir']
                test_pass = num_train_epoch - 1
                cmd = 'paddle_trainer --config=%s --use_gpu=0 --trainer_count=8 ' \
                        '--log_period=200 --test_period=0 --num_passes=%d --save_dir=%s ' \
                        '--job=test --test_pass=%d ' \
                        '--config_args %s' \
                         % (test_conf, num_train_epoch, model_dir, test_pass, conf_args)
                os.system(cmd)
            logging.info('Prediction results save to %s', result_file)

    def benchmark_evaluation(self):
        """
        Finally, evaluate results based on public benchmark evalation criteria
        """
        for test_conf in self.test_confs:
            result_file = test_conf['result_file']
            result_json_file = test_conf['result_json_file']
            if not path.exists(result_json_file):
                make_json_func = test_conf['make_json_func']
                getattr(make_json, make_json_func)(result_file, result_json_file)
            question_gt_file = test_conf['question_gt_file']
            answer_gt_file = test_conf['answer_gt_file']
            score_file = test_conf['score_file']
            if not path.exists(score_file):
                eval_func = test_conf['eval_func']
                getattr(evaluate, eval_func)(result_json_file, question_gt_file,
                                             answer_gt_file, score_file)
            logging.info('Benchmark scores save to %s', score_file)

    def send_email(self):
        """
        Send email notification
        """
        subject = self.task_conf['model_dir']
        message = io.open(self.task_conf['train_log']).readlines()[-6:]
        for test_conf in self.test_confs:
            score_file = test_conf['score_file']
            scores = json.load(io.open(score_file, 'rb'))
            for key in scores.keys():
                if isinstance(scores[key], (int, long, float)):
                    message.append(key + ': ' + '%.3f\n' % scores[key])
        print ''.join(message)
        to_addr = 'yangyi02@gmail.com'
        from_addr = 'yangyi05@baidu.com'
        server = 'hotswap-in.baidu.com'
        attach_file = self.task_conf['train_log']
        cmd = '/home/yi/code/tools/sendEmail -u %s -m %s -t %s -f %s -s %s -a %s' \
                % (subject, str(message), to_addr, from_addr, server, attach_file)
        os.system(cmd)
        logging.info('Email send to %s', to_addr)

    def export_html(self):
        """
        Generate html from prediction results
        """
        for test_conf in self.test_confs:
            id_url_file = test_conf['id_url_file']
            if not path.exists(id_url_file):
                id_url_module = test_conf['id_url_module']
                id_url_func = test_conf['id_url_func']
                if id_url_func is not '':
                    module = __import__(id_url_module, fromlist=[id_url_func])
                    getattr(module, id_url_func)(id_url_file)
            task_type = test_conf['task_type']
            result_file = test_conf['result_file']
            html_file = test_conf['html_file']
            make_html.make_html(task_type, id_url_file, result_file, html_file)
            logging.info('Prediction results save to %s', html_file)

def main():
    """
    Main function for training, testing and demo
    Parse command line options and start the server.
    """
    parser = optparse.OptionParser()
    parser.add_option('-t', '--task', help='training task', type='string', default='')
    parser.add_option('--train_conf', help='training config', type='string', default='')
    parser.add_option('--test_conf', help='prediction config', type='string', default='')
    parser.add_option('--feature_type', help='image feature type', type='string', default='')
    parser.add_option('--train_epoch', help='training epoch', type='int', default=100)
    parser.add_option('-d', '--debug', help='enable debug mode', action='store_true', default=False)
    parser.add_option('-g', '--gpu', help="use gpu mode", action='store_true', default=False)
    opts, args = parser.parse_args()
    robot_learning = RobotLearning(opts)
    robot_learning.prepare_data()
    robot_learning.prepare_task_data()
    robot_learning.draw_net()
    robot_learning.train()
    robot_learning.draw_error_curve()
    robot_learning.evaluate()
    robot_learning.predict()
    robot_learning.benchmark_evaluation()
    robot_learning.send_email()
    robot_learning.export_html()
    logging.info('Done')

if __name__ == '__main__':
    main()
