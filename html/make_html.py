'''
Created on 11/24, 2015
create a web page for easy evaluation of generated text
@author: yangyi05
'''

import sys
import io
import logging

logging.basicConfig(format='[%(levelname)s %(asctime)s %(filename)s:%(lineno)s] %(message)s')
logging.getLogger().setLevel(logging.INFO)

def read_urls(text_file):
    """
    Load image id and image url pairs
    """
    data = io.open(text_file, 'rb').readlines()
    image_ids, image_urls = [], []
    for line in data:
        items = line.strip().split('\t')
        image_ids.append(items[0])
        image_urls.append(items[1])
    return image_ids, image_urls

def read_text(text_file):
    """
    Load result text file, the text file consists of 3 columns:
    id, question, answer
    """
    data = io.open(text_file, 'rb').readlines()
    question_ids, questions, answers = [], [], []
    for line in data:
        items = line.strip().split('\t')
        question_ids.append(items[0].strip())
        questions.append(items[1].strip())
        # Remove end sign in the prediction answers
        answers.append(items[2].replace('$$E$$', '').strip())
    return question_ids, questions, answers

def wrap_image_text(url, question, answer):
    """
    Create image text pair in html
    """
    text = '<tr><td><img src=\"' + url + '\" style=\"width:256px\"></td>'
    text += '<td>'
    text += question.strip()
    text += '</td><td>'
    text += answer.strip()
    text += "</td>  </tr>"
    return text

def make_html(task_type, id_url_file, text_file, html_file):
    """
    Create html file
    """
    question_ids, questions, answers = read_text(text_file)
    question_dict = dict(zip(question_ids, questions))
    answer_dict = dict(zip(question_ids, answers))

    if id_url_file is not '':
        image_ids, image_urls = read_urls(id_url_file)
        url_dict = dict(zip(image_ids, image_urls))
    else:
        image_ids = question_ids

    file_out = io.open(html_file, 'wb')
    head = "<!DOCTYPE html> \n<meta charset=\"UTF-8\"> \n<html> \n<body> \n<h1>Image to text</h1> \n<table style=\"width:80%\" cellpadding=\"10\">"
    foot = "</table> \n</body> \n</html>"

    print >> file_out, head
    print >> file_out, "<tr><td>Image</td><td>Question</td><td>Answer</td></tr>"
    for question_id in list(set(question_ids)):
        if task_type == 'image caption':
            image_id = question_id
        elif task_type == 'image qa' or task_type == 'qa':
            image_id = question_id[:-1]
        else:
            logging.fatal('Unrecognized task: %s', task_type)
        if image_id in url_dict:
            print >>file_out, wrap_image_text(url_dict[image_id], question_dict[question_id], answer_dict[question_id])
    print >> file_out, foot

if __name__ == '__main__':
    if len(sys.argv) < 4:
        sys.stderr.write('Usage: *.py task_type id_url_file text_file html_file\n')
        sys.exit(1)
    task_type, id_url_file, text_file, html_file = sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4]
    make_html(task_type, id_url_file, text_file, html_file)

