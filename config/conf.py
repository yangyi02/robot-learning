# -*- coding: utf-8 -*-
"""
Created on Mon Dec  7 15:29:24 2015

@author: yangyi05
"""

import os
from os import path
from feature_info import add_img_feat_dim

def get_task_conf(opts):
    """
    Specify task information for data preparation, training and testing
    """
    module = __import__('config.task_info', fromlist=[opts.task])
    conf = getattr(module, opts.task)()
    conf['use_gpu'] = opts.gpu
    conf['debug'] = opts.debug
    # cache_dir stores all the training, testing results
    cache_dir = 'cache'
    if not path.exists(cache_dir):
        os.mkdir(cache_dir)
    conf['cache_dir'] = cache_dir
    conf['feature_type'] = opts.feature_type
    conf = add_img_feat_dim(conf, opts.feature_type)
    task_dir = path.join(cache_dir, conf['name'])
    if conf['debug']:
        task_dir = task_dir + '_debug'
    if not path.exists(task_dir):
        os.mkdir(task_dir)
    task_dir = path.join(task_dir, conf['feature_type'])
    if not path.exists(task_dir):
        os.mkdir(task_dir)
    conf['task_dir'] = task_dir
    conf['word_freq_file'] = path.join(task_dir, 'word_freq.txt')
    conf['dict_pkl'] = path.join(task_dir, 'dict.pkl')
    conf['dict_file'] = path.join(task_dir, 'dict.txt')
    conf['img_feat_list'] = path.join(task_dir, 'img_feat.list')
    conf['train_list'] = path.join(task_dir, 'train.list')
    conf['val_list'] = path.join(task_dir, 'val.list')
    conf['num_train_epoch'] = opts.train_epoch
    conf['train_conf'] = opts.train_conf
    conf['test_conf'] = opts.test_conf
    conf['model_name'] = path.splitext(path.basename(opts.train_conf))[0]
    model_dir = path.join(task_dir, conf['model_name'])
    if not path.exists(model_dir):
        os.mkdir(model_dir)
    conf['model_dir'] = model_dir
    conf['train_log'] = path.join(model_dir, 'train.log')
    conf['train_curve'] = path.join(model_dir, 'train')
    conf['train_net_graph'] = path.join(model_dir, 'net_train.jpg')
    conf['test_net_graph'] = path.join(model_dir, 'net_test.jpg')
    # data_cache_dir stores all the training and testing data
    data_cache_dir = 'cache_data'
    if not path.exists(data_cache_dir):
        os.mkdir(data_cache_dir)
    conf['data_cache_dir'] = data_cache_dir
    return conf

def get_data_confs(task_conf):
    """
    Specify data information for data preparation, training and validation
    """
    confs = []
    for data in task_conf['data_infos']:
        module = __import__('config.data_info', fromlist=[data])
        conf = getattr(module, data)(task_conf['feature_type'])
        data_dir = path.join(task_conf['data_cache_dir'], conf['name'])
        conf['debug'] = task_conf['debug']
        if conf['debug']:
            data_dir = data_dir + '_debug'
        if not path.exists(data_dir):
            os.mkdir(data_dir)
        data_dir = path.join(data_dir, task_conf['feature_type'])
        if not path.exists(data_dir):
            os.mkdir(data_dir)
        conf['data_dir'] = data_dir
        conf['train_data_file'] = path.join(data_dir, 'train_data.txt')
        conf['val_data_file'] = path.join(data_dir, 'val_data.txt')
        conf['word_freq_file'] = path.join(data_dir, 'word_freq.txt')
        conf['dict_pkl'] = path.join(data_dir, 'dict.pkl')
        conf['dict_file'] = path.join(data_dir, 'dict.txt')
        conf['img_feat_list'] = path.join(data_dir, 'img_feat.list')
        conf['train_batch_dir'] = path.join(data_dir, 'train_batches')
        conf['val_batch_dir'] = path.join(data_dir, 'val_batches')
        conf['train_list'] = path.join(data_dir, 'train.list')
        conf['val_list'] = path.join(data_dir, 'val.list')
        if conf['id_url_file'] is not '':
            conf['id_url_file'] = path.join(data_dir, conf['id_url_file'])
        if conf['question_gt_file'] is not '':
            conf['question_gt_file'] = path.join(data_dir, conf['question_gt_file'])
        if conf['answer_gt_file'] is not '':
            conf['answer_gt_file'] = path.join(data_dir, conf['answer_gt_file'])
        feature_dir = path.join(task_conf['data_cache_dir'], conf['feature_name'])
        if not path.exists(feature_dir):
            os.mkdir(feature_dir)
        conf['feature_dir'] = feature_dir
        if conf['image_list'] is not '':
            conf['image_list'] = path.join(feature_dir, conf['image_list'])
            conf['image_feature_dir'] = path.join(feature_dir, conf['image_feature_dir'])
        confs.append(conf)
    return confs

def get_eval_confs(task_conf, data_confs):
    """
    Specify data information for direct evaluation after training
    """
    confs = []
    for data_conf in data_confs:
        conf = {}
        conf['eval_batch_dir'] = data_conf['val_batch_dir']
        conf['eval_list'] = data_conf['val_list']
        model_dir = task_conf['model_dir']
        data_name = data_conf['name']
        conf['eval_log'] = path.join(model_dir, 'eval_' + data_name + '.log')
        confs.append(conf)
    return confs

def get_test_confs(task_conf, data_confs):
    """
    Specify data information for direct evaluation after training
    """
    confs = []
    for data_conf in data_confs:
        conf = {}
        conf['test_batch_dir'] = data_conf['val_batch_dir']
        conf['test_list'] = data_conf['val_list']
        conf['task_type'] = data_conf['task_type']
        model_dir = task_conf['model_dir']
        data_name = data_conf['name']
        conf['result_file'] = path.join(model_dir, 'res_' + data_name + '.txt')
        conf['id_url_file'] = data_conf['id_url_file']
        conf['id_url_module'] = data_conf['id_url_module']
        conf['id_url_func'] = data_conf['id_url_func']
        conf['html_file'] = path.join(model_dir, data_name + '.html')
        conf['result_json_file'] = path.join(model_dir, 'res_' + data_name + '.json')
        conf['make_json_func'] = data_conf['make_json_func']
        conf['question_gt_file'] = data_conf['question_gt_file']
        conf['answer_gt_file'] = data_conf['answer_gt_file']
        conf['score_file'] = path.join(model_dir, 'score_' + data_name + '.json')
        conf['eval_func'] = data_conf['eval_func']
        confs.append(conf)
    return confs
