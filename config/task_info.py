# -*- coding: utf-8 -*-
"""
Created on Mon Dec  7 16:13:17 2015

@author: yangyi05
"""

def coco_qa_en():
    """
    Task task_infoiguration for English QA
    """
    task_info = {}
    task_info['name'] = 'coco_qa_en'
    task_info['data_infos'] = ['coco_qa_en']
    return task_info

def coco_cap_en():
    """
    Task task_infoiguration for English Caption
    """
    task_info = {}
    task_info['name'] = 'coco_cap_en'
    task_info['data_infos'] = ['coco_cap_en']
    return task_info

def coco_qa_cap_en():
    """
    Task task_infoiguration for English QA and Caption
    """
    task_info = {}
    task_info['name'] = 'coco_qa_cap_en'
    task_info['data_infos'] = ['coco_qa_en', 'coco_cap_en']
    return task_info

def coco_qa_cn():
    """
    Task task_infoiguration for Chinese QA
    """
    task_info = {}
    task_info['name'] = 'coco_qa_cn'
    task_info['data_infos'] = ['coco_qa_cn']
    return task_info

def coco_cap_cn():
    """
    Task task_infoiguration for Chinese Caption
    """
    task_info = {}
    task_info['name'] = 'coco_cap_cn'
    task_info['data_infos'] = ['coco_cap_cn']
    return task_info

def coco_qa_cap_cn():
    """
    Task task_infoiguration for Chinese QA and Caption
    """
    task_info = {}
    task_info['name'] = 'coco_qa_cap_cn'
    task_info['data_infos'] = ['coco_qa_cn', 'coco_cap_cn']
    return task_info

def tieba_dia_cn():
    """
    Task task_infoiguration for Chinese Dialog
    """
    task_info = {}
    task_info['name'] = 'tieba_dia_cn'
    task_info['data_infos'] = ['tieba_dia_cn']
    return task_info

def coco_qa_cap_dia_cn():
    """
    Task task_infoiguration for Chinese QA, Caption and Dialog
    """
    task_info = {}
    task_info['name'] = 'coco_qa_cap_dia_cn'
    task_info['data_infos'] = ['coco_qa_cn', 'coco_cap_cn', 'tieba_dia_cn']
    return task_info

def coco_qa_cap_en_noword():
    """
    Task task_infoiguration for English QA and Caption
    With questions no particular word, but captions have
    """
    task_info = {}
    task_info['name'] = 'coco_qa_cap_en_noword'
    task_info['data_infos'] = ['coco_qa_en_noword', 'coco_cap_en']
    return task_info

def coco_junhua():
    """
    """
    task_info = {}
    task_info['name'] = 'coco_junhua'
    task_info['data_infos'] = ['coco_junhua']
    return task_info

def coco_junhua2():
    """
    """
    task_info = {}
    task_info['name'] = 'coco_junhua2'
    task_info['data_infos'] = ['coco_junhua']
    return task_info

def imagenet_cap_en():
    """
    """
    task_info = {}
    task_info['name'] = 'imagenet_cap_en'
    task_info['data_infos'] = ['imagenet_cap_en']
    return task_info

def imagenet_qa_en():
    """
    """
    task_info = {}
    task_info['name'] = 'imagenet_qa_en'
    task_info['data_infos'] = ['imagenet_qa_en']
    return task_info

def imagenet_qa_cap_en():
    """
    """
    task_info = {}
    task_info['name'] = 'imagenet_qa_cap_en'
    task_info['data_infos'] = ['imagenet_cap_en', 'imagenet_qa_en']
    return task_info

def imagenet_qa_cap_en_noword():
    """
    """
    task_info = {}
    task_info['name'] = 'imagenet_qa_cap_en_noword'
    task_info['data_infos'] = ['imagenet_cap_en', 'imagenet_qa_en_noword']
    return task_info
