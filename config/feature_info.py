# -*- coding: utf-8 -*-
"""
Created on Mon Jan 11 11:28:01 2016

@author: yangyi05
"""

import sys
from os import path
import logging

logging.getLogger().setLevel(logging.INFO)

def add_img_feat_dim(task_info, feature_type):
    """
    Get image feature dimension
    """
    if feature_type == 'vgg_fc7_4096':
        task_info['img_feat_dim'] = 4096
    elif feature_type == 'vgg_fc7_4096_oversample':
        task_info['img_feat_dim'] = 4096
    elif feature_type == 'google_pool5_1024':
        task_info['img_feat_dim'] = 1024
    elif feature_type == 'google_pool5_1024_oversample':
        task_info['img_feat_dim'] = 1024
    elif feature_type == 'google_3072':
        task_info['img_feat_dim'] = 3072
    elif feature_type == 'junhua':
        task_info['img_feat_dim'] = 4096
    elif feature_type == '':
        task_info['img_feat_dim'] = ''
    elif feature_type == 'resnet_pool5_2048_oversample':
        task_info['img_feat_dim'] = 2048
    elif feature_type == 'resnet152_pool5_2048_oversample':
        task_info['img_feat_dim'] = 2048
    else:
        logging.fatal('No such feature type: %s', feature_type)
        sys.exit(1)
    return task_info

def empty_feature_info(data_info, feature_type):
    """
    Empty info
    """
    data_info['feature_name'] = ''
    data_info['image_list'] = ''
    data_info['image_folders'] = []

    data_info['id_url_file'] = ''
    data_info['id_url_module'] = ''
    data_info['id_url_func'] = ''

    data_info['image_feature_dir'] = ''
    data_info['feature_module'] = ''
    data_info['feature_func'] = ''
    data_info['load_feature_module'] = ''
    data_info['load_feature_func'] = ''
    return data_info

def coco_feature_info(data_info, feature_type):
    """
    Generate coco feature info
    """
    data_info = coco_image_feature(data_info, feature_type)
    data_info = coco_image_path(data_info)
    data_info = coco_info(data_info)
    return data_info

def coco_image_feature(data_info, feature_type):
    """
    Image feature configure file for extracting image features
    """
    if feature_type == 'vgg_fc7_4096':
        data_info['image_feature_dir'] = 'vgg_fc7_4096'
        data_info['image_feature_oversample'] = False
        data_info['feature_module'] = 'prepare.extract_vgg'
        data_info['feature_func'] = 'extract_feature'
        data_info['load_feature_module'] = 'prepare.extract_vgg'
        data_info['load_feature_func'] = 'load_feature'
    elif feature_type == 'vgg_fc7_4096_oversample':
        data_info['image_feature_dir'] = 'vgg_fc7_4096_oversample'
        data_info['image_feature_oversample'] = True
        data_info['feature_module'] = 'prepare.extract_vgg'
        data_info['feature_func'] = 'extract_feature'
        data_info['load_feature_module'] = 'prepare.extract_vgg'
        data_info['load_feature_func'] = 'load_feature'
    elif feature_type == 'google_pool5_1024':
        data_info['image_feature_dir'] = 'google_pool5_1024'
        data_info['image_feature_oversample'] = False
        data_info['feature_module'] = 'prepare.extract_google'
        data_info['feature_func'] = 'extract_feature'
        data_info['load_feature_module'] = 'prepare.extract_google'
        data_info['load_feature_func'] = 'load_feature'
    elif feature_type == 'google_pool5_1024_oversample':
        data_info['image_feature_dir'] = 'google_pool5_1024_oversample'
        data_info['image_feature_oversample'] = True
        data_info['feature_module'] = 'prepare.extract_google'
        data_info['feature_func'] = 'extract_feature'
        data_info['load_feature_module'] = 'prepare.extract_google'
        data_info['load_feature_func'] = 'load_feature'
    elif feature_type == 'google_3072':
        data_info['image_feature_dir'] = '/media/yi/DATA/data-baidu/201510-qa-sen-dia/google_3072_own'
        data_info['feature_module'] = ''
        data_info['feature_func'] = ''
        data_info['load_feature_module'] = 'prepare.from_image_to_sentence'
        data_info['load_feature_func'] = 'load_kv_feature_to_memory'
    elif feature_type == 'junhua':
        data_info['image_feature_dir'] = 'junhua'
    elif feature_type == 'resnet_pool5_2048_oversample':
        data_info['image_feature_dir'] = 'resnet_pool5_2048_oversample'
        data_info['image_feature_oversample'] = True
        data_info['feature_module'] = 'prepare.extract_resnet'
        data_info['feature_func'] = 'extract_feature'
        data_info['load_feature_module'] = 'prepare.extract_resnet'
        data_info['load_feature_func'] = 'load_feature'
    elif feature_type == 'resnet152_pool5_2048_oversample':
        data_info['image_feature_dir'] = 'resnet152_pool5_2048_oversample'
        data_info['image_feature_oversample'] = True
        data_info['feature_module'] = 'prepare.extract_resnet152'
        data_info['feature_func'] = 'extract_feature'
        data_info['load_feature_module'] = 'prepare.extract_resnet152'
        data_info['load_feature_func'] = 'load_feature'
    else:
        logging.fatal('No such feature type: %s', feature_type)
        sys.exit(1)
    return data_info

def coco_image_path(data_info):
    """
    coco image paths, for extracting image features
    """
    image_root_path = '/media/yi/DATA/data-orig/microsoft_coco/coco/images'
    train_image_path = path.join(image_root_path, 'train')
    val_image_path = path.join(image_root_path, 'val')
    data_info['feature_name'] = 'coco'
    data_info['image_list'] = 'coco.list'
    data_info['image_folders'] = [train_image_path, val_image_path]
    return data_info

def coco_info(data_info):
    """
    Information for extracting image id and image url pairs
    for html demo
    """
    data_info['id_url_file'] = 'coco_info_val.txt'
    data_info['id_url_module'] = 'prepare.coco_id_url'
    data_info['id_url_func'] = 'print_id_url'
    return data_info

def imagenet_feature_info(data_info, feature_type):
    """
    Generate imagenet feature info
    """
    data_info = coco_image_feature(data_info, feature_type)
    data_info = imagenet_image_path(data_info)
    data_info = imagenet_info(data_info)
    return data_info

def imagenet_image_path(data_info):
    """
    imagenet image paths, for extracting image features
    """
    image_root_path = '/media/yi/DATA/data-orig/ILSVRC2012_img_val'
    train_image_path = image_root_path
    val_image_path = image_root_path
    data_info['feature_name'] = 'imagenet'
    data_info['image_list'] = 'imagenet.list'
    data_info['image_folders'] = [train_image_path, val_image_path]
    return data_info

def imagenet_info(data_info):
    """
    Information for extracting image id and image url pairs
    for html demo
    """
    data_info['id_url_file'] = 'imagenet_info_val.txt'
    data_info['id_url_module'] = 'prepare.imagenet_id_url'
    data_info['id_url_func'] = 'print_id_url'
    return data_info

