# -*- coding: utf-8 -*-
"""
Created on Mon Dec  7 16:02:32 2015

@author: yangyi05
"""

from os import path
from feature_info import coco_feature_info, empty_feature_info, imagenet_feature_info

def coco_qa_en(feature_type):
    """
    Data configuration for English Question Answering
    """
    data_info = {}
    data_info['name'] = 'coco_qa_en'
    data_info['task_type'] = 'image qa'
    data_info['prepare_module'] = 'prepare.coco_qa_en'
    data_info['batch_size'] = 4096
    data_info['dict_freq_thresh'] = 10
    data_info = coco_feature_info(data_info, feature_type)
    data_info['question_gt_file'] = 'coco_val_que.json'
    data_info['answer_gt_file'] = 'coco_val_ans.json'
    data_info['make_json_func'] = 'make_json_qa'
    data_info['eval_func'] = 'evaluate_qa'
    return data_info

def coco_cap_en(feature_type):
    """
    Data configuration for English Caption
    """
    data_info = {}
    data_info['name'] = 'coco_cap_en'
    data_info['task_type'] = 'image caption'
    data_info['prepare_module'] = 'prepare.coco_cap_en'
    data_info['batch_size'] = 4096
    data_info['dict_freq_thresh'] = 10
    data_info = coco_feature_info(data_info, feature_type)
    data_info['question_gt_file'] = ''
    data_info['answer_gt_file'] = 'coco_val_cap.json'
    data_info['make_json_func'] = 'make_json_cap'
    data_info['eval_func'] = 'evaluate_cap'
    return data_info

def coco_qa_cn(feature_type):
    """
    Data configuration for Chinese Question Anwsering
    """
    data_info = {}
    data_info['name'] = 'coco_qa_cn'
    data_info['task_type'] = 'image qa'
    data_info['prepare_module'] = 'prepare.coco_qa_cn'
    data_info['batch_size'] = 4096
    data_info['dict_freq_thresh'] = 10
    data_info = coco_feature_info(data_info, feature_type)
    data_info['question_gt_file'] = 'coco_val_que_baidu.json'
    data_info['answer_gt_file'] = 'coco_val_ans_baidu.json'
    data_info['make_json_func'] = 'make_json_qa'
    data_info['eval_func'] = 'evaluate_qa'
    return data_info

def coco_cap_cn(feature_type):
    """
    Data configuration for Chinese Caption
    """
    data_info = {}
    data_info['name'] = 'coco_cap_cn'
    data_info['task_type'] = 'image caption'
    data_info['prepare_module'] = 'prepare.coco_cap_cn'
    data_info['batch_size'] = 4096
    data_info['dict_freq_thresh'] = 10
    data_info = coco_feature_info(data_info, feature_type)
    data_info['question_gt_file'] = ''
    data_info['answer_gt_file'] = 'coco_val_cap_baidu.json'
    data_info['make_json_func'] = 'make_json_cap'
    data_info['eval_func'] = 'evaluate_cap'
    return data_info

def tieba_dia_cn(feature_type):
    """
    Data configuration for Chinese Dialog
    """
    data_info = {}
    data_info['name'] = 'tieba_dia_cn'
    data_info['task_type'] = 'qa'
    data_info['prepare_module'] = 'prepare.tieba_dia_cn'
    data_info['batch_size'] = 4096 * 4
    data_info['dict_freq_thresh'] = 100
    data_info = empty_feature_info(data_info)
    data_info['question_gt_file'] = 'dia_que_baidu.json'
    data_info['answer_gt_file'] = 'dia_ans_baidu.json'
    data_info['make_json_func'] = 'make_json_qa'
    data_info['eval_func'] = 'evaluate_qa'
    return data_info

def coco_qa_en_noword(feature_type):
    """
    Data configuration for English Question Answering
    With some particular words
    """
    data_info = {}
    data_info['name'] = 'coco_qa_en_noword'
    data_info['task_type'] = 'image qa'
    data_info['prepare_module'] = 'prepare.coco_qa_en_noword'
    data_info['batch_size'] = 4096
    data_info['dict_freq_thresh'] = 10
    data_info = coco_feature_info(data_info, feature_type)
    data_info['question_gt_file'] = 'coco_val_que.json'
    data_info['answer_gt_file'] = 'coco_val_ans.json'
    data_info['make_json_func'] = 'make_json_qa'
    data_info['eval_func'] = 'evaluate_qa'
    return data_info

def coco_junhua(feature_type):
    """
    Data configuration for English Caption
    """
    data_info = {}
    data_info['name'] = 'coco_junhua'
    data_info['task_type'] = 'image caption'
    data_info['prepare_module'] = ''
    data_info['batch_size'] = []
    data_info['dict_freq_thresh'] = []
    data_info = empty_feature_info(data_info)
    data_info['question_gt_file'] = ''
    data_info['answer_gt_file'] = ''
    data_info['make_json_func'] = ''
    data_info['eval_func'] = ''
    return data_info

def imagenet_qa_en(feature_type):
    """
    Data configuration for English Question Answering
    """
    data_info = {}
    data_info['name'] = 'imagenet_qa_en'
    data_info['task_type'] = 'image qa'
    data_info['prepare_module'] = 'prepare.imagenet_qa_en'
    data_info['batch_size'] = 4096
    data_info['dict_freq_thresh'] = 10
    data_info = imagenet_feature_info(data_info, feature_type)
    data_info['question_gt_file'] = 'imagenet_val_que.json'
    data_info['answer_gt_file'] = 'imagenet_val_ans.json'
    data_info['make_json_func'] = 'make_json_qa'
    data_info['eval_func'] = 'evaluate_qa'
    return data_info

def imagenet_cap_en(feature_type):
    """
    Data configuration for English Caption
    """
    data_info = {}
    data_info['name'] = 'imagenet_cap_en'
    data_info['task_type'] = 'image caption'
    data_info['prepare_module'] = 'prepare.imagenet_cap_en'
    data_info['batch_size'] = 4096
    data_info['dict_freq_thresh'] = 10
    data_info = imagenet_feature_info(data_info, feature_type)
    data_info['question_gt_file'] = ''
    data_info['answer_gt_file'] = 'imagenet_val_cap.json'
    data_info['make_json_func'] = 'make_json_cap'
    data_info['eval_func'] = 'evaluate_cap'
    return data_info

def imagenet_qa_en_noword(feature_type):
    """
    Data configuration for English Question Answering
    With some particular words
    """
    data_info = {}
    data_info['name'] = 'imagenet_qa_en_noword'
    data_info['task_type'] = 'image qa'
    data_info['prepare_module'] = 'prepare.imagenet_qa_en_noword'
    data_info['batch_size'] = 4096
    data_info['dict_freq_thresh'] = 10
    data_info = imagenet_feature_info(data_info, feature_type)
    data_info['question_gt_file'] = 'imagenet_val_que.json'
    data_info['answer_gt_file'] = 'imagenet_val_ans.json'
    data_info['make_json_func'] = 'make_json_qa'
    data_info['eval_func'] = 'evaluate_qa'
    return data_info
