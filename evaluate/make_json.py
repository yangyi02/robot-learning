'''
Created on Dec 01, 2015

create a web page for easy evaluation of generated text

@author: yangyi05
'''

import sys
import io
import json

def read_text(text_file):
    """
    Load result text file, the text file consists of 3 columns:
    id, question, answer
    """
    data = io.open(text_file, 'rb').readlines()
    question_ids, questions, answers = [], [], []
    for line in data:
        items = line.strip().split('\t')
        question_ids.append(items[0].strip())
        questions.append(items[1].strip())
        # Remove end sign in the prediction answers
        answers.append(items[2].replace('$$E$$', '').strip())
    return question_ids, questions, answers

def make_json_cap(text_file, json_file):
    """
    Make caption results into a json results for benchmark evaluation
    """
    image_ids, questions, answers = read_text(text_file)
    question_dict = dict(zip(image_ids, questions))
    answer_dict = dict(zip(image_ids, answers))
    image_ids = list(set(image_ids)) # There are duplicate image ids
    res = []
    for image_id in image_ids:
        entry = {}
        entry['image_id'] = int(image_id)
        entry['caption'] = answer_dict[image_id]
        res.append(entry)
    json.dump(res, io.open(json_file, 'wb'))

def make_json_qa(text_file, json_file):
    """
    Make QA results into a json results for benchmark evaluation
    """
    question_ids, questions, answers = read_text(text_file)
    question_dict = dict(zip(question_ids, questions))
    answer_dict = dict(zip(question_ids, answers))
    res = []
    for question_id in question_ids:
        entry = {}
        entry['question_id'] = int(question_id)
        entry['answer'] = answer_dict[question_id]
        res.append(entry)
    json.dump(res, io.open(json_file, 'wb'))

if __name__ == '__main__':
    if len(sys.argv) < 4:
        sys.stderr.write('Usage: *.py task text_file json_file\n')
        sys.exit(1)
    task, text_file, json_file = sys.argv[1], sys.argv[2], sys.argv[3]
    if task == 'cap':
        make_json_cap(text_file, json_file)
    if task == 'qa' or task == 'dia':
        make_json_qa(text_file, json_file)

