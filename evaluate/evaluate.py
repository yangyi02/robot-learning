'''
Created on Dec 02, 2015

create a web page for easy evaluation of generated text

@author: yangyi05
'''

import sys
import io
import json
from tokenizer.ptbtokenizer import PTBTokenizer
from bleu.bleu import Bleu
from cider.cider import Cider
from meteor.meteor import Meteor
from rouge.rouge import Rouge
from vqaTools.vqa import VQA
from vqaEvaluation.vqaEval import VQAEval

def evaluate_cap(resFile, quesFile, annFile, score_file):
    """
    Evaluate benchmark scores for image captioning
    """
    # create coco object and cocoRes object
    dataset = json.load(io.open(annFile, 'rb'))
    imgToAnns_gts = {ann['image_id']: [] for ann in dataset['annotations']}
    for ann in dataset['annotations']:
        imgToAnns_gts[ann['image_id']] += [ann]

    result = json.load(io.open(resFile, 'rb'))
    imgToAnns_res = {ann['image_id']: [] for ann in result}
    for ann in result:
        imgToAnns_res[ann['image_id']] += [ann]

    imgIds = [ann['image_id'] for ann in result]

    gts = {}
    res = {}
    for imgId in imgIds:
        gts[imgId] = imgToAnns_gts[imgId]
        res[imgId] = imgToAnns_res[imgId]

    # =================================================
    # Set up scorers
    # =================================================
    print 'tokenization...'
    tokenizer = PTBTokenizer()
    gts = tokenizer.tokenize(gts)
    res = tokenizer.tokenize(res)

    # =================================================
    # Set up scorers
    # =================================================
    print 'setting up scorers...'
    scorers = [
        (Bleu(4), ["Bleu_1", "Bleu_2", "Bleu_3", "Bleu_4"]),
        (Meteor(), "METEOR"),
        (Rouge(), "ROUGE_L"),
        (Cider(), "CIDEr")
    ]

    # =================================================
    # Compute scores
    # =================================================
    eval_score = {}
    for scorer, method in scorers:
        print 'computing %s score...'%(scorer.method())
        score, scores = scorer.compute_score(gts, res)
        if isinstance(method, list):
            for sc, scs, m in zip(score, scores, method):
                eval_score[m] = sc
                print "%s: %0.3f"%(m, sc)
        else:
            eval_score[method] = score
            print "%s: %0.3f"%(method, score)

    # print output evaluation scores
    for metric, score in eval_score.items():
        print '%s: %.3f'%(metric, score)
    json.dump(eval_score, io.open(score_file, 'wb'))

def evaluate_qa(resFile, quesFile, annFile, score_file):
    """
    Evaluate benchmarks for image question answering
    """
    # create vqa object and vqaRes object
    vqa = VQA(annFile, quesFile)
    vqaRes = vqa.loadRes(resFile, quesFile)
    # create vqaEval object by taking vqa and vqaRes
    # n is precision of accuracy (number of places after decimal), default is 2
    vqaEval = VQAEval(vqa, vqaRes, n=2)
    # evaluate results
    """
    If you have a list of question ids on which you would like to evaluate
    your results, pass it as a list to below function
    By default it uses all the question ids in annotation file
    """
    anns = json.load(io.open(resFile, 'rb'))
    assert type(anns) == list, 'results is not an array of objects'
    question_ids = [ann['question_id'] for ann in anns]
    vqaEval.evaluate(set(question_ids))
    # print accuracies
    print "\n"
    print "Overall Accuracy is: %.02f\n" %(vqaEval.accuracy['overall'])
    print "Per Question Type Accuracy is the following:"
    for quesType in vqaEval.accuracy['perQuestionType']:
        print "%s : %.02f" %(quesType, vqaEval.accuracy['perQuestionType'][quesType])
    print "\n"
    print "Per Answer Type Accuracy is the following:"
    for ansType in vqaEval.accuracy['perAnswerType']:
        print "%s : %.02f" %(ansType, vqaEval.accuracy['perAnswerType'][ansType])
    print "\n"
    json.dump(vqaEval.accuracy, io.open(score_file, 'wb'))
    # save evaluation results to ./Results folder
    # json.dump(vqaEval.accuracy,     open(accuracyFile,     'w'))
    # json.dump(vqaEval.evalQA,       open(evalQAFile,       'w'))
    # json.dump(vqaEval.evalQuesType, open(evalQuesTypeFile, 'w'))
    # json.dump(vqaEval.evalAnsType,  open(evalAnsTypeFile,  'w'))

if __name__ == '__main__':
    if len(sys.argv) < 4:
        sys.stderr.write('Usage: *.py task json_file gt_file score_file\n')
        sys.exit(1)
    task, json_file, gt_file, score_file = sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4]
    if task == 'cap':
        evaluate_cap(json_file, gt_file, score_file)
    if task == 'qa' or task == 'dia':
        evaluate_qa(json_file, gt_file, score_file)
