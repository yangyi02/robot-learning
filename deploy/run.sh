PADDLE_PATH=/home/yi/code/paddle/idl/paddle
export PATH=$PADDLE_PATH/paddle:$PATH
# Train path
export PYTHONPATH=$PADDLE_PATH/paddle/output/pylib:$PYTHONPATH
export PYTHONPATH=$PADDLE_PATH/paddle/trainer:$PYTHONPATH

#paddle_trainer --config=lstm_gen.py --use_gpu=0 --trainer_count=4 --test_period=0 --num_passes=44 --save_dir=model --job=test --test_pass=43 --config_args gen_list=gen.list,result_file=result.txt,dict_file=dict.txt,dict_pkl=dict.pkl,img_feat_dim=4096
paddle_trainer --config=lstm_gen.py --use_gpu=0 --trainer_count=4 --test_period=0 --num_passes=44 --save_dir=model --job=test --test_pass=43 --config_args=gen_list=gen.list,result_file=result.txt,dict_file=dict.txt,dict_pkl=dict.pkl,img_feat_dim=4096
