# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 15:55:28 2015

@author: yangyi05
"""

from os import path
import io
import re
import logging
import json
import random
from prepare.clean_sentence import process_english_sentence

logging.basicConfig(format='[%(levelname)s %(asctime)s %(filename)s:%(lineno)s] %(message)s')
logging.getLogger().setLevel(logging.INFO)

def prepare_text(train_data_file, val_data_file, debug=False):
    """
    This function processes microsoft coco VQA dataset
    """
    root_path_q = '/media/yi/DATA/data-orig/microsoft_coco/VQA/Questions'
    root_path_a = '/media/yi/DATA/data-orig/microsoft_coco/VQA/Annotations'
    json_file_train_q = path.join(root_path_q, 'OpenEnded_mscoco_train2014_questions.json')
    json_file_train_a = path.join(root_path_a, 'mscoco_train2014_annotations.json')
    json_file_val_q = path.join(root_path_q, 'OpenEnded_mscoco_val2014_questions.json')
    json_file_val_a = path.join(root_path_a, 'mscoco_val2014_annotations.json')
    data_train_q = json.load(io.open(json_file_train_q))
    data_train_a = json.load(io.open(json_file_train_a))
    data_val_q = json.load(io.open(json_file_val_q))
    data_val_a = json.load(io.open(json_file_val_a))
    # Move sentences with key_words out of train data and put them into val data
    # key_words = ['dog', 'cat', 'dogs', 'cats']
    key_words = ['cat', 'cats']
    data_train, data_val = \
            reorganize_data(data_train_q, data_train_a, data_val_q, data_val_a)
    data_train, data_val = remove_keywords(data_train, data_val, key_words)
    print_to_txt(data_train, train_data_file, debug)
    print_to_txt(data_val, val_data_file, debug)

def reorganize_data(data_train_q, data_train_a, data_val_q, data_val_a):
    """
    Since there are many validation data, put them into train data
    """
    logging.info('%d, %d, %d, %d', len(data_train_q['questions']), len(data_train_a['annotations']), \
            len(data_val_q['questions']), len(data_val_a['annotations']))
    # question_answer_pair = zip(data_val_q['questions'], data_val_a['annotations'])
    data_train = {}
    data_train['annotations'] = data_train_q['questions']
    for i in xrange(len(data_train_q['questions'])):
        data_train['annotations'][i]['answer'] = data_train_a['annotations'][i]['multiple_choice_answer']
    data_val = {}
    data_val['annotations'] = data_val_q['questions']
    for i in xrange(len(data_val_q['questions'])):
        data_val['annotations'][i]['answer'] = data_val_a['annotations'][i]['multiple_choice_answer']
    new_data = []
    image_id_dict = {}
    cnt = 0
    for item in data_val['annotations']:
        image_id = item['image_id']
        if image_id not in image_id_dict:
            data = {}
            data['image_id'] = item['image_id']
            data['question_id'] = [item['question_id']]
            data['question'] = [item['question']]
            data['answer'] = [item['answer']]
            new_data.append(data)
            image_id_dict[image_id] = cnt
            cnt += 1
        else:
            index = image_id_dict[image_id]
            new_data[index]['question_id'].append(item['question_id'])
            new_data[index]['question'].append(item['question'])
            new_data[index]['answer'].append(item['answer'])
    random.shuffle(new_data)
    new_data_train = expand_data(new_data[1024:])
    new_data_val = expand_data(new_data[0:1024])
    data_train['annotations'].extend(new_data_train)
    random.shuffle(data_train['annotations'])
    data_val['annotations'] = new_data_val
    logging.info('%d, %d', len(data_train['annotations']), len(data_val['annotations']))
    return data_train, data_val

def remove_keywords(data_train, data_val, key_words):
    """
    Since there are many validation data, put them into train data
    """
    logging.info('%d, %d', len(data_train['annotations']), len(data_val['annotations']))
    data_train_new, data_val_new = {}, {}
    data_train_new['annotations'], data_val_new['annotations'] = [], []
    for sample in data_train['annotations']:
        #if any(word in sample['answer'] for word in key_words):
        if any(findWholeWord(word)(sample['answer']) is not None for word in key_words):
            logging.info('%s\t%s', sample['question'], sample['answer'])
            data_val_new['annotations'].append(sample)
        else:
            data_train_new['annotations'].append(sample)
    for sample in data_val['annotations']:
        #if any(word in sample['answer'] for word in key_words):
        if any(findWholeWord(word)(sample['answer']) is not None for word in key_words):
            logging.info('%s\t%s', sample['question'], sample['answer'])
            data_val_new['annotations'].append(sample)
        else:
            data_train_new['annotations'].append(sample)
    logging.info('%d, %d', len(data_train_new['annotations']), len(data_val_new['annotations']))
    return data_train_new, data_val_new

def findWholeWord(w):
    return re.compile(r'\b({0})\b'.format(w), flags=re.IGNORECASE).search

def expand_data(new_data):
    """
    new_data is in the format organized by image_id
    expand new_data back to format organized by every sentence
    """
    data_expand = []
    for item in new_data:
        for i in xrange(len(item['question'])):
            data = {}
            data['image_id'] = item['image_id']
            data['question_id'] = item['question_id'][i]
            data['question'] = item['question'][i]
            data['answer'] = item['answer'][i]
            data_expand.append(data)
    return data_expand

def print_to_txt(data, output_file, debug):
    """
    Due to the different dataset format, all the hacky code exist here
    After data processing, data will all follow unified formats
    """
    image_ids, question_ids, questions, answers = [], [], [], []
    if debug: # Batch size is 1024, and we use 3 batches for debugging
        data['annotations'] = data['annotations'][:4096*3]
    for sample in data['annotations']:
        image_ids.append(sample['image_id'])
        question_ids.append(sample['question_id'])
        questions.append(process_english_sentence(sample['question']))
        answers.append(process_english_sentence(sample['answer']))
    handle = io.open(output_file, 'wb')
    handle.write('image qa\n')
    for i in xrange(len(image_ids)):
        handle.write('%d\t%d\t%s\t%s\n' % (image_ids[i], question_ids[i],
                                           questions[i], answers[i]))

def prepare_json(text_file, gt_file_q, gt_file_a):
    """
    Create json ground truth file for evaluation
    """
    questions, annotations = [], []
    data = io.open(text_file).readlines()
    data = data[1:]
    for line in data:
        items = line.strip().split('\t')
        ques, anno = {}, {}
        ques['image_id'], anno['image_id'] = int(items[0]), int(items[0])
        ques['question_id'], anno['question_id'] = int(items[1]), int(items[1])
        ques['question'], anno['question'] = items[2], items[2]
        #anno['multiple_choice_answer'] = items[3]
        ans = {'answer':items[3]}
        anno['answers'] = [ans]
        anno['question_type'] = 'not available'
        anno['answer_type'] = 'not available'
        questions.append(ques)
        annotations.append(anno)
    #task_type = 'Multiple Choice'
    task_type = ''
    handle = io.open(gt_file_q, 'wb')
    json.dump({'info': '', 'license': '', 'task_type': task_type, 'data_type': '',
               'data_subtype': '', 'questions': questions}, handle)
    handle = io.open(gt_file_a, 'wb')
    json.dump({'annotations': annotations}, handle)

if __name__ == '__main__':
    prepare_text('train_data.txt', 'val_data.txt', debug=True)
