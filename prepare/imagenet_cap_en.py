# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 16:11:08 2015

@author: yangyi05
"""

from os import path
import io
import logging
import json
import random
from prepare.clean_sentence import process_english_sentence

logging.basicConfig(format='[%(levelname)s %(asctime)s %(filename)s:%(lineno)s] %(message)s')
logging.getLogger().setLevel(logging.INFO)

def prepare_text(train_data_file, val_data_file, debug=False):
    """
    This function processes microsoft coco caption dataset
    """
    groundtruth_txt = '/media/yi/DATA/data-orig/ILSVRC2012_devkit_t12/data/groundtruth_name.txt'
    lines = open(groundtruth_txt, 'rb').readlines()

    data = {}
    data['annotations'] = []
    cnt = 1
    for line in io.open(groundtruth_txt, 'rb').readlines():
        line = line.strip()
        one_data = {}
        one_data['image_id'] = cnt
        one_data['id'] = cnt
        flag = random.randint(0, 9)
        if flag == 0:
            one_data['caption'] = 'a %s in the image' % line
        elif flag == 1:
            one_data['caption'] = 'a %s on something' % line
        elif flag == 2:
            one_data['caption'] = 'a %s doing something' % line
        elif flag == 3:
            one_data['caption'] = 'a %s is looking at you' % line
        elif flag == 4:
            one_data['caption'] = 'a beautiful %s looking at you' % line
        elif flag == 5:
            one_data['caption'] = 'a nice %s doing something in the image' % line
        elif flag == 6:
            one_data['caption'] = 'a pretty %s on something looking at you' % line
        elif flag == 7:
            one_data['caption'] = 'a beautiful %s on something in the image' % line
        elif flag == 8:
            one_data['caption'] = 'a pretty %s doing something' % line
        else:
            one_data['caption'] = 'a nice %s on something looing at you' % line
        cnt += 1
        data['annotations'].append(one_data)

    random.shuffle(data['annotations'])
    data_train = {}
    data_train['annotations'] = data['annotations'][:25000]
    data_val = {}
    data_val['annotations'] = data['annotations'][25000:]

    data_train, data_val = reorganize_data(data_train, data_val)
    print_to_txt(data_train, train_data_file, debug)
    print_to_txt(data_val, val_data_file, debug)

def reorganize_data(data_train, data_val):
    """
    Since there are many validation data, put them into train data
    """
    logging.info('%d, %d', len(data_train['annotations']), len(data_val['annotations']))
    new_data = []
    image_id_dict = {}
    cnt = 0
    for item in data_val['annotations']:
        image_id = item['image_id']
        if image_id not in image_id_dict:
            data = {}
            data['image_id'] = item['image_id']
            data['id'] = [item['id']]
            data['caption'] = [item['caption']]
            new_data.append(data)
            image_id_dict[image_id] = cnt
            cnt += 1
        else:
            index = image_id_dict[image_id]
            new_data[index]['id'].append(item['id'])
            new_data[index]['caption'].append(item['caption'])
    random.shuffle(new_data)
    new_data_train = expand_data(new_data[1024:])
    new_data_val = expand_data(new_data[0:1024])
    data_train['annotations'].extend(new_data_train)
    random.shuffle(data_train['annotations'])
    data_val['annotations'] = new_data_val
    logging.info('%d, %d', len(data_train['annotations']), len(data_val['annotations']))
    return data_train, data_val

def expand_data(new_data):
    """
    new_data is in the format organized by image_id
    expand new_data back to format organized by every sentence
    """
    data_expand = []
    for item in new_data:
        for i in xrange(len(item['caption'])):
            data = {}
            data['image_id'] = item['image_id']
            data['id'] = item['id'][i]
            data['caption'] = item['caption'][i]
            data_expand.append(data)
    return data_expand

def print_to_txt(data, output_file, debug):
    """
    Due to the different dataset format, all the hacky code exist here
    After data processing, data will all follow unified formats
    """
    image_ids, captions = [], []
    if debug: # Batch size is 4096, and we use 3 batches for debugging
        data['annotations'] = data['annotations'][:4096*3]
    for sample in data['annotations']:
        image_ids.append(sample['image_id'])
        captions.append(process_english_sentence(sample['caption']))
    handle = io.open(output_file, 'wb')
    handle.write('image caption\n')
    for i in xrange(len(image_ids)):
        handle.write('%d\t%s\n' % (image_ids[i], captions[i]))

def prepare_json(text_file, output_file):
    """
    Create json ground truth file for evaluation
    """
    annotations = []
    data = io.open(text_file).readlines()
    data = data[1:]
    for line in data:
        items = line.strip().split('\t')
        anno = {}
        anno['image_id'] = int(items[0])
        anno['caption'] = items[1]
        annotations.append(anno)
    task_type = 'captions'
    images = []
    handle = io.open(output_file, 'wb')
    json.dump({'info': '', 'licenses': '', 'type': task_type,
               'images': images, 'annotations': annotations}, handle)

if __name__ == '__main__':
    prepare_text('train_data.txt', 'val_data.txt', debug=True)
