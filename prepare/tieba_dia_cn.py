# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 16:34:33 2015

@author: yangyi05
"""

from os import path
import io
import json
import random
from prepare.clean_sentence import process_chinese_sentence

def prepare_text(train_data_file, val_data_file, debug=False):
    """
    This function processes Baidu Tieba dialog dataset
    """
    root_path = '/media/yi/DATA/data-baidu/201510-qa-sen-dia/dia'
    text_file = path.join(root_path, 'tieba.30m.filter.sampled.twice.word_tokenized')
    data = io.open(text_file, errors='ignore').readlines()
    random.shuffle(data)
    data_train = data[0:1000000]
    data_val = data[1000000:1000000+4096*4*3]
    print_to_txt(data_train, train_data_file, debug)
    print_to_txt(data_val, val_data_file, debug)

def print_to_txt(data, output_file, debug):
    """
    Due to the different dataset format, all the hacky code exist here
    After data processing, data will all handlellow unified formats
    """
    questions, answers = [], []
    if debug: # Batch size is 4096, and we use 3 batches for debugging
        data = data[:4096*4*3]
    for line in data:
        items = line.strip().split('\t')
        questions.append(process_chinese_sentence(items[0].encode('utf-8')))
        answers.append(process_chinese_sentence(items[1].encode('utf-8')))
    handle = io.open(output_file, 'wb')
    for i in xrange(len(questions)):
        handle.write('%s\t%d\t%s\t%s\n' % ('', i, questions[i], answers[i]))

def prepare_json(text_file, gt_file_q, gt_file_a):
    """
    Create json ground truth file for evaluation
    """
    questions, annotations = [], []
    data = io.open(text_file).readlines()
    for line in data:
        items = line.split('\t')
        ques, anno = {}, {}
        ques['image_id'], anno['image_id'] = [], []
        ques['question_id'] = int(items[1])
        anno['question_id'] = int(items[1])
        ques['question'] = items[2].strip()
        anno['question'] = items[2].strip()
        anno['multiple_choice_answer'] = items[3].strip()
        questions.append(ques)
        annotations.append(anno)
    task_type = 'Multiple Choice'
    handle = io.open(gt_file_q, 'wb')
    json.dump({'info': '', 'license': '', 'task_type': task_type, 'data_type': '',
               'data_subtype': '', 'questions': questions}, handle)
    handle = io.open(gt_file_a, 'wb')
    json.dump({'annotations': annotations}, handle)
