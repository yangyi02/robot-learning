# -*- coding: utf-8 -*-
"""
Created on Sat Dec  5 04:54:14 2015

@author: yangyi05
"""

import os
import io
import json

def print_id_url_web(json_file, output_file):
    """
    Save image id and image web url pairs
    """
    handle = io.open(output_file, 'wb')
    data = json.load(io.open(json_file, 'rb'))
    for sample in data['images']:
        image_id = sample['id']
        web_url = sample['flickr_url']
        handle.write("%d\t%s\n" % (image_id, web_url))

def print_id_url_local(output_file, local_img_path):
    """
    Save image id and image local url pairs
    """
    handle = io.open(output_file, 'wb')
    for i in xrange(50000):
        image_id = i + 1
        file_name = 'ILSVRC2012_val_%.8d.JPEG' % image_id
        local_url = os.path.join(local_img_path, file_name)
        handle.write("%d\t%s\n" % (image_id, local_url))

def print_id_url(id_url_file):
    """
    Save image id and url pairs to file
    """
    root_path = '/media/yi/DATA/data-orig/ILSVRC2012_img_val'
    #local_img_path = os.path.join(root_path, 'images/train2014')
    #json_file = os.path.join(root_path, 'annotations/captions_train2014.json')
    #print_id_url_web(json_file, id_url_file)
    #local_img_path = os.path.join(root_path, 'images/val2014')
    #json_file = os.path.join(root_path, 'annotations/captions_val2014.json')
    #print_id_url_web(json_file, id_url_file)
    print_id_url_local(id_url_file, root_path)

if __name__ == '__main__':
    print_id_url('imagenet_info_val2012.txt')

