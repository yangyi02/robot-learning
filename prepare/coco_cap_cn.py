# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 16:25:44 2015

@author: yangyi05
"""

from os import path
import io
import logging
import json
import random
from prepare.clean_sentence import process_chinese_sentence

logging.basicConfig(format='[%(levelname)s %(asctime)s %(filename)s:%(lineno)s] %(message)s')
LOGGER = logging.getLogger('Robot Learning')
LOGGER.setLevel(logging.DEBUG)

def prepare_text(train_data_file, val_data_file, debug=False):
    """
    This function processes microsoft coco Baidu Chinese caption dataset
    """
    root_path = '/media/yi/DATA/data-baidu/201510-qa-sen-dia/sen'
    text_file_train = path.join(root_path, 'sen.file.train.seg')
    text_file_val = path.join(root_path, 'sen.file.val.seg')
    data_train = io.open(text_file_train).readlines()
    data_val = io.open(text_file_val).readlines()
    data_train, data_val = reorganize_data(data_train, data_val)
    print_to_txt(data_train, train_data_file, debug)
    print_to_txt(data_val, val_data_file, debug)

def reorganize_data(data_train, data_val):
    """
    Since there are many validation data, put them into train data
    """
    LOGGER.info('%d, %d', len(data_train), len(data_val))
    random.shuffle(data_val)
    if len(data_val) > 4096*3:
        data_train.extend(data_val[4096*3:len(data_val)])
        data_val = data_val[0:4096*3]
    LOGGER.info('%d, %d', len(data_train), len(data_val))
    return data_train, data_val

def print_to_txt(data, output_file, debug):
    """
    Due to the different dataset format, all the hacky code exist here
    After data processing, data will all follow unified formats
    """
    image_ids, captions = [], []
    if debug: # Batch size is 1024, and we use 3 batches for debugging
        data = data[:4096*3]
    for line in data:
        items = line.strip().split('\t')
        image_ids.append(int(items[0]))
        captions.append(process_chinese_sentence(items[1].encode('utf-8')))
    handle = io.open(output_file, 'wb')
    for i in xrange(len(image_ids)):
        handle.write('%d\t%s\t%s\t%s\n' % (image_ids[i], '', '', captions[i]))

def prepare_json(text_file, output_file):
    """
    Create json ground truth file for evaluation
    """
    annotations = []
    data = io.open(text_file).readlines()
    for line in data:
        items = line.strip().split('\t')
        anno = {}
        anno['image_id'] = int(items[0])
        anno['caption'] = items[3]
        annotations.append(anno)
    task_type = 'captions'
    images = []
    handle = io.open(output_file, 'wb')
    json.dump({'info': '', 'licenses': '', 'type': task_type,
               'images': images, 'annotations': annotations}, handle)

