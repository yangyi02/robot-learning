# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 16:31:13 2015

@author: yangyi05
"""

from os import path
import io
import logging
import json
import random
from prepare.clean_sentence import process_chinese_sentence

logging.basicConfig(format='[%(levelname)s %(asctime)s %(filename)s:%(lineno)s] %(message)s')
LOGGER = logging.getLogger('Robot Learning')
LOGGER.setLevel(logging.DEBUG)

def prepare_text(train_data_file, val_data_file, debug=False):
    """
    This function processes microsoft coco Baidu Chinese VQA dataset
    """
    root_path = '/media/yi/DATA/data-baidu/201510-qa-sen-dia/qa'
    text_file_train = path.join(root_path, 'qa.file.train.seg')
    text_file_val = path.join(root_path, 'qa.file.val.seg')
    data_train = io.open(text_file_train).readlines()
    data_val = io.open(text_file_val).readlines()
    data_train, data_val = reorganize_data(data_train, data_val)
    print_to_txt(data_train, train_data_file, debug)
    print_to_txt(data_val, val_data_file, debug)

def reorganize_data(data_train, data_val):
    """
    Since there are many validation data, put them into train data
    """
    LOGGER.info('%d, %d', len(data_train), len(data_val))
    random.shuffle(data_val)
    if len(data_val) > 4096*3:
        data_train.extend(data_val[4096*3:len(data_val)])
        data_val = data_val[0:4096*3]
    LOGGER.info('%d, %d', len(data_train), len(data_val))
    return data_train, data_val

def print_to_txt(data, output_file, debug):
    """
    Due to the different dataset format, all the hacky code exist here
    After data processing, data will all handlellow unified formats
    """
    image_ids, question_ids, questions, answers = [], [], [], []
    if debug: # Batch size is 1024, and we use 3 batches for debugging
        data = data[:4096*3]
    id_dict = {}
    for line in data:
        items = line.strip().split('\t')
        image_id = int(items[0])
        image_ids.append(image_id)
        questions.append(process_chinese_sentence(items[1].encode('utf-8')))
        answers.append(process_chinese_sentence(items[2].encode('utf-8')))
        if image_id in id_dict:
            id_dict[image_id] += 1
        else:
            id_dict[image_id] = 0
        question_ids.append(int(str(image_id) + str(id_dict[image_id])))
    handle = io.open(output_file, 'wb')
    for i in xrange(len(image_ids)):
        handle.write('%d\t%d\t%s\t%s\n' % (image_ids[i], question_ids[i],
                                           questions[i], answers[i]))

def prepare_json(text_file, gt_file_q, gt_file_a):
    """
    Create json ground truth file for evaluation
    """
    questions, annotations = [], []
    data = io.open(text_file).readlines()
    for line in data:
        items = line.strip().split('\t')
        ques, anno = {}, {}
        ques['image_id'], anno['image_id'] = int(items[0]), int(items[0])
        ques['question_id'], anno['question_id'] = int(items[1]), int(items[1])
        ques['question'], anno['question'] = items[2], items[2]
        anno['multiple_choice_answer'] = items[3]
        questions.append(ques)
        annotations.append(anno)
    task_type = 'Multiple Choice'
    handle = io.open(gt_file_q, 'wb')
    json.dump({'info': '', 'license': '', 'task_type': task_type, 'data_type': '',
               'data_subtype': '', 'questions': questions}, handle)
    handle = io.open(gt_file_a, 'wb')
    json.dump({'annotations': annotations}, handle)

