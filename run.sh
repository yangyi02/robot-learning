PADDLE_PATH=/home/yi/code/paddle/idl/paddle
export PATH=$PADDLE_PATH/paddle:$PATH
# Draw net path
export PYTHONPATH=$PADDLE_PATH/platform/tool:$PYTHONPATH
# Train path
export PYTHONPATH=$PADDLE_PATH/paddle/output/pylib:$PYTHONPATH
export PYTHONPATH=$PADDLE_PATH/paddle/trainer:$PYTHONPATH
export PYTHONPATH=./train/data_provider:$PYTHONPATH
export PYTHONPATH=./train/conf:$PYTHONPATH
# Evaluation path
#COCO_PATH=/media/yi/DATA/data-orig/microsoft_coco
# export PYTHONPATH=$COCO_PATH/coco-caption/pycocoevalcap:$PYTHONPATH
#export PYTHONPATH=$COCO_PATH/coco-caption:$PYTHONPATH
#export PYTHONPATH=$COCO_PATH/VQA/PythonEvaluationTools:$PYTHONPATH
#export PYTHONPATH=$COCO_PATH/VQA/PythonHelperTools:$PYTHONPATH
# Caffe path
#CAFFE_PATH=/home/yi/code/tools/caffe
#CAFFE_PATH=/home/yi/code/tools/deep-residual-network/caffe
#export PYTHONPATH=$CAFFE_PATH/python:$PYTHONPATH

#TASK=coco_junhua
#TASK=coco_junhua2
#TASK=coco_qa_en
#TASK=coco_cap_en
#TASK=coco_qa_cn
#TASK=coco_cap_cn
#TASK=tieba_dia_cn
#TASK=coco_qa_cap_en
#TASK=coco_qa_cap_en_noword
#TASK=coco_qa_cap_cn
#TASK=coco_qa_cap_dia_cn
#TASK=imagenet_cap_en
#TASK=imagenet_qa_en
#TASK=imagenet_qa_cap_en
TASK=imagenet_qa_cap_en_noword

#TRAIN_CONF=./train/conf/img_qa.py
#TEST_CONF=./train/conf/img_qa_gen.py
#TRAIN_CONF=./train/conf/chat.py
#TEST_CONF=./train/conf/chat_gen.py
#TRAIN_CONF=./train/conf/lstm_v2.py
#TEST_CONF=./train/conf/lstm_v2_gen.py
#TRAIN_CONF=./train/conf/lstm_junhua.py
#TEST_CONF=./train/conf/lstm_junhua_gen.py
#TRAIN_CONF=./train/conf/lstm_junhua2.py
#TEST_CONF=./train/conf/lstm_junhua2_gen.py
#TRAIN_CONF=./train/conf/lstm_recurrentunit.py
#TEST_CONF=./train/conf/lstm_recurrentunit_gen.py
#TRAIN_CONF=./train/conf/lstm_junhua_noimage.py
#TEST_CONF=./train/conf/lstm_junhua_noimage_gen.py
#TRAIN_CONF=./train/conf/lstm_yi.py
#TEST_CONF=./train/conf/lstm_yi_gen.py
#TRAIN_CONF=./train/conf/lstm_yi_recurrentunit.py
#TEST_CONF=./train/conf/lstm_yi_recurrentunit_gen.py
#TRAIN_CONF=./train/conf/lstm_v1.py
#TEST_CONF=./train/conf/lstm_v1_gen.py
#TRAIN_CONF=./train/conf/lstm_v2_junhua.py
#TEST_CONF=./train/conf/lstm_v2_junhua_gen.py
#TRAIN_CONF=./train/conf/lstm_v2.py
#TEST_CONF=./train/conf/lstm_v2_gen.py
TRAIN_CONF=./train/conf/img_qa.py
TEST_CONF=./train/conf/img_qa_gen.py
#TRAIN_CONF=./train/conf/img_qa_gate.py
#TEST_CONF=./train/conf/img_qa_gate_gen.py
#TRAIN_CONF=./train/conf/img_qa_gate2.py
#TEST_CONF=./train/conf/img_qa_gate2_gen.py
#TRAIN_CONF=./train/conf/img_qa_regularize_more.py
#TEST_CONF=./train/conf/img_qa_gen.py
#TRAIN_CONF=./train/conf/caption_qa.py
#TEST_CONF=./train/conf/caption_qa_gen.py

#FEATURE_TYPE=vgg_fc7_4096
#FEATURE_TYPE=vgg_fc7_4096_oversample
#FEATURE_TYPE=google_pool5_1024
#FEATURE_TYPE=google_pool5_1024_oversample
#FEATURE_TYPE=google_3072
#FEATURE_TYPE=junhua
#FEATURE_TYPE=resnet_pool5_2048_oversample
FEATURE_TYPE=resnet152_pool5_2048_oversample

#BATCH_TYPE=image_text
#BATCH_TYPE=text

#python robot_learning.py --debug --task=$TASK --train_conf=$TRAIN_CONF --test_conf=$TEST_CONF --feature_type=$FEATURE_TYPE --batch_type=$BATCH_TYPE  --train_epoch=3 --gpu

TRAIN_EPOCH=31
#python robot_learning.py --debug --task=$TASK --train_conf=$TRAIN_CONF --test_conf=$TEST_CONF --feature_type=$FEATURE_TYPE --train_epoch=3
#python robot_learning.py --debug --task=$TASK --train_conf=$TRAIN_CONF --test_conf=$TEST_CONF --feature_type=$FEATURE_TYPE --train_epoch=3 --gpu
python robot_learning.py --task=$TASK --train_conf=$TRAIN_CONF --test_conf=$TEST_CONF --feature_type=$FEATURE_TYPE --train_epoch=$TRAIN_EPOCH --gpu
